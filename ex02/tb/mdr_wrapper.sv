module mdr_wrapper 
import alu_pkg::*;
import tb_mdr_pkg::*;

(
input          clk,
input          rst,
tb_mdr_if.mdr  itf
);

//FIXME[DEV]:define two variable using your RTL datatype
inp_t result     ;
inp_t remainder  ;


//Instance your own MDR and cast to the specific datatype to RTL
ALU dut(
    .clk        ( itf.clk              ),
    .rst        ( rst                  ),
    .data_a     ( inp_t'(itf.data_a)   ),
    .data_b     ( inp_t'(itf.data_b)   ),
    .operacion  ( op_t'(itf.operacion) ),
    .start      ( !if.start             ),
    .result     ( result               ),
    .ready      ( itf.done             )
);

//Cast using this testbench data_t type
assign itf.result    = data_t'( result     );
assign itf.remainder = data_t'( remainder  );
 
          
endmodule
