interface tb_mdr_if(
input clk
);
import alu_pkg::*;

logic [WIDTH_OP-1:0] operacion;
data_t      data_a;
data_t      data_b;
data_t      result;
logic       start;
logic       done;

modport tstr (
   input clk,
   output data_a,
   output data_b,
   output start,
   output operacion,
   input done,
   input result
);

modport  mdr(
   input clk,
   input data_a,
   input data_b,
   input start,
   input operacion,
   output done,
   output result
);
endinterface
