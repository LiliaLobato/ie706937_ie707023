// Coder: DSc Abisai Ramirez Perez
// Date:  2020, 28th May
// Disclaimer: This code is not intended to be shared. It is 
// intended to be used for ITESO students at DVVSD Class 2020

class tester_mdr ;

localparam DW = 4;


`ifdef INVERTED_PULSES
   localparam FALSE  = 1'b1;
   localparam TRUE   = 1'b0;
`else
   localparam TRUE   = 1'b1;
   localparam FALSE  = 1'b0;
`endif

`protect
typedef logic signed [DW:0]       data_t;
typedef logic signed [2*DW-1:0]     results_t;
    typedef enum logic [3:0]{ 
        SUMA    = 3'd000,
        RESTA   = 3'b001,
        MULTI   = 3'b010,
        COMPL   = 3'b011,
        AND     = 3'b100,
        OR      = 3'b101,
        NEG     = 3'b110,
        XOR     = 3'b111,
    } op_e;

localparam LIMINF =-(2**(DW-1));
localparam LIMSUP = (2**(DW-1))-1;
localparam PERIOD = 4; // Minimum value 4
localparam DLY       = 2;
localparam INC       = 256;
localparam OP_INIT   = 0;
localparam OP_LAST   = 2;

`endprotect
longint BASE      =  (((2**DW)*(2**DW))/INC);
longint TESTED    =  2*BASE + ((2**DW)/INC) ;
real calf;
integer text_id         ;
integer count           ;
virtual tb_mdr_if itf   ;

data_t  x, y;
data_t  x_o_q, y_o_q;
data_t  x_r, y_r;
data_t  x_md, y_md;
logic [1:0] op_o ;
data_t  q_data_a[$];
data_t  q_data_b[$];

logic signed [DW-1:0] data_a_int, data_b_int;
results_t       result_exp_tb ;
results_t       result_hw ;
results_t       temp_result ;
results_t       remainer_exp_tb ;
results_t       remainer_hw ;
logic           ready;
logic           error_exp_tb;


`protect
function new(virtual tb_mdr_if.tstr t);
    itf = t;
endfunction

//***    Inject two data    ***//
task automatic inj_two_data(data_t x_in, data_t y_in );
    #(0.2) // Offset
    itf.start   = TRUE  ;
    #(PERIOD)  itf.start   = FALSE  ;
    itf.data_a = X_in;
    itf.data_b = y_in;
    q_data_a.push_front(x_in);
    q_data_b.push_front(y_in);
endtask

//***    Inject data   ***//
task automatic inject_all_data_md();
    for (x_md =LIMINF; x_md<LIMSUP ; x_md=x_md+INC ) begin
        for (y_md =LIMINF; y_md<LIMSUP ; y_md= y_md+INC) begin
            if (x_md ==LIMINF && y_md==LIMINF)
                #(3*PERIOD) inj_two_data(x_md, y_md);
            else begin
                #(3*PERIOD) inj_two_data(x_md, y_md);
            end
            @(posedge itf.done) ;
        end
    end
endtask

//***    Inject data for all operations   ***//
task automatic inject_all_data_mdr();
    $display("SUMA %0dps", $time);
    #(PERIOD)   itf.operacion = SUMA;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("RESTA %0dps", $time);
    #(PERIOD)   itf.operacion = RESTA;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("MULTIPLICACION %0dps", $time);
    #(PERIOD)   itf.operacion = MULTI;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("COMPLEMENTO A2 %0dps", $time);
    #(PERIOD)   itf.operacion = COMPL;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("AND %0dps", $time);
    #(PERIOD)   itf.operacion = AND;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("OR %0dps", $time);
    #(PERIOD)   itf.operacion = OR;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("NEGACION %0dps", $time);
    #(PERIOD)   itf.operacion = NEG;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
    $display("XOR %0dps", $time);
    #(PERIOD)   itf.operacion = XOR;
    inject_all_data_md();
    #(PERIOD) #(PERIOD)
endtask 


task automatic init();
   itf.operacion = FALSE ;
   itf.data_a    = FALSE ;
   itf.data_b    = FALSE ;
   itf.start     = FALSE ;
endtask

// ***      Calculate result based on operation     ***//
task op_calc (op_e operacion, data_t a, data_t b);
  case (operacion)
      SUMA: begin
        temp_result         = a+b;
        result_exp_tb       = temp_result;
      end
      RESTA: begin
        temp_result         = a-b;
        result_exp_tb       = temp_result;
      end
      MULTI: begin
        temp_result         = a*b;
        result_exp_tb       = temp_result;
      end
      COMPL: begin
        temp_result         = a*(-1);
        result_exp_tb       = temp_result;
      end
      AND: begin
        temp_result         = a&&b;
        result_exp_tb       = temp_result;
      end
      OR: begin
        temp_result         = a||b;
        result_exp_tb       = temp_result;
      end
      NEG: begin
        temp_result         = !a;
        result_exp_tb       = temp_result;
      end
      XOR: begin
        temp_result         = a^b;
        result_exp_tb       = temp_result;
      end
  endcase
endtask 


task automatic open_file();
    text_id = $fopen("report.txt", "w");
endtask

task automatic close_file();
    $fclose(text_id);
endtask

task automatic review_output ();
   count =0;
   for (op_o =OP_INIT; op_o<=OP_LAST ; op_o++ ) begin
      for (int x_o =LIMINF; x_o<LIMSUP ; x_o=x_o+INC ) begin
         for (int y_o =LIMINF; y_o<LIMSUP ; y_o=y_o + INC) begin
            @(posedge itf.done)
            #(DLY*PERIOD)
                      x_o_q   = q_data_a.pop_front();
                      y_o_q   = q_data_b.pop_front();
                      op_calc(itf.operacion, x_o_q, y_o_q);
                      if ( (itf.result != result_exp_tb)  || (error_exp_tb != itf.error)) begin
                      $fwrite(text_id, "[%0dps] ERROR: op: %0d, x: %0d, y: %0d, Exp Result:%0d Hw Res:%0d \n", $time, itf.operacion, x_o_q, y_o_q, result_exp_tb, itf.result);
                      $display("[%0dps] ERROR: op: %0d, x: %0d, y: %0d, Exp Result:%0d Hw Res:%0d \n", $time, itf.operacion, x_o_q, y_o_q, result_exp_tb, itf.result);
                      count = count +1;
           end
      end
   end
   calf = ( real'(count)/real'(TESTED) ) *100;
   $display("[%0d] Tested: %0d, Errors:%0d, Error porcentage: %3.2f",$time,  TESTED, count, calf);
endtask 

`endprotect
endclass
