onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mdr/itf/clk
add wave -noupdate /tb_mdr/uut/rst
add wave -noupdate /tb_mdr/itf/op
add wave -noupdate /tb_mdr/uut/dut/pipo_op/inp
add wave -noupdate /tb_mdr/uut/dut/pipo_op/out
add wave -noupdate /tb_mdr/uut/dut/data_op_wr
add wave -noupdate /tb_mdr/uut/dut/data_op
add wave -noupdate /tb_mdr/uut/dut/op_wr
add wave -noupdate /tb_mdr/uut/dut/state_Mult/op
add wave -noupdate /tb_mdr/itf/data
add wave -noupdate -radix decimal /tb_mdr/uut/dut/pipo_data/inp
add wave -noupdate -radix decimal /tb_mdr/uut/dut/pipo_data/out
add wave -noupdate -radix decimal /tb_mdr/uut/dut/pipo_data/out_reg
add wave -noupdate -radix decimal /tb_mdr/uut/dut/pipo_data/rgstr_r
add wave -noupdate /tb_mdr/itf/result
add wave -noupdate /tb_mdr/itf/remainder
add wave -noupdate -divider flags
add wave -noupdate /tb_mdr/itf/load
add wave -noupdate /tb_mdr/itf/start
add wave -noupdate /tb_mdr/uut/dut/pipo_start/out_reg
add wave -noupdate /tb_mdr/itf/load_x
add wave -noupdate /tb_mdr/itf/load_y
add wave -noupdate /tb_mdr/itf/error
add wave -noupdate /tb_mdr/itf/ready
add wave -noupdate /tb_mdr/uut/dut/dataX/enb
add wave -noupdate -divider MDR
add wave -noupdate -radix decimal /tb_mdr/uut/dut/dataX/inp
add wave -noupdate -radix decimal /tb_mdr/uut/dut/dataX/out
add wave -noupdate -radix decimal /tb_mdr/uut/dut/data_x
add wave -noupdate /tb_mdr/uut/dut/dataY/enb
add wave -noupdate -radix decimal /tb_mdr/uut/dut/dataY/inp
add wave -noupdate -radix decimal /tb_mdr/uut/dut/dataY/out
add wave -noupdate -radix decimal /tb_mdr/uut/dut/data_y
add wave -noupdate -radix decimal /tb_mdr/uut/dut/MDR_inst/out
add wave -noupdate -radix decimal /tb_mdr/uut/dut/MDR_inst/x_bin
add wave -noupdate -radix decimal /tb_mdr/uut/dut/MDR_inst/y_bin
add wave -noupdate /tb_mdr/uut/dut/MDR_inst/pipo_en
add wave -noupdate /tb_mdr/uut/dut/state_Mult/Edo_Actual
add wave -noupdate /tb_mdr/uut/dut/state_Mult/En
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {30821 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 217
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {208896 ps}
