package tb_mdr_pkg;

localparam DW = 16;
localparam PERIOD = 4;
typedef logic signed [DW-1:0]       data_t;
typedef logic signed [2*DW-1:0]     results_t;

    typedef enum logic [3:0]{ 
        SUMA    = 3'd000,
        RESTA   = 3'b001,
        MULTI   = 3'b010,
        COMPL   = 3'b011,
        AND     = 3'b100,
        OR      = 3'b101,
        NEG     = 3'b110,
        XOR     = 3'b111,
    } op_e;

endpackage
