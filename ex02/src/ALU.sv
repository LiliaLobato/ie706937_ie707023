// =============================================================================
// Title        : Pipo
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   t05
// File         :   pipo.sv
// Date         :   21/02/2020 tt
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module ALU (
	input 			clk,
	input 			rst,
	input  inp_t    data_a,
	input  inp_t    data_b,
	input  op_e     operacion,
	input 			start,
	output 			done,
	output out_t    result
);

inp_t data_a_reg;
inp_t data_b_reg;
inp_t data_a_N;
inp_t data_b_N;
inp_t data_a_bin;
inp_t data_b_bin;
logic sign_a;
logic sign_b;
logic sign_rem;
logic dbcr_start

op_e  operacion_wr;
op_e  operacion_reg;
op_e  operacion_N;
op_e  operacion_c2;

out_t result_wr;
out_t result_c2;

//////////////////////////// SEÑALES DE CONTROL
pipo_Nenb pipo_start(
		.clk(clk_gen),
		.rst(rst),
		.inp(!start),
		.out(dbcr_start)
);

/////////////////////////// ENTRADAS
pipo_Nenb #(
.DW(DW)
) pipo_data_a(
	.clk(clk),
	.rst(rst),
	.inp(data_a), 
	.out(data_a_N)
);

pipo_Nenb #(
.DW(DW)
) pipo_data_b(
	.clk(clk),
	.rst(rst),
	.inp(data_b), 
	.out(data_b_N)
);

pipo_Nenb #(
.DW(WIDTH_OP)
) operation (
	.clk(clk),
	.rst(rst),
	.inp(operacion),
	.out(operacion_N)
);

/////////////////////////// ENTRADAS REGISTRADAS
pipo #(
.DW(DW)
) pipoREG_data_a(
	.clk(clk),
	.rst(rst),
	.enb(dbcr_start),
	.inp(data_a_N), 
	.out(data_a_reg)
);

pipo #(
.DW(DW)
) pipoREG_data_b(
	.clk(clk),
	.rst(rst),
	.enb(dbcr_start),
	.inp(data_b_N), 
	.out(data_b_reg)
);

pipo #(
.DW(WIDTH_OP)
) operationREG (
	.clk(clk),
	.rst(rst),
	.enb(dbcr_start),
	.inp(operacion_N),
	.out(operacion_wr)
);

assign operacion_reg = op_e'(operacion_wr);

///////////////COMPLEMENTO A 2
C22Bin #(
.DW(DW)
) conv_b(
	.complemento2(data_b_N),
	.binario     (data_b_bin),
	.signo		 (sign_b)
);

C22Bin #(
.DW(DW)
) conv_a(
	.complemento2(data_a_N),
	.binario     (data_a_bin),
	.signo		 (sign_a)
);

/////////////////////// FSM
fsm_moore state_Mult(
    .start(dbcr_start),
    .Rst(rst),
    .Clk(clk),
    .done(done)
);

//////////////////////////////////// ALU
always_comb begin : proc_ALU
    result_wr = '0;
	case (operacion_reg)
        SUMA:
        	result_wr = data_a_reg + data_b_reg;
        	sign_rem  = result_wr[DW-1];
        RESTA:
        	result_wr = data_a_reg - data_b_reg;
        	sign_rem  = result_wr[DW-1];
        MULTI:
        	result_wr = data_a_reg * data_b_reg;
        	sign_rem  = sign_a ^ sign_b;
        COMPL:
        	result_wr = data_a_bin;
        	sign_rem  = sign_a;
        AND:
        	result_wr = data_a_reg && data_b_reg;
        	sign_rem  = result_wr[DW-1];
        OR:
        	result_wr = data_a_reg || data_b_reg;
        	sign_rem  = result_wr[DW-1];
        NEG:
        	result_wr = !data_a_reg;
        	sign_rem  = result_wr[DW-1];
        XOR:
        	result_wr = data_a_reg ^ data_b_reg;
        	sign_rem  = result_wr[DW-1];
		default : 
        	result_wr = '0;
        	sign_rem  = '0;
	endcase

end

//////////////////////////// RESULT
Bin2C2 #(
.DW(DW*2)
) conv_remainder(
	.signo			(sign_rem),
	.binario     	(result_wr),
	.complemento2	(result_c2)
);

pipo_Nenb #(
.DW(DW*2)
) operation (
	.clk(clk),
	.rst(rst),
	.inp(done ? result_wr:'0),
	.out(result)
);
