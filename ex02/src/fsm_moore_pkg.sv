// =============================================================================
// Title        :   Package
// Project      :   t03
// File         :   fsm_moore_pkg.sv
// Date         :   01/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

`ifndef FSM_MOORE_PKG_SV
    `define FSM_MOORE_PKG_SV
package fsm_moore_pkg;

    //localparam W_IN = 5;
    //localparam MAX_TIMES = 1;
    //typedef logic [W_IN-1:0]    data_t;

  
    localparam W_ST = 2;
    typedef logic [W_ST-1:0]    state_t;
    typedef enum state_t{ 
        STATE_RESULT    = 2'b10, //Load X
        STATE_OP        = 2'b00, //genera los shift
        IDLE            = 2'b01
    } state_e;

    localparam W_VAL = 1;  
    typedef logic [W_VAL-1:0]   value_t; 
    typedef enum value_t{
        ON  = 1'b1,
        OFF = 1'b0
    } value_e;

endpackage
`endif
 
