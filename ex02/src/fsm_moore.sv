//`timescale 1ns / 1ps
// =============================================================================
// Title        : Fsmb de dos estados:
//                  -> Idle
//                  -> Load X
//                  -> Load Y
//                  -> SHIFT
//                Genera una secuencia de DW shifts  
// Project      :   p02
// File         :   fsm_moore.sv
// Date         :   01/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import mult_top_pkg::*;

module fsm_moore
import fsm_moore_pkg::*;
(
    input               Rst,
    input               Clk,
    input               start,
    output value_t      done
);

state_e Edo_Actual;	
state_e edo_siguiente;


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(Edo_Actual)
      STATE_RESULT:begin 
          edo_siguiente=IDLE;
      end
      STATE_OP:begin 
          edo_siguiente=STATE_RESULT;
      end
      IDLE:begin 
        if ( start ) begin
          edo_siguiente=STATE_OP;
        end else begin
          edo_siguiente=IDLE;
        end
      end
   endcase
end

//asignaciones por default y siguiente
always_ff@(posedge Clk or negedge Rst) begin // Circuito Secuenicial en un proceso always.
   if (!Rst) 
      Edo_Actual  <= IDLE;
   else 
      Edo_Actual  <= edo_siguiente;
end

// CTO combinacional de salida.
always_comb begin
case(Edo_Actual)
    IDLE : begin
        done   = OFF;
    end
    STATE_OP: begin
        done      = OFF;
    end
    STATE_RESULT: begin
        done      = ON;
    end
    default: begin
        done   = OFF;
      end
endcase 
end

endmodule