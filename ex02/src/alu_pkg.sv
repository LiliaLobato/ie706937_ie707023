// =============================================================================
// Title        :   Package
// Project      :   p02
// File         :   mult_top_pkg.sv
// Date         :   01/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`ifndef ALU_PKG
    `define ALU_PKG
package alu_pkg;

    localparam DW = 4;

    typedef logic signed [DW-1:0]       inp_t;
    typedef logic signed [(DW*2)-1:0]   out_t;
    typedef logic signed [DW-1:0]       data_t;

    localparam OP = 8;
    localparam WIDTH_OP = $clog2(OP)+1;
    typedef logic [WIDTH_OP-1:0]	op_t; 
    typedef enum op_t{ 
        SUMA    = 3'd000,
        RESTA   = 3'b001,
        MULTI   = 3'b010,
        COMPL   = 3'b011,
        AND     = 3'b100,
        OR      = 3'b101,
        NEG     = 3'b110,
        XOR     = 3'b111,
    } op_e;
    
endpackage
`endif
 
