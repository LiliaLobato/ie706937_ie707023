// =============================================================================
// Title       	:	Implementación de un decodificador binario en sistema complemento a 2
//					a su equivalente en unidades, decenas y centenas
// Inputs		:	8 Switches
// Outputs		:	3 displays de 7 segmentos
// Project     	: 	t01
// File        	: 	tb_bin_BCD.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`timescale 1ns / 1ps
module tb_fifo();


// Parameter Declarations
parameter DATA_WIDTH = 4;
parameter BUFFER_DEPTH = 2;

// Input Ports
bit clk;
bit rst;
bit pop;
bit push;
bit [DATA_WIDTH-1:0] DataInput;

// Output Ports
bit full;
bit empty;
bit [DATA_WIDTH-1:0] DataOutput;

/********************* Device Under Verification **************/
fifo #(
    // Parameter Declarations
    .DATA_WIDTH(DATA_WIDTH),
    .BUFFER_DEPTH(BUFFER_DEPTH)
) DUV (
    // Input Ports
    .clk(clk),
    .rst(rst),
    .pop(pop),
    .push(push),
    .DataInput(DataInput),

    // Output Ports
    .full(full),
    .empty(empty),
    .DataOutput(DataOutput)
);
   
initial begin
    clk = 0;
    rst = 1;
    DataInput = '0;
end

always begin
        #2  clk = ~clk;
end

initial begin 
    rst = 0;    #20
    rst = 1;

   /* repeat (BUFFER_DEPTH+1) begin
        push = 1'b1; #2
        pop  = 1'b1; #2
        #4 DataInput = DataInput + 1;
    end */
 /*repeat (BUFFER_DEPTH*2) begin
        push = 1'b1; #4
        push  = 1'b0;
        #4 DataInput = DataInput + 1; 
    end*/

    #3 DataInput = 1;
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;

    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;
   
    pop  = 1'b0;
    push = 1'b1;
    #6 push = 1'b0;
    #4 DataInput = DataInput+1;

    //pop
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4

    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
    
    push  = 1'b0;
    pop = 1'b1;
    #6 pop = 1'b0;
    #4
   
   
    #800 
    $stop;
end

endmodule
