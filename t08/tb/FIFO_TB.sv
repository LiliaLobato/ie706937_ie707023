
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module FIFO_TB;

// Parameter Declarations
parameter DATA_WIDTH = 16;
parameter BUFFER_DEPTH = 2;//8;

// Input Ports
bit clk;
bit rst;
bit pop;
bit push;
bit [DATA_WIDTH-1:0] DataInput;

// Output Ports
bit full;
bit empty;
bit [DATA_WIDTH-1:0] DataOutput;



/********************* Device Under Verification **************/
fifo #(
	// Parameter Declarations
	.DATA_WIDTH(DATA_WIDTH),
	.BUFFER_DEPTH(BUFFER_DEPTH)
) DUV (
	// Input Ports
	.clk(clk),
	.rst(rst),
	.pop(pop),
	.push(push),
	.DataInput(DataInput),

	// Output Ports
	.full(full),
	.empty(empty),
	.DataOutput(DataOutput)
);

/**************************************************************************/
	
/******************** Stimulus *************************/
initial // Clock generator
  begin
    clk = 1'b0;
    forever #2 clk = !clk;
  end
/*----------------------------------------------------------------------------------------*/
initial begin /*rst*/
	# 0 rst = 1'b0;
	#3 rst = 1'b1;
end
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 0 pop  = 1'b0;
	# 80 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
	# 4 pop  = 1'b1;
	# 4 pop  = 1'b0;
end 
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 0  push = 0;
	# 10  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
	# 4  push = 1'b1;
	# 4  push = 0;
end
/*----------------------------------------------------------------------------------------*/ 
initial begin
	# 0  DataInput = 1'b0;
	# 8  DataInput = 8;
	# 4  DataInput = 7;
	# 4  DataInput = 6;
	# 4  DataInput = 5;
	# 4  DataInput = 0;
	# 4  DataInput = 4;
	# 4  DataInput = 0;
	# 4  DataInput = 3;
	# 4  DataInput = 0;
	# 4  DataInput = 2;
	# 4  DataInput = 0;
	# 4  DataInput = 1;
end


/*--------------------------------------------------------------------*/



endmodule
 
 
/*************************************************************/
/*************************************************************/

 