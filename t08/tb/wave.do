onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_fifo/clk
add wave -noupdate /tb_fifo/rst
add wave -noupdate /tb_fifo/pop
add wave -noupdate /tb_fifo/push
add wave -noupdate -radix unsigned /tb_fifo/DataInput
add wave -noupdate -radix unsigned /tb_fifo/DataOutput
add wave -noupdate -radix decimal -childformat {{{/tb_fifo/DUV/ram_instance/ram[3]} -radix decimal} {{/tb_fifo/DUV/ram_instance/ram[2]} -radix decimal} {{/tb_fifo/DUV/ram_instance/ram[1]} -radix decimal} {{/tb_fifo/DUV/ram_instance/ram[0]} -radix decimal}} -subitemconfig {{/tb_fifo/DUV/ram_instance/ram[3]} {-height 15 -radix decimal} {/tb_fifo/DUV/ram_instance/ram[2]} {-height 15 -radix decimal} {/tb_fifo/DUV/ram_instance/ram[1]} {-height 15 -radix decimal} {/tb_fifo/DUV/ram_instance/ram[0]} {-height 15 -radix decimal}} /tb_fifo/DUV/ram_instance/ram
add wave -noupdate -divider flags
add wave -noupdate /tb_fifo/full
add wave -noupdate /tb_fifo/empty
add wave -noupdate -divider pointers
add wave -noupdate -radix unsigned /tb_fifo/DUV/addr_next
add wave -noupdate -radix unsigned /tb_fifo/DUV/nrd_ptr
add wave -noupdate -radix unsigned /tb_fifo/DUV/rd_ptr
add wave -noupdate -radix unsigned /tb_fifo/DUV/wr_ptr
add wave -noupdate -radix unsigned /tb_fifo/DUV/nwr_ptr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {20786 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 235
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {51180 ps}
