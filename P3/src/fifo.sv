// =============================================================================
// Title        :   FIFO
// Inputs       :   4 bits decimal number 
// Outputs      :   3 displays de 7 segmentos
// Project      :   t08
// File         :   fifo.sv
// Date         :   21/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
//Description: this is the module of FIFO, save a data into a memory. Output empty is high when the fifo is empty and output full is high when fifo is full.
//Write enable makes push and read enable makes pops. 
//
//      	                    fifo
//
//				                /.................\
//				                |                 |
//            clk	--->	|                 |
//            rst	--->	|                 | ---> DataOutput
//            push	--->	|                 | ---> empty
//            pop	--->	|                 | ---> full
//            DataInput	  --->	|                 |
//                    		|                 |
//                        /.................\
//
//To Do:
import sp_ram_pkg::*;

module fifo #(
    DATA_WIDTH=W_DATA, 
    BUFFER_DEPTH=W_DEPTH
  )(
    input  logic                    clk,   //clk
    input  logic                    rst,   //rst
    input  logic                    push,  //push enable
    input  logic                    pop,  // pop enable
    input  logic [(DATA_WIDTH-1):0] DataInput,    //data input
    output logic [(DATA_WIDTH-1):0] DataOutput,   //data output
    output logic                    empty,  // signal of empty
    output logic                    full    //signal of full
  );
    
  logic nempty;
  logic nfull;
  logic nempty_next;
  logic nfull_next;
  logic we_next;
  logic [(DATA_WIDTH-1):0] DataOutput_next;
   
  logic [(BUFFER_DEPTH-1):0] wr_ptr, nwr_ptr, addr_next;           // Write pointer
  logic [(BUFFER_DEPTH-1):0] rd_ptr, nrd_ptr;           // Read pointer

  sp_ram_if mem_interface();
  
  sp_ram ram_instance(
    .cclk(clk),
    .mem_if(mem_interface.mem)
  );


 // --------------------------------------------
  always_ff @(posedge clk or negedge rst) begin 
    if(!rst) begin
        wr_ptr <= '0;
        rd_ptr <= '0;
        full   <= '0;
        empty  <= '1;
    end else begin
        wr_ptr  <= nwr_ptr; //clear wr ptr
        rd_ptr  <= nrd_ptr; //clear rd ptr
        full    <= nfull;
        empty   <= nempty;
    end
  end

  always_comb begin
    nwr_ptr = (push && !full) ? wr_ptr+1'b1 : wr_ptr; // if have a push enable and isnt full, move the pointer +1 
    nrd_ptr = (pop && !empty) ? rd_ptr+1'b1 : rd_ptr; // if have a pop enable and isnt empty, move the pointer +1
  end

  // --------------------------------------------
  always_ff @(posedge clk or negedge rst) begin 
    if(!rst) 
        DataOutput  <= 0;
    else 
        DataOutput  <= DataOutput_next; //push ? DataOutput : pop ? mem_interface.rd_data : DataOutput;//DataOutput_next;
  end

  always_ff @(posedge clk or negedge rst) begin
    if(!rst) begin
      mem_interface.we      <= 0;
      mem_interface.data    <= '0;
      mem_interface.rw_addr <= 0;
    end else begin //if fifo isnt full data input is saved in the buffer
      mem_interface.we      <= we_next;
      mem_interface.data    <= DataInput;
      mem_interface.rw_addr <= push ? addr_next:nrd_ptr;
  end
end
  
  always_comb begin
    we_next = (push && !full) ? '1:'0;
    DataOutput_next = empty ? DataOutput : mem_interface.rd_data;
    addr_next = pop?rd_ptr:wr_ptr;//push ? wr_ptr: pop ? rd_ptr : '0;
  end

//-------------------------------------------------------------------
               
  always_comb begin
    nempty = (!rst) ? '1 : (wr_ptr==nwr_ptr)?(((!nfull) &&(push) && (!pop) && (wr_ptr==nrd_ptr)))?'1:((!nfull) && (wr_ptr==nrd_ptr))?'1:'0 :'0;//if wr pnt is the same of rd ptr, is empty
    nfull =  (!rst) ? '0 : (( (push) && (!pop) && (!empty) && (rd_ptr==(nwr_ptr))))?'1: ((!empty) && (rd_ptr==nwr_ptr))?'1:'0; //if wr pnt +1 is the same of rd ptr, is full

  end          




endmodule: fifo// --------------------------------------------------------------------------

