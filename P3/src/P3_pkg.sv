// =============================================================================
// Title       	:	P03 Package
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	P03
// File        	: 	P3_pkg.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

`ifndef P3_PKG_SV
    `define P3_PKG_SV
package P3_pkg;

   localparam DW = 8;
	localparam DW_1_BIT = 1;
	localparam D_DW = 2*DW;
	localparam N = 4; //4x4 Matrix
	localparam CMD_Flags_DW = 4; //L, CMD, Start and Stop signals 
	localparam START_FLAG = 8'hFE;
	localparam END_FLAG = 8'hEF;
	localparam MAX_NUMBER_OF_BITS = 536; //8x8 matrix + CMD + L + End Flag
	 
	
   typedef logic [DW-1:0] 	DATA_BYTE_t;
	typedef logic [2*DW-1:0] MUL_DATA_t;
	typedef logic [4:0]		CNTR_t;
	typedef logic [MAX_NUMBER_OF_BITS - 1: 0] DATA_WORD_t;
	 
   localparam W_ST = 3;
   typedef logic [W_ST-1:0]    state_t;
   typedef enum state_t
	{ 
       IDLE    		= 3'b000, //Waits for start flag
       Rx    			= 3'b001, //waits for end flag
		 ID    			= 3'b010, //waits for end flag
       ALIMENTADOR	= 3'b011, //
       PROCESSING 	= 3'b100, //waits for ready
       COLECTOR	 	= 3'b101, //
       Tx			 	= 3'b110 //waits for end flag
		 
   } state_e;
	 
    localparam W_VAL = 1;  
    typedef logic [W_VAL-1:0]   value_t; 
    typedef enum value_t{
        ON  = 1'b1,
        OFF = 1'b0
    } value_e;
	 
    localparam OP = 3;
    typedef logic [OP-1:0]	CMD_t; 
    typedef enum CMD_t{ 
      Set_Matrix_Size   = 3'b001, 
      Retransmit        = 3'b010, 
      Start_Data_Cap    = 3'b011,
		Transmition			= 3'b100,
		INVALID_CMD			= 3'b000
		
    } CMD_e;
	 
	 
	 localparam demux_bits = 9;
	 typedef logic [demux_bits - 1:0] fifo_demux_t;
	 typedef enum fifo_demux_t{
	 
		NO_FIFO		= 9'b000000000,
		FIFO_N1		= 9'b000000001,
		FIFO_N2		= 9'b000000010,
		FIFO_N3		= 9'b000000100,
		FIFO_N4		= 9'b000001000,
		FIFO_N5		= 9'b000010000,
		FIFO_N6		= 9'b000100000,
		FIFO_N7		= 9'b001000000,
		FIFO_N8		= 9'b010000000,
		FIFO_V		= 9'b100000000
	 
	 } fifo_demux_e;
	 
	 
endpackage
`endif