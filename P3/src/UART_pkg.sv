// =============================================================================
// Title       	:	UART Top
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t07
// File        	: 	UART_plg.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

`ifndef UART_PKG_SV
    `define UART_PKG_SV
package UART_pkg;

  // localparam DW = 8;
	//localparam UART_EXTENSION = DW + 3; //3 extra bits: Start bit, parity bit and stop bit.
	 
	
   //typedef logic [DW-1:0] 	DATA_BUS_t;
	//typedef logic [UART_EXTENSION-1:0] 	UART_BUS_t; // Complete transmision: Data + Protocol bits.
	typedef logic [4:0]		CNTR_t;
	 
	 typedef enum logic [1:0]{
        IDLE_STATE	= 2'b00,
        Tx_STATE		= 2'b01,
		  Rx_STATE		= 2'b10
    } UART_state;
	 

endpackage
`endif