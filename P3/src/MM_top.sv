// =============================================================================
// Title       	:	Implementacion de un multiplicador matricial con un arreglo de procesadores
// Inputs		:	System clk, reset, RX
// Outputs		:	ready, TX
// Project     	: 	p03
// File        	: 	MM_top.sv
// Date 		: 	01/04/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//							Gabriel Paz	   ie707023
// =============================================================================

import fsm_moore_pkg::*;
import clk_div_pkg::*;
import P3_pkg::*;
import UART_pkg::*;
import top_uart_pkg::*;

module MM_top(
	input  logic	clk,
	input  logic	rst,
	input  logic	PC_2_Rx_data,
	output logic	ready, //not sure if we need it
	output logic	Tx_2_PC_Data

);

//Clk_gen
logic clk_gen;
logic clk_pll;
logic clk_uart;



//guallers
//DATA_BUS_t P_Data_Rx_2_Comp_wr; //logic 8 bits data type
DATA_BYTE_t P_Data_Rx_2_Comp_wr; //logic 8 bits data type
DATA_BYTE_t P_Data_Rx_2_be_inverted; //logic 8 bits data type
logic Rx_Interrupt_wr;
logic Rx_Interrupt_wr_F;
logic Start_flag_wr;
logic counter_ovf_wr;


//registers
DATA_BYTE_t L_value_reg;
DATA_WORD_t sipo_out_reg;
CMD_e CMD_reg;
DATA_BYTE_t N_reg;
DATA_WORD_t Matrix_data_reg;
DATA_BYTE_t N_Value_out_reg;
DATA_BYTE_t N_counter_reg;
fifo_demux_t fifo_demux_reg;
DATA_BYTE_t data_2_fifo1_reg;
DATA_BYTE_t data_2_fifo2_reg;
DATA_BYTE_t data_2_fifo3_reg;
DATA_BYTE_t data_2_fifo4_reg;
DATA_BYTE_t data_2_fifo5_reg;
DATA_BYTE_t data_2_fifo6_reg;
DATA_BYTE_t data_2_fifo7_reg;
DATA_BYTE_t data_2_fifo8_reg;
DATA_BYTE_t data_2_fifov_reg;


//enable wires
logic L_is_next_en;
logic L_is_next_en_delayed;
logic Data_Capture_en;
logic Data_Capture_en_S;
logic clear_Rx_Interrupt;
logic clear_Rx_Interrupt_S;
logic count_L_times_en;
logic count_L_times_en_delayed;
logic end_flag_en;
logic pipo_L_en;
logic CMD_Decoder_en;
logic CMD_Decoder_pulse;
logic N_2_Feeder_en;


//flags
logic CMD_Decoded_flag;

assign clk_gen = clk;
/** UART TX Clk*/
uart_clk clk_uart_tx (
	.clk(clk_gen),
	.rst(rst),
	.clk_gen(clk_uart)
);


//set flag in the other clock domain
anti_oneShot
Clear_Rx_AOS(
	.clk(clk_gen),
	.rst(rst),
	.inp(clear_Rx_Interrupt),
	.out(clear_Rx_Interrupt_S)
);

top_uart P3_UART(
	.clk_pll(clk_gen),
	.clk_tx(clk_uart),
	.rst(rst),
	.serial_data_rx(PC_2_Rx_data), //uart rx
	.clear_interrupt(clear_Rx_Interrupt_S), //From CU
	.transmit(), //start tx //From CU
	.parallel_data_tx(), //data to transmit
	
	.parallel_data_rx(P_Data_Rx_2_Comp_wr), //data recieved
	.rx_interrupt(Rx_Interrupt_wr), //flag for data received //to CU
	.parity_error(),
	.serial_data_tx()
);

//comb_organizer bit_order_inverter
//(
//
//	.inp_Data(P_Data_Rx_2_be_inverted),
//	.Out_Data(P_Data_Rx_2_Comp_wr)
//);


pipo_Nenb#(
	.DW(CMD_Flags_DW)
) pipo_CMD_Flags(
	.clk(clk_uart),
	.rst(rst),
	.inp(),
	.out()
);

oneShot Rx_interrupt_OS(
	.clk(clk_gen),
	.rst(rst),
	.inp(Rx_Interrupt_wr), //detects rising edge of Rx_interrupt
	.out(Rx_Interrupt_wr_F) //and generates a pulse for one (Faster clock) cycle 
);

//oneShot L_is_next_OS(
//	.clk(clk_gen),
//	.rst(rst),
//	.inp(L_is_next_en),
//	.out(L_is_next_en_delayed) //L_is_next flag delayed 1 clock cylce and lasts 1 clock cycle too.
//);

//set flag in the other clock domain
anti_oneShot
L_is_next_AOS(
	.clk(clk_gen),
	.rst(rst),
	.inp(L_is_next_en),
	.out(L_is_next_en_delayed) //L_is_next flag delayed 1 clock cylce and lasts 1 clock cycle too.
);



//Compares if the received data its a start flag
comparator#(
	.DW(DW)
) Start_Flag_Comp(
	.clk(clk_gen),
	.rst(rst),
	.enb(Rx_Interrupt_wr_F), //must compare when a byte of information is received 
	.in_A(P_Data_Rx_2_Comp_wr),
	.in_B(START_FLAG), 
	.out(Start_flag_wr)
);


	
assign pipo_L_en = (L_is_next_en_delayed & Rx_Interrupt_wr_F);

pipo pipo_L (
	.clk(clk_gen),
	.rst(rst),
	.enb(pipo_L_en), //L_is_next flag must be high all Rx state, so the & will be logic 1 when Rx_Interrupt_wr
	.inp(P_Data_Rx_2_Comp_wr),
	.out(L_value_reg)
);
 
 
//set flag in the other clock domain
anti_oneShot
Data_cap_AOS(
	.clk(clk_gen),
	.rst(rst),
	.inp(Data_Capture_en),
	.out(Data_Capture_en_S)
);
 
 
sipo_byte_msb word_message (
	.clk(clk_gen),
	.rst(rst),
	.enb((Data_Capture_en & Rx_Interrupt_wr_F)), //Data capture_en_S must be high all time in Rx state so & operation will be 1 when the Rx interrupt is set
	.inp(P_Data_Rx_2_Comp_wr),
	.out(sipo_out_reg)  //8*(L-L_byte) - 1 bits. Max number of bits = 8* (66-1) -1 = 519:0 = 520 bits 
);

 
 //Control Unit 
 
CU god(
	.clk(clk_gen),
	.rst(rst),
	.Start_Flag(Start_flag_wr),
	.End_Flag(end_flag_en),
	.L_Captured(pipo_L_en),
	.CMD(CMD_reg),
	.N_Value(N_reg),
	.CMD_Decoded_flag(CMD_Decoded_flag),
	.Feeder_finished(),

	.L_is_next(L_is_next_en),
	.Data_Capture_en(Data_Capture_en),
	.count_L_times_en(count_L_times_en),
	.CMD_Decoder_en(CMD_Decoder_en),
	.N_2_Feeder_en(N_2_Feeder_en),
	.N_Value_out(N_Value_out_reg),
	.clear_RxInterrupt(clear_Rx_Interrupt)
);
 


//pipo#(
//	.DW(DW_1_BIT)
//)pipo_count_L_times_en ( //we need to delay the signal one clk cycle, so we can take the correct value of L
//	.clk(clk_gen),
//	.rst(rst),
//	.enb(count_L_times_en),
//	.inp(count_L_times_en),
//	.out(count_L_times_en_delayed)
//);

pipo_clean pipo_count_L_times_en(
	.clk(clk_gen),
	.rst(rst),
	.enb(count_L_times_en),
	.inp(count_L_times_en),
	.out(count_L_times_en_delayed),
	.clean(~count_L_times_en)
);



//anti_oneShot
//L_Counter_en_AOS(
//	.clk(clk_gen),
//	.rst(rst),
//	.inp(count_L_times_en),
//	.out(count_L_times_en_delayed)
//);


variable_bin_counter count_L_times(
	.clk(Rx_Interrupt_wr), //
	.rst(rst),
	.enb(count_L_times_en_delayed), //must be high all Rx state
	.MAXCNT(L_value_reg), //might not haveits correct value when counting starts, but not sure if thats gonna be a problem
	.ovf(counter_ovf_wr),
	.count()
);
 
 
 //Compares if the received data is an end flag
comparator#(
	.DW(DW)
) end_Flag_Comp(
	.clk(clk_gen),
	.rst(rst),
	.enb(counter_ovf_wr), //must compare when L + 1 bytes were taken
	.in_A(P_Data_Rx_2_Comp_wr),
	.in_B(END_FLAG), 
	.out(end_flag_en)
);
 
 
anti_oneShot CMD_Decoder_AOS( //la señal que se espera viene de un modulo mas lento, por lo que el enable debe durar un poco mas
	.clk(clk_gen),
	.rst(rst),
	.inp(CMD_Decoder_en),
	.out(CMD_Decoder_pulse) //Enable is now a pulse, so we just save the data once on next module
);


 
 
 CMD_Decoder instruction_decode(
	.clk(clk_gen),
	.rst(rst),
	.Data(sipo_out_reg),
	.enable(CMD_Decoder_pulse),
	.L(L_value_reg),
	
	.ready(CMD_Decoded_flag),
	.CMD(CMD_reg),
	.N(N_reg),
	.Out_Data(Matrix_data_reg)
);

variable_bin_counter count_N_times(
	.clk(clk_gen), //
	.rst(rst),
	.enb(N_2_Feeder_en), //when it goes high, must remain for rest of the state
	.MAXCNT(N_Value_out_reg), //might not haveits correct value when counting starts, but not sure if thats gonna be a problem
	.ovf(),
	.count(N_counter_reg)
);

feeder Cuerda_para_morros_de_kinder(
	.clk(clk_gen),
	.rst(rst),
	.data_in(Matrix_data_reg), //vergazos de informacion
	.enable(N_2_Feeder_en),
	.N(N_Value_out_reg),
	.Counter_val(N_counter_reg),
	
	.fifo_demux(fifo_demux_reg), 			
	.data_2_fifo1(data_2_fifo1_reg),
	.data_2_fifo2(data_2_fifo1_reg),
	.data_2_fifo3(data_2_fifo1_reg),
	.data_2_fifo4(data_2_fifo1_reg),
	.data_2_fifo5(data_2_fifo1_reg),
	.data_2_fifo6(data_2_fifo1_reg),
	.data_2_fifo7(data_2_fifo1_reg),
	.data_2_fifo8(data_2_fifo1_reg),
	.data_2_fifoV(data_2_fifo1_reg)

);









endmodule