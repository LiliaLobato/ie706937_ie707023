// =============================================================================
// Title        : Processor
//                  Processing unit
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   P03
// File         :   pipo.sv
// Date         :   02/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module Processor
import P3_pkg::*;
(
	input		clk,
	input		rst,
	input 	DATA_BYTE_t A_Data,
	input 	DATA_BYTE_t B_Data,
	input 	enable,
	input 	ready,
	input		sum,
	output  	MUL_DATA_t Out_Data
);

MUL_DATA_t mul_result;
MUL_DATA_t PIPO_out; 
MUL_DATA_t sum_result;

assign PIPO_out = '0;
//assign mul_result = A_Data * B_Data;
//
//
//assign sum_result = mul_result + PIPO_out;

always_comb begin: cto_comb_entrada

	mul_result = A_Data * B_Data;


	sum_result = mul_result + PIPO_out;

end

 pipo #(
 .DW(D_DW) //D_DW = 2*DW
) Acumulator (
 .clk(clk),
 .rst(rst),
 .enb(enable),
 .inp(sum_result),
 .out(PIPO_out)
);


assign Out_Data = PIPO_out;



endmodule 
