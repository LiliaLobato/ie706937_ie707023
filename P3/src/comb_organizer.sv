module comb_organizer
import P3_pkg::*;
(

	input 	DATA_BYTE_t inp_Data,
	output  	DATA_BYTE_t Out_Data
);


assign Out_Data[0] = inp_Data[7];
assign Out_Data[1] = inp_Data[6];
assign Out_Data[2] = inp_Data[5];
assign Out_Data[3] = inp_Data[4];
assign Out_Data[4] = inp_Data[3];
assign Out_Data[5] = inp_Data[2];
assign Out_Data[6] = inp_Data[1];
assign Out_Data[7] = inp_Data[0];

endmodule 