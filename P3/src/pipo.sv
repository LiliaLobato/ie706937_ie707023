// =============================================================================
// Title        : Pipo
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   t05
// File         :   pipo.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module pipo #(
parameter DW = 8
) (
input               clk,
input               rst,
input               enb,
input  [DW-1:0]     inp,
output [DW-1:0]     out
);

logic [DW-1:0]      rgstr_r     ;
logic [DW-1:0]      rgstr_nxt   ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= inp;
end:rgstr_label

assign out  = rgstr_r;

endmodule

