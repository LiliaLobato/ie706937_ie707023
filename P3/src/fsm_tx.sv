// =============================================================================
// Title        : FSM TX UART
//                  State machine for transmitions
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module fsm_tx
import top_uart_pkg::*;
(
	input		clk,
	input 		rst,
	input		bit_cnt_ovf,
	input		transmit,
	output logic 		load,
	output logic 		enb,
	output logic  	bit_cnt_en,
	output logic 		start,
	output logic 		clean,
	output tx_out_e		selector
);

uart_state_e edo_actual;	
uart_state_e edo_siguiente;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      edo_actual  <= IDLE;
   else 
      edo_actual  <= edo_siguiente;
end


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(edo_actual)
		IDLE: begin
			if(transmit) //begins tx transmition
				edo_siguiente <= START;
			else
				edo_siguiente <= IDLE;
		end
		START: begin
			edo_siguiente <= SHIFT;
		end
		SHIFT: begin
			if(bit_cnt_ovf) //shifter for tx data
				edo_siguiente <= PARITY;
			else
				edo_siguiente <= SHIFT;
		end
		PARITY: begin
			edo_siguiente <= STOP;
		end
		STOP: begin
			edo_siguiente <= IDLE;
		end
		default: begin
			edo_siguiente <= IDLE;
		end
	endcase
end


always_comb begin
	case(edo_actual)
		IDLE: begin
			load 		= ON;
			enb 		= ON;
			bit_cnt_en 	= OFF;
			start 		= OFF;
			selector	= IDLE_BIT;
			clean 		= OFF;
		end
		START: begin
			load 		= OFF;
			enb 		= OFF;
			bit_cnt_en 	= OFF;
			start 		= OFF;
			selector	= IDLE_BIT;
			clean 		= ON;
		end
		SHIFT: begin
			load 		= OFF;
			enb 		= ON;
			bit_cnt_en 	= ON;
			start 		= OFF;
			selector	= DATA_OUT;
			clean 		= OFF;
		end
		PARITY: begin
			load 		= OFF;
			enb 		= OFF;
			bit_cnt_en 	= OFF;
			start 		= OFF;
			selector	= PARITY_BIT;
			clean 		= OFF;
		end
		STOP: begin
			load 		= OFF;
			enb 		= OFF;
			bit_cnt_en 	= OFF;
			start 		= ON;
			selector	= START_OR_STOP;
			clean 		= OFF;
		end
		default: begin
			load 		= ON;
			enb 		= ON;
			bit_cnt_en 	= OFF;
			start 		= OFF;
			selector	= IDLE_BIT;
			clean 		= OFF;
		end
	endcase
end


endmodule