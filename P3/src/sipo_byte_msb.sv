// =============================================================================
// Title        : Sipo byte lsb
//               	The byte input will be added to the lsb 
//					of the data word output 
// Inputs       : System clk, reset, enb, serial input
// Outputs      : Parallel output
// Project      :   t05
// File         :   sipo_msb.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          		Gabriel Paz    ie707023
// =============================================================================
import P3_pkg::*;
module sipo_byte_msb #(
parameter DW = MAX_NUMBER_OF_BITS //equals to L
) (
input               	clk,
input               	rst,
input               	enb,
input   DATA_BYTE_t   inp,
output  [ DW - 1:0]		out  //8*L - 1 bits. Max number of bits = 8* (67) -1 = 535:0 = 536 bits 
);

logic [DW - 1:0]	      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (enb)
        rgstr_r     <= {rgstr_r[DW - 8: 0], inp}; // 
end:rgstr_label

assign out  = rgstr_r;

endmodule