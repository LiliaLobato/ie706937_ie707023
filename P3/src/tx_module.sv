// =============================================================================
// Title        : 	TX module
//                  Tramnsmition of data
// Project      :   t09
// File         :   tx_module.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module tx_module
import top_uart_pkg::*;
(	
	input				clk,
	input				rst,
	input				transmit,
	input uart_baud_t	data, //data to transmit
	output				serial_output //data transmited
);

//Banderas
logic 		bit_cnt_ovf;
logic 		bit_cnt_en;
logic 		load;
logic 		enb;
logic 		start_or_stop; //signla for start or end of transmition
logic 		clean;
tx_out_e 	sel;

logic 	parity; //parity value
logic 	parity_or;
logic 	data_serial; //data in serial mode

logic serial_output_reg;

//Control unit
fsm_tx
fsm_tx(
	.clk(clk),
	.rst(rst),
	.bit_cnt_ovf(bit_cnt_ovf),
	.transmit(transmit),
	.load(load),
	.enb(enb),
	.bit_cnt_en(bit_cnt_en),
	.start(start_or_stop),
	.clean(clean),
	.selector(sel)
);

//tx bit cnt
bin_counter_ovf # (
	.DW    (CNT_DW_PC),
	.MAXCNT(DW_PC)
) bit_counter(
	.clk(clk),
	.rst(rst),
	.enb(bit_cnt_en),
	.count(),
	.ovf(bit_cnt_ovf)
);

//get data serialized
piso_lsb #(
	.DW(DW_PC)
) piso_tx(
	.clk(clk),
	.rst(rst),
	.enb(enb),
	.l_s(load),
	.inp(data),
	.out(data_serial)
);

pipo_Nenb pipo_or (
	.clk(clk),
	.rst(rst),
	.inp(data_serial^parity),
	.out(parity_or)
);

//parity calculation
pipo_double parity_pipo (
	.clk(clk),
	.rst(rst),
	.enb(enb),
	.inp(parity_or),
	.out(parity),
	.clean(clean)
);

//mux for output
always_comb begin : selector_serial
	case (sel)
		START_OR_STOP: begin
			serial_output_reg = start_or_stop;
		end
		DATA_OUT: begin
			serial_output_reg = data_serial;
		end
		PARITY_BIT: begin
			serial_output_reg = parity;
		end
		IDLE_BIT: begin
			serial_output_reg = '1;
		end
		default :  begin
			serial_output_reg = '0;
		end
	endcase
end


assign serial_output = serial_output_reg;

endmodule