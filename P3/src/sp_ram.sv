/*
Coder:          DSc Abisai Ramirez Perez
Date:           03/31/2019
Name:           sp_ram.sv
Description:    This is the HDL of a single port random access memory. 
*/

`ifndef SP_RAM_SV
    `define SP_RAM_SV
module sp_ram 
import sp_ram_pkg::*;
(
// Core clock
input   cclk,
// Memory interface
sp_ram_if.mem mem_if
);

// Declare a RAM variable 
data_t  ram [W_DEPTH-1:0];

//Variable to hold the registered read adddres
data_t  addr_logic;

always_ff@(posedge cclk) begin
	if(mem_if.we)
	    ram[mem_if.rw_addr] <= 	mem_if.data;
	mem_if.rd_data 		<= 	ram [mem_if.rw_addr];
end

endmodule
`endif

