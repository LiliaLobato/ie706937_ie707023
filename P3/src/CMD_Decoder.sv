// =============================================================================
// Title        : CMD_Decoder
//                  separates Control Pad from Data pad in the data input
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   P03
// File         :   pipo.sv
// Date         :   02/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
import P3_pkg::*;
module CMD_Decoder#(
	parameter DW = MAX_NUMBER_OF_BITS
)(
	input						clk,
	input						rst,
	input DATA_WORD_t 	Data, //67 bytes
	input 					enable,
	input DATA_BYTE_t 	L,
	
	output logic 		ready,
	output CMD_e			CMD,
	output DATA_BYTE_t	N,
	output DATA_WORD_t 	Out_Data //67 bytes, might not use all memory
);

DATA_WORD_t       rgstr_r     ;
DATA_BYTE_t			L_reg;
DATA_BYTE_t			CMD_reg;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
	 begin
        rgstr_r  <= '0;
		  L_reg <= '0;
    end
	 
	 else
	 begin
	 
		if (enable)
			 begin
				  rgstr_r  <= Data;  //End Flag...D, D, CMD , L
				  L_reg <= L; 
			 end
			 
	 end
	 
end:rgstr_label



assign CMD_reg = rgstr_r >> ((L - 1) * 8); //End Flag...D, D, CMD , L


always_comb 
begin
	case(CMD_e'(CMD_reg))
	
		Set_Matrix_Size: 
		
		begin 
		
			CMD =  Set_Matrix_Size;
			N = rgstr_r >> ((L -2) * 8);
			Out_Data = '0;
			ready = 1'b1;
		
		end
		
		Retransmit: 
		
		begin 
			
			CMD =  Retransmit;
			N = '0;
			Out_Data = '0;
			ready = 1'b1;
			
		end
		
		Start_Data_Cap: 
		
		begin 

			CMD =  Start_Data_Cap;
			N = '0;
			Out_Data = '0;
			ready = 1'b1;
		
		end
		
		Transmition: 
		
		begin 

			CMD =  Transmition;
			N = '0;
			Out_Data = {'0, rgstr_r[DW - 1: 8]};
			ready = 1'b1;
			//Out_Data = {'0, rgstr_r[((L_reg - 2) * 8) - 1: 16]}; // (L-2)*8 - 1 reprents data bytes
		
		end	
		
		INVALID_CMD: //invalid or idle
		
		begin 

			CMD =  INVALID_CMD;
			N = '0;
			Out_Data = '0;
			ready = '0;
		
		end	
	
		default: 
		
		begin

			CMD =  INVALID_CMD;
			N = '0;
			Out_Data = '0;
			ready = '0;
		
		end
		
	endcase
end


endmodule 