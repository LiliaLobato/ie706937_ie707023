// =============================================================================
// Title        : FSM RX UART
//                  edo_siguiente machine for reception
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module fsm_rx
import top_uart_pkg::*;
(
	input		clk,
	input 		rst,
	//overflow flags
	input 		bit_cnt_ovf,
	input 		time_cnt_ovf,
	input		data_in, 	//data input
	input 		clean_in, 	//clean input
	//cnt starts
	output logic		bit_cnt_start,
	output logic		time_cnt_start,
	// enable signals
	output logic		en_sipo,
	output logic		en_parity,
	output logic		en_interrupt,
	output logic		en_error,
	// clean signals
	output logic		clean,
	output logic		clean_out,
	output logic 		start_signal, 	//start
	output logic		ready 			//ready
);

uart_state_e edo_actual;	
uart_state_e edo_siguiente;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      edo_actual  <= IDLE;
   else 
      edo_actual  <= edo_siguiente;
end


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(edo_actual)
		IDLE: begin
			if(!data_in)
				edo_siguiente <= START;
			else
				edo_siguiente <= IDLE;
		end
		START: begin
			if(time_cnt_ovf) //start reception
				edo_siguiente <= SHIFT;
			else
				edo_siguiente <= START;
		end
		SHIFT: begin
			//Receives 8 bits
			if(bit_cnt_ovf && time_cnt_ovf)
				edo_siguiente <= PARITY;
			else
				edo_siguiente <= SHIFT;
		end
		PARITY: begin
			if(time_cnt_ovf) //checks parity
				edo_siguiente <= STOP;
			else
				edo_siguiente <= PARITY;
		end
		STOP: begin
			if(time_cnt_ovf && data_in) //success!
				edo_siguiente <= IDLE;
			else if(!time_cnt_ovf)
				edo_siguiente <= STOP;
			else //error detected
				edo_siguiente <= ERROR;
		end
		ERROR: begin
			if(time_cnt_ovf)
				edo_siguiente <= IDLE;
			else
				edo_siguiente <= ERROR;
		end
		default: begin
			edo_siguiente <= IDLE;
		end
	endcase
end


always_comb begin
	case(edo_actual)
		IDLE: begin //waits for rx tramsaction
			bit_cnt_start	= OFF;
			time_cnt_start	= OFF;
			en_sipo			= OFF;
			en_parity		= OFF;
			en_interrupt 	= OFF;
			en_error 		= OFF;
			ready			= OFF;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		START: begin //reception
			bit_cnt_start 	= (time_cnt_ovf)? (ON) : (OFF);
			time_cnt_start	= ON;
			en_sipo			= OFF;
			en_parity		= OFF;
			en_interrupt 	= OFF;
			en_error 		= OFF;
			ready			= OFF;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		SHIFT: begin
			bit_cnt_start 	= (time_cnt_ovf)? (ON) : (OFF);
			time_cnt_start	= ON;
			en_sipo 		= (time_cnt_ovf)? (ON) : (OFF);
			en_parity		= OFF;
			en_interrupt 	= OFF;
			en_error 		= OFF;
			ready			= OFF;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		PARITY: begin //check parity
			bit_cnt_start	= OFF;
			time_cnt_start	= ON;
			en_sipo			= OFF;
			en_parity 		= ON;
			en_interrupt 	= OFF;
			en_error 		= ON;
			ready			= OFF;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		STOP: begin //finish reception
			bit_cnt_start	= OFF;
			time_cnt_start	= ON;
			en_sipo			= OFF;
			en_parity		= OFF;
			en_interrupt 	= data_in;
			en_error 		= OFF;
			ready			= data_in;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		ERROR: begin //error detected
			bit_cnt_start	= OFF;
			time_cnt_start	= ON;
			en_sipo			= OFF;
			en_parity		= OFF;
			en_interrupt 	= OFF;
			en_error 		= OFF;
			ready			= OFF;
			clean 			= OFF;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
		default: begin
			bit_cnt_start	= OFF;
			time_cnt_start	= ON;
			en_sipo			= OFF;
			en_parity		= OFF;
			en_interrupt 	= OFF;
			en_error 		= OFF;
			ready			= OFF;
			clean 			= ON;
			clean_out 		= clean_in;
			start_signal 	= ON;
		end
	endcase
end

endmodule