// =============================================================================
// Title        : Sipo msb
//               	The serial input will be added to the msb 
//					of the parallel output 
// Inputs       : System clk, reset, enb, serial input
// Outputs      : Parallel output
// Project      :   t05
// File         :   sipo_msb.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          		Gabriel Paz    ie707023
// =============================================================================

module sipo_msb #(
parameter DW = 4
) (
input               clk,
input               rst,
input               enb,
input               inp,
output  [DW-1:0]    out
);

logic [DW-1:0]      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (enb)
        rgstr_r     <= {inp, rgstr_r[DW-1:1]};
end:rgstr_label

assign out  = rgstr_r;

endmodule