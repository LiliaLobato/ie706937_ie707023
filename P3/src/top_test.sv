// =============================================================================
// Title        :   UART
// Inputs       :   clk, rst, rx data, tx flag, tx data 
// Outputs      :   rx data, rx interrupt, parity error
// Project      :   uart
// File         :   top_uart.sv
// Date         :   09/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
import top_uart_pkg::*;
import clk_div_pkg::*;

module top_test 
(
	input 						clk,
	input 						rst,
	input 				 		serial_data_rx,
	output						serial_data_tx,
	output	rx_interrupt, //flag for data received
	input 	clear_interrupt,
	input 	transmit, //start tx
	input	uart_baud_t		parallel_data_tx, //data to transmit
	output	uart_baud_t		parallel_data_rx //data received
);

logic						pll_clk;
logic						clk_tx;


/** PLL_CLK for simulation*/
assign pll_clk = clk;

/** UART TX Clk*/
uart_clk clk_uart_tx (
	.clk(pll_clk),
	.rst(rst),
	.clk_gen(clk_tx)
);

/** UART Module*/
top_uart uart(
	.clk_pll(pll_clk),
	.clk_tx(clk_tx),
	.rst(rst),
	.serial_data_rx(serial_data_rx),
	.clear_interrupt(clear_interrupt),
	.transmit(transmit),
	.parallel_data_tx(parallel_data_tx),

	.parallel_data_rx(parallel_data_rx),
	.rx_interrupt(rx_interrupt),
	.parity_error(),
	.serial_data_tx(serial_data_tx)
);



endmodule : top_test