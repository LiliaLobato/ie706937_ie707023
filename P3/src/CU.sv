// =============================================================================
// Title        : Control Unit
//                  God in this project 
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   P03
// File         :   CU.sv
// Date         :   02/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
module CU
import P3_pkg::*;
(
	input					clk,
	input					rst,
	input 				Start_Flag,
	input					End_Flag,
	input					L_Captured,
	input CMD_e			CMD,
	input DATA_BYTE_t N_Value,
	input 				CMD_Decoded_flag,
	input					Feeder_finished,

	output value_t		L_is_next,
	output value_t		Data_Capture_en,
	output value_t		count_L_times_en,
	output value_t		CMD_Decoder_en,
	output value_t		N_2_Feeder_en,
	output DATA_BYTE_t N_Value_out,
	output value_t		clear_RxInterrupt
);


state_e Edo_Actual;	
state_e edo_siguiente;

logic L_Captured_aux;
logic CMD_Decoded_flag_aux;





// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(Edo_Actual)
      Rx:begin 
        if ( End_Flag )
		  begin
          edo_siguiente = ID;
        end 
		  else 
		  begin
          edo_siguiente = Rx;
        end
      end
		
      ID:begin 
		
			case(CMD_e'(CMD))
			
				Set_Matrix_Size:
				begin
				
					edo_siguiente = IDLE;
				
				end
				
				Retransmit:
				begin
	
					edo_siguiente = Tx;
				
				end
				
				Start_Data_Cap:
				begin

					edo_siguiente = IDLE;
				
				end
				
				Transmition:
				begin
				
					edo_siguiente = ALIMENTADOR;
				
				end
				
				
				default:
				begin
				
					edo_siguiente = ID;
				
				end
			endcase
			
		end
		
		ALIMENTADOR:begin 
        if ( Feeder_finished )
		  begin
          edo_siguiente = PROCESSING;
        end 
		  else 
		  begin
          edo_siguiente = ALIMENTADOR;
        end
      end

      PROCESSING:begin 
        if ( Start_Flag )
		  begin
          edo_siguiente = Rx;
        end 
		  else 
		  begin
          edo_siguiente = IDLE;
        end
      end
		
      COLECTOR:begin 
        if ( Start_Flag )
		  begin
          edo_siguiente = Rx;
        end 
		  else 
		  begin
          edo_siguiente = IDLE;
        end
      end

      Tx:begin 
        if ( Start_Flag )
		  begin
          edo_siguiente = Rx;
        end 
		  else 
		  begin
          edo_siguiente = IDLE;
        end
      end
		
      IDLE:begin 
        if ( Start_Flag )
		  begin
          edo_siguiente = Rx;
        end 
		  else 
		  begin
          edo_siguiente = IDLE;
        end
      end
   endcase
end

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      Edo_Actual  <= IDLE;
   else 
      Edo_Actual  <= edo_siguiente;
end


always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
	begin
      L_Captured_aux  <= OFF;
		N_Value_out <= '0;
   end
	
	else
	begin
	
		if(L_Captured)
			L_Captured_aux  <= ON;
		if(End_Flag)
			L_Captured_aux  <= OFF;
			
		if(CMD_Decoded_flag | (Set_Matrix_Size == CMD)) //this condition guarantees that N_value will be captured just once
			N_Value_out <= N_Value;
			
			
	end
end


// CTO combinacional de salida.
always_comb begin
case(Edo_Actual)
    IDLE : 
	 begin

		//RX_Flags
		clear_RxInterrupt = OFF;
		L_is_next = OFF;
		count_L_times_en = OFF;
		Data_Capture_en = OFF;
		
		//ID_Flags
		CMD_Decoder_en = OFF;
		N_2_Feeder_en = OFF;

	 
    end
	 
    Rx: 
	 begin
	 
		if(!L_Captured_aux) //this condition guarantees that L will be captured just one time per transmition 
		begin
		
			//RX_Flags
			clear_RxInterrupt = ON;
			L_is_next = ON;
			count_L_times_en = ON;
			Data_Capture_en = ON;
			
			//ID_Flags
			CMD_Decoder_en = OFF;
			N_2_Feeder_en = OFF;

			
		end
		
		else
		begin
		
			//RX_Flags
			clear_RxInterrupt = ON;
			L_is_next = OFF;
			count_L_times_en = ON;
			Data_Capture_en = ON;
			
			//ID_Flags
			CMD_Decoder_en = OFF;
			N_2_Feeder_en = OFF;

		
		end
		
    end
	 
    ID: 
	 begin

			case(CMD_e'(CMD))
			
				Set_Matrix_Size:
				begin
				
					//RX_Flags
					clear_RxInterrupt = OFF;
					L_is_next = OFF;
					count_L_times_en = OFF;
					Data_Capture_en = OFF;
					
					//ID_Flags
					CMD_Decoder_en = ON;
					N_2_Feeder_en = OFF; 

				
				end
				
				Retransmit:
				begin
				
					//RX_Flags
					clear_RxInterrupt = OFF;
					L_is_next = OFF;
					count_L_times_en = OFF;
					Data_Capture_en = OFF;
					
					//ID_Flags
					CMD_Decoder_en = ON;
					N_2_Feeder_en = OFF; 

				
				end
				
				Start_Data_Cap:
				begin
				
					//RX_Flags
					clear_RxInterrupt = OFF;
					L_is_next = OFF;
					count_L_times_en = OFF;
					Data_Capture_en = OFF;
					
					//ID_Flags
					CMD_Decoder_en = ON;
					N_2_Feeder_en = OFF; 

				
				end
				
				Transmition:
				begin
				
					//RX_Flags
					clear_RxInterrupt = OFF;
					L_is_next = OFF;
					count_L_times_en = OFF;
					Data_Capture_en = OFF;
					
					//ID_Flags
					CMD_Decoder_en = ON;
					N_2_Feeder_en = ON; 

				
				end
				
				
				default:
				begin
				
					//RX_Flags
					clear_RxInterrupt = OFF;
					L_is_next = OFF;
					count_L_times_en = OFF;
					Data_Capture_en = OFF;
					
					//ID_Flags
					CMD_Decoder_en = ON;
					N_2_Feeder_en = OFF; 

				
				end
			endcase
		
		

		
	 
    end
	 
    PROCESSING: 
	 begin
	 
		//RX_Flags
		clear_RxInterrupt = OFF;
		L_is_next = OFF;
		count_L_times_en = OFF;
		Data_Capture_en = OFF;
		
		//ID_Flags
		CMD_Decoder_en = OFF;
		N_2_Feeder_en = OFF;

	 
    end
	 
    COLECTOR: 
	 begin

		//RX_Flags
		clear_RxInterrupt = OFF;
		L_is_next = OFF;
		count_L_times_en = OFF;
		Data_Capture_en = OFF;
		
		//ID_Flags
		CMD_Decoder_en = OFF;
		N_2_Feeder_en = OFF;

	 
    end
	 
    Tx: 
	 begin

		//RX_Flags
		clear_RxInterrupt = OFF;
		L_is_next = OFF;
		count_L_times_en = OFF;
		Data_Capture_en = OFF;
		
		//ID_Flags
		CMD_Decoder_en = OFF;
		N_2_Feeder_en = OFF;

	 
    end
	 
	 default: 
	 begin

		//RX_Flags
		clear_RxInterrupt = OFF;
		L_is_next = OFF;
		count_L_times_en = OFF;
		Data_Capture_en = OFF;
		
		//ID_Flags
		CMD_Decoder_en = OFF;
		N_2_Feeder_en = OFF;

	 
	 end
	 	 
endcase 
    //default case example: ready_en = (done_en & load_en);
end



endmodule 