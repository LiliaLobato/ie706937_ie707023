
// Coder:       Gabombon
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

module variable_bin_counter #(
parameter 
DW      = 8, 
RST_VAL = 0
)(
input           clk,
input           rst,
input           enb,
input [DW-1:0]	 MAXCNT,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;

always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  RST_VAL[DW-1:0];
    else if (enb)
				count_r     <= count_nxt    ;
			else
				count_r     <=  RST_VAL[DW-1:0];
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
    if (count_r == (MAXCNT + 1'b1)) begin
        ovf       = 1'b1;
        count_nxt = RST_VAL[DW-1:0];
    end else begin
        ovf     =   1'b0;
        count_nxt =  count_r + 1'b1;
    end
end

assign count    =   count_r;

endmodule