// =============================================================================
// Title        :   UART
// Inputs       :   clk, rst, rx data, tx flag, tx data 
// Outputs      :   rx data, rx interrupt, parity error
// Project      :   uart
// File         :   top_uart.sv
// Date         :   09/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module top_uart
import top_uart_pkg::*;
(
	input 	clk_pll,
	input	clk_tx,
	input 	rst,
	input 	serial_data_rx, //uart rx
	input 	clear_interrupt,
	input 	transmit, //start tx
	input	uart_baud_t		parallel_data_tx, //data to transmit
	
	output	uart_baud_t		parallel_data_rx, //data recieved
	output	rx_interrupt, //flag for data received
	output	parity_error,
	output 	serial_data_tx
);

//data out UART
tx_module tx_uart (
	.clk(clk_tx),
	.rst(rst),
	.transmit(transmit),
	.data(parallel_data_tx),
	.serial_output(serial_data_tx)
);

//data in UART
rx_module rx_uart (
	.clk(clk_pll),
	.rst(rst),
	.data(serial_data_rx),
	.clear(clear_interrupt),
	.data_parallel(parallel_data_rx),
	.RX_interrupt(rx_interrupt),
	.parity_error(parity_error)
);


endmodule : top_uart