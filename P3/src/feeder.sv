// =============================================================================
// Title        : Alimentador de datos
//                  sends data to its respective modules.
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   P03
// File         :   feeder.sv
// Date         :   02/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
import P3_pkg::*;
module feeder(
	input						clk,
	input						rst,
	input DATA_WORD_t		data_in, //vergazos de informacion
	input 					enable,
	input DATA_BYTE_t		N,
	input DATA_BYTE_t		Counter_val,
	
	output fifo_demux_t	fifo_demux, 			
	output DATA_BYTE_t	data_2_fifo1,
	output DATA_BYTE_t	data_2_fifo2,
	output DATA_BYTE_t	data_2_fifo3,
	output DATA_BYTE_t	data_2_fifo4,
	output DATA_BYTE_t	data_2_fifo5,
	output DATA_BYTE_t	data_2_fifo6,
	output DATA_BYTE_t	data_2_fifo7,
	output DATA_BYTE_t	data_2_fifo8,
	output DATA_BYTE_t	data_2_fifoV

);

DATA_BYTE_t N_Value;
DATA_BYTE_t count_N_cycles;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
	 begin
        N_Value  <= '0;
		  count_N_cycles <= '0;
    end
	 
	 else
	 begin
	 
		if (enable)
			 begin
				N_Value = N;
				count_N_cycles = Counter_val;
			 end
			 
	 end
	 
end:rgstr_label



always_comb 
begin
	case(N_Value)
	
		1: 
		
		begin 
		
			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0];
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				
				end
			endcase
		
		end
		
		2: 
		
		begin 
			
			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0];
					data_2_fifo2 = data_in[23:16];
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8];
					data_2_fifo2 = data_in[31:24];
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				
				end
			endcase
			
		end
		
		3: 
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[31:24]; //f2 1er byte (4to byte)
					data_2_fifo3 = data_in[55:48];
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; //2do byte
					data_2_fifo2 = data_in[39:32]; //f2 2do byte
					data_2_fifo3 = data_in[63:56];
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; //3er byte
					data_2_fifo2 = data_in[47:40]; //f2 3er byte
					data_2_fifo3 = data_in[71:64];
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				
				end
			endcase
		
		end
		
		4: 
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[39:32]; 
					data_2_fifo3 = data_in[71:64];
					data_2_fifo4 = data_in[103:96];
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; //2do byte
					data_2_fifo2 = data_in[47:40]; //f2 2do byte
					data_2_fifo3 = data_in[79:72];
					data_2_fifo4 = data_in[111:104];
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; //3er byte
					data_2_fifo2 = data_in[55:48]; //f2 3er byte
					data_2_fifo3 = data_in[87:80];
					data_2_fifo4 = data_in[119:112];
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4;
				
				end
				
				4:
				begin
				
					data_2_fifo1 = data_in[31:24]; 
					data_2_fifo2 = data_in[63:56]; 
					data_2_fifo3 = data_in[95:88];
					data_2_fifo4 = data_in[127:120];
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				
				end
			endcase
		
		end	
		
		5: //invalid or idle
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[47:40]; 
					data_2_fifo3 = data_in[87:80];
					data_2_fifo4 = data_in[127:120];
					data_2_fifo5 = data_in[167:160];
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; 
					data_2_fifo2 = data_in[55:48]; 
					data_2_fifo3 = data_in[95:88];
					data_2_fifo4 = data_in[135:128];
					data_2_fifo5 = data_in[175:168];
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; 
					data_2_fifo2 = data_in[63:56]; 
					data_2_fifo3 = data_in[103:96];
					data_2_fifo4 = data_in[143:136];
					data_2_fifo5 = data_in[183:176];
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5;
				
				end
				
				4:
				begin
				
					data_2_fifo1 = data_in[31:24]; 
					data_2_fifo2 = data_in[71:64]; 
					data_2_fifo3 = data_in[111:104];
					data_2_fifo4 = data_in[151:144];
					data_2_fifo5 = data_in[191:184];
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5;
				
				end
				
				5:
				begin
				
					data_2_fifo1 = data_in[39:32]; 
					data_2_fifo2 = data_in[79:72]; 
					data_2_fifo3 = data_in[119:112];
					data_2_fifo4 = data_in[159:152];
					data_2_fifo5 = data_in[199:192];
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				end
			endcase
		
		end
	
		6: 
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[55:48]; 
					data_2_fifo3 = data_in[103:96];
					data_2_fifo4 = data_in[151:144];
					data_2_fifo5 = data_in[199:192];
					data_2_fifo6 = data_in[247:240];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; 
					data_2_fifo2 = data_in[63:56]; 
					data_2_fifo3 = data_in[111:104];
					data_2_fifo4 = data_in[159:152];
					data_2_fifo5 = data_in[207:200];
					data_2_fifo6 = data_in[255:248];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; 
					data_2_fifo2 = data_in[71:64]; 
					data_2_fifo3 = data_in[119:112];
					data_2_fifo4 = data_in[167:160];
					data_2_fifo5 = data_in[215:208];
					data_2_fifo6 = data_in[263:256];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				4:
				begin
				
					data_2_fifo1 = data_in[31:24]; 
					data_2_fifo2 = data_in[79:72]; 
					data_2_fifo3 = data_in[127:120];
					data_2_fifo4 = data_in[175:168];
					data_2_fifo5 = data_in[223:216];
					data_2_fifo6 = data_in[271:264];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				5:
				begin
				
					data_2_fifo1 = data_in[39:32]; 
					data_2_fifo2 = data_in[87:80]; 
					data_2_fifo3 = data_in[135:128];
					data_2_fifo4 = data_in[183:176];
					data_2_fifo5 = data_in[231:224];
					data_2_fifo6 = data_in[279:272];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				6:
				begin
				
					data_2_fifo1 = data_in[47:40]; 
					data_2_fifo2 = data_in[95:88]; 
					data_2_fifo3 = data_in[143:136];
					data_2_fifo4 = data_in[191:184];
					data_2_fifo5 = data_in[239:232];
					data_2_fifo6 = data_in[287:280];
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				end
			endcase
		
		end
	
		7: 
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[63:56]; 
					data_2_fifo3 = data_in[119:112];
					data_2_fifo4 = data_in[175:168];
					data_2_fifo5 = data_in[231:224];
					data_2_fifo6 = data_in[287:280];
					data_2_fifo7 = data_in[343:336];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; 
					data_2_fifo2 = data_in[71:64]; 
					data_2_fifo3 = data_in[127:120];
					data_2_fifo4 = data_in[183:176];
					data_2_fifo5 = data_in[239:232];
					data_2_fifo6 = data_in[295:288];
					data_2_fifo7 = data_in[351:344];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; 
					data_2_fifo2 = data_in[79:72]; 
					data_2_fifo3 = data_in[135:128];
					data_2_fifo4 = data_in[191:184];
					data_2_fifo5 = data_in[247:240];
					data_2_fifo6 = data_in[303:296];
					data_2_fifo7 = data_in[359:352];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				4:
				begin
				
					data_2_fifo1 = data_in[31:24]; 
					data_2_fifo2 = data_in[87:80]; 
					data_2_fifo3 = data_in[143:136];
					data_2_fifo4 = data_in[199:192];
					data_2_fifo5 = data_in[255:248];
					data_2_fifo6 = data_in[311:304];
					data_2_fifo7 = data_in[367:360];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				5:
				begin
				
					data_2_fifo1 = data_in[39:32]; 
					data_2_fifo2 = data_in[95:88]; 
					data_2_fifo3 = data_in[151:144];
					data_2_fifo4 = data_in[207:200];
					data_2_fifo5 = data_in[263:256];
					data_2_fifo6 = data_in[319:312];
					data_2_fifo7 = data_in[375:368];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				6:
				begin
				
					data_2_fifo1 = data_in[47:40]; 
					data_2_fifo2 = data_in[103:96]; 
					data_2_fifo3 = data_in[159:152];
					data_2_fifo4 = data_in[215:208];
					data_2_fifo5 = data_in[271:264];
					data_2_fifo6 = data_in[327:320];
					data_2_fifo7 = data_in[383:376];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				7:
				begin
				
					data_2_fifo1 = data_in[55:48]; 
					data_2_fifo2 = data_in[111:104]; 
					data_2_fifo3 = data_in[167:160];
					data_2_fifo4 = data_in[223:216];
					data_2_fifo5 = data_in[279:272];
					data_2_fifo6 = data_in[335:328];
					data_2_fifo7 = data_in[391:384];
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				end
			endcase
		
		end	
	
		8: 
		
		begin 

			case(count_N_cycles)
				1:
				begin
				
					data_2_fifo1 = data_in[7:0]; //Primer byte
					data_2_fifo2 = data_in[71:64]; 
					data_2_fifo3 = data_in[135:128];
					data_2_fifo4 = data_in[199:192];
					data_2_fifo5 = data_in[263:256];
					data_2_fifo6 = data_in[327:320];
					data_2_fifo7 = data_in[391:384];
					data_2_fifo8 = data_in[455:448];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				2:
				begin
				
					data_2_fifo1 = data_in[15:8]; 
					data_2_fifo2 = data_in[79:72]; 
					data_2_fifo3 = data_in[143:136];
					data_2_fifo4 = data_in[207:200];
					data_2_fifo5 = data_in[271:264];
					data_2_fifo6 = data_in[335:328];
					data_2_fifo7 = data_in[399:392];
					data_2_fifo8 = data_in[463:456];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				3:
				begin
				
					data_2_fifo1 = data_in[23:16]; 
					data_2_fifo2 = data_in[87:80]; 
					data_2_fifo3 = data_in[151:144];
					data_2_fifo4 = data_in[215:208];
					data_2_fifo5 = data_in[279:272];
					data_2_fifo6 = data_in[343:336];
					data_2_fifo7 = data_in[407:400];
					data_2_fifo8 = data_in[471:464];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				4:
				begin
				
					data_2_fifo1 = data_in[31:24]; 
					data_2_fifo2 = data_in[95:88]; 
					data_2_fifo3 = data_in[159:152];
					data_2_fifo4 = data_in[223:216];
					data_2_fifo5 = data_in[287:280];
					data_2_fifo6 = data_in[351:344];
					data_2_fifo7 = data_in[415:408];
					data_2_fifo8 = data_in[479:472];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				5:
				begin
				
					data_2_fifo1 = data_in[39:32]; 
					data_2_fifo2 = data_in[103:96]; 
					data_2_fifo3 = data_in[167:160];
					data_2_fifo4 = data_in[231:224];
					data_2_fifo5 = data_in[295:288];
					data_2_fifo6 = data_in[359:352];
					data_2_fifo7 = data_in[423:416];
					data_2_fifo8 = data_in[487:480];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				6:
				begin
				
					data_2_fifo1 = data_in[47:40]; 
					data_2_fifo2 = data_in[111:104]; 
					data_2_fifo3 = data_in[175:168];
					data_2_fifo4 = data_in[239:232];
					data_2_fifo5 = data_in[303:296];
					data_2_fifo6 = data_in[367:360];
					data_2_fifo7 = data_in[431:424];
					data_2_fifo8 = data_in[495:488];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				7:
				begin
				
					data_2_fifo1 = data_in[55:48]; 
					data_2_fifo2 = data_in[119:112]; 
					data_2_fifo3 = data_in[183:176];
					data_2_fifo4 = data_in[247:240];
					data_2_fifo5 = data_in[311:304];
					data_2_fifo6 = data_in[375:368];
					data_2_fifo7 = data_in[439:432];
					data_2_fifo8 = data_in[503:496];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				8:
				begin
				
					data_2_fifo1 = data_in[63:56]; 
					data_2_fifo2 = data_in[127:120]; 
					data_2_fifo3 = data_in[191:184];
					data_2_fifo4 = data_in[255:248];
					data_2_fifo5 = data_in[319:312];
					data_2_fifo6 = data_in[383:376];
					data_2_fifo7 = data_in[447:440];
					data_2_fifo8 = data_in[511:504];
					
					data_2_fifoV = '0;
					
					fifo_demux	= FIFO_N1 | FIFO_N2 | FIFO_N3 | FIFO_N4 | FIFO_N5 | FIFO_N6 | FIFO_N7 | FIFO_N8;
				
				end
				
				default:
				begin
				
					data_2_fifo1 = '0;
					data_2_fifo2 = '0;
					data_2_fifo3 = '0;
					data_2_fifo4 = '0;
					data_2_fifo5 = '0;
					data_2_fifo6 = '0;
					data_2_fifo7 = '0;
					data_2_fifo8 = '0;
					
					data_2_fifoV = '0;
					
					fifo_demux	= NO_FIFO;
				end
			endcase
		
		end		
	
	
		default: 
		
		begin

				data_2_fifo1 = '0;
				data_2_fifo2 = '0;
				data_2_fifo3 = '0;
				data_2_fifo4 = '0;
				data_2_fifo5 = '0;
				data_2_fifo6 = '0;
				data_2_fifo7 = '0;
				data_2_fifo8 = '0;
					
				data_2_fifoV = '0;
					
				fifo_demux	= NO_FIFO;
		
		end
		
	endcase
end
endmodule