// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module oneShot
import top_uart_pkg::*;
(
	input		clk,
	input 		rst,
	input		inp,
	output logic out
);

os_state_e edo_siguiente;
os_state_e edo_actual;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      edo_actual  <= WAIT_TRUE;
   else 
      edo_actual  <= edo_siguiente;
end

// Circuito combinacional entrada, define ssiguiente estado
always_comb begin
	case(edo_actual)
		WAIT_TRUE: begin
			edo_siguiente <= inp?SHOT:WAIT_TRUE;
		end
		SHOT: begin
			edo_siguiente <= WAIT_FALSE;
		end
		WAIT_FALSE: begin
			edo_siguiente <= inp?WAIT_FALSE:WAIT_TRUE;
		end
		default: begin
			edo_siguiente <= WAIT_TRUE;
		end
	endcase
end

always_comb begin
	case(edo_actual)
		WAIT_TRUE: begin //wait input
			out = OFF;
		end
		SHOT: begin //send en clk 2
			out = ON;
		end
		WAIT_FALSE: begin //wait input change
			out = OFF;
		end
		default: begin
			out = OFF;
		end
	endcase
end

	

endmodule