/******************************************************************
* Module
*  tb_p03.sv
* Description
*	This is Test Bench for P03
* Version:
*	1.0
* Author:
*	Ricardo Delsordo
* email:
*	ie702570@iteso.mx
* Date:
*	29/05/2019
******************************************************************/

`timescale 10ns / 1ps

module tb_uart;
import fsm_moore_pkg::*;
import clk_div_pkg::*;
import P3_pkg::*;
import UART_pkg::*;
import top_uart_pkg::*;

	logic 	clk;
	logic 	rst;
	logic 	serial_data_rx;
	logic	serial_data_tx;
	logic	rx_interrupt; //flag for data received
	logic 	clear_interrupt;
	logic 	transmit; //start tx
	uart_baud_t		parallel_data_tx; //data to transmit
	uart_baud_t		parallel_data_rx; //data received

	logic	PC_2_Rx_data;
	logic	ready;
	logic	Tx_2_PC_Data;

/** Module initialization*/
top_test uut (
	.clk(clk),
	.rst(rst),
	.serial_data_rx(serial_data_rx),
	.serial_data_tx(serial_data_tx),
	.rx_interrupt(rx_interrupt), //flag for data received
	.clear_interrupt(clear_interrupt),
	.transmit(transmit), //start tx
	.parallel_data_tx(parallel_data_tx), //data to transmit
	.parallel_data_rx(parallel_data_rx) //data received
);

MM_top uut2(
	.clk(clk),
	.rst(rst),
	.PC_2_Rx_data(serial_data_rx),
	.ready(ready), //not sure if we need it
	.Tx_2_PC_Data(Tx_2_PC_Data)

);


/*********************************************************/
initial
begin
	/** Begins test*/
	clk   = 0;
	rst = 0;
	clear_interrupt = 0;
	transmit = 0;
	parallel_data_tx = '0;

	#6 rst = 1;

	//RX TEST
	send_rx();
	delay_clk(50);

	//TX TEST
	send_tx();
	delay_clk(50);
	
	delay_clk(500);
	$stop;

end

always begin
    #1 clk <= ~clk;
end


//Send rx
task send_rx();
//Matrix size
	data_send_rx(8'hFE);
	data_send_rx(8'h03);
	data_send_rx(8'h01);
	data_send_rx(8'h02);
	data_send_rx(8'hEF);

//IDLE BUS
	data_send_rx(8'hFF);
	data_send_rx(8'hFF);  

//Transmit 
	data_send_rx(8'hFE);
	data_send_rx(8'h06);
	data_send_rx(8'h04);
	data_send_rx(8'hF0);
	data_send_rx(8'h04);
	data_send_rx(8'hF0);
	data_send_rx(8'h01);
	data_send_rx(8'hEF); 

//Data cap
//	data_send_rx(8'hFE);
//	data_send_rx(8'h02);
//	data_send_rx(8'h03);
//	data_send_rx(8'hEF); 

//retransmit
//	data_send_rx(8'hFE);
//	data_send_rx(8'h02);
//	data_send_rx(8'h02);
//	data_send_rx(8'hEF); 

//Matrix size
//	data_send_rx(8'hFE);
//	data_send_rx(8'h03);
//	data_send_rx(8'h01);
//	data_send_rx(8'h05);
//	data_send_rx(8'hEF);
endtask

//Send tx
task send_tx();
	data_send_tx(8'hFE);
	data_send_tx(8'h02);
	data_send_tx(8'h03);
	data_send_tx(8'hEF);
endtask

//Byte to send with UART
task data_send_rx(uart_baud_t	uart_in = 8'h01);
	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = uart_in[0];
	#8 serial_data_rx = uart_in[1];
	#8 serial_data_rx = uart_in[2];
	#8 serial_data_rx = uart_in[3];

	#8 serial_data_rx = uart_in[4];
	#8 serial_data_rx = uart_in[5];
	#8 serial_data_rx = uart_in[6];
	#8 serial_data_rx = uart_in[7];

	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = 1'b1;

	clear_interrupt = 1;
	#8 clear_interrupt = 0;
endtask

//Byte to send with UART
task data_send_tx(uart_baud_t	uart_in = 8'h01);
	#8 parallel_data_tx = 1'b0;

	#8 parallel_data_tx = uart_in;

	transmit = 1;
	#8 transmit = 0;
	delay_clk(50);
endtask

//delay
task delay_clk(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask



/*********************************************************/
endmodule
