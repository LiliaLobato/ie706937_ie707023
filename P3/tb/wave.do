onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART CLK}
add wave -noupdate /tb_uart/uut/clk
add wave -noupdate /tb_uart/uut/rst
add wave -noupdate -divider {UART System}
add wave -noupdate -color Violet /tb_uart/uut/serial_data_rx
add wave -noupdate -color Violet /tb_uart/uut/rx_interrupt
add wave -noupdate -color Violet -radix hexadecimal /tb_uart/uut/parallel_data_rx
add wave -noupdate /tb_uart/uut/clear_interrupt
add wave -noupdate /tb_uart/uut/clk_uart_tx/clk_gen
add wave -noupdate -color {Slate Blue} -radix hexadecimal /tb_uart/uut/parallel_data_tx
add wave -noupdate -color {Slate Blue} /tb_uart/uut/transmit
add wave -noupdate -color {Slate Blue} /tb_uart/uut/serial_data_tx
add wave -noupdate -divider {RX UART}
add wave -noupdate /tb_uart/uut/uart/rx_uart/fsm_rx/edo_actual
add wave -noupdate /tb_uart/uut/uart/rx_uart/data
add wave -noupdate /tb_uart/uut2/P3_UART/rx_uart/sipo_out
add wave -noupdate /tb_uart/uut/uart/rx_uart/clear
add wave -noupdate -radix hexadecimal /tb_uart/uut/uart/rx_uart/data_parallel
add wave -noupdate /tb_uart/uut/uart/rx_uart/RX_interrupt
add wave -noupdate /tb_uart/uut/uart/rx_uart/parity_error
add wave -noupdate -divider {TX UART}
add wave -noupdate /tb_uart/uut/uart/tx_uart/fsm_tx/edo_actual
add wave -noupdate /tb_uart/uut/uart/tx_uart/transmit
add wave -noupdate -radix hexadecimal -childformat {{{/tb_uart/uut/uart/tx_uart/data[7]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[6]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[5]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[4]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[3]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[2]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[1]} -radix hexadecimal} {{/tb_uart/uut/uart/tx_uart/data[0]} -radix hexadecimal}} -expand -subitemconfig {{/tb_uart/uut/uart/tx_uart/data[7]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[6]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[5]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[4]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[3]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[2]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[1]} {-height 15 -radix hexadecimal} {/tb_uart/uut/uart/tx_uart/data[0]} {-height 15 -radix hexadecimal}} /tb_uart/uut/uart/tx_uart/data
add wave -noupdate /tb_uart/uut/uart/tx_uart/serial_output
add wave -noupdate -divider P3_UART
add wave -noupdate /tb_uart/uut2/P3_UART/clear_interrupt
add wave -noupdate /tb_uart/uut2/P3_UART/rx_interrupt
add wave -noupdate /tb_uart/uut2/P3_UART/rx_uart/data
add wave -noupdate /tb_uart/uut2/P3_UART/rx_uart/sipo_out
add wave -noupdate /tb_uart/uut2/P3_UART/parallel_data_rx
add wave -noupdate /tb_uart/uut2/P3_UART/rx_uart/data_parallel
add wave -noupdate /tb_uart/uut2/P3_UART/parallel_data_tx
add wave -noupdate -divider L_times_counter
add wave -noupdate /tb_uart/uut2/count_L_times/clk
add wave -noupdate /tb_uart/uut2/count_L_times/enb
add wave -noupdate /tb_uart/uut2/count_L_times/MAXCNT
add wave -noupdate /tb_uart/uut2/count_L_times/ovf
add wave -noupdate /tb_uart/uut2/count_L_times/count_r
add wave -noupdate -label L_VALUE /tb_uart/uut2/pipo_L/out
add wave -noupdate -divider enables
add wave -noupdate -label Start_Comp_en /tb_uart/uut2/Start_Flag_Comp/enb
add wave -noupdate -label End_comp_en /tb_uart/uut2/end_Flag_Comp/enb
add wave -noupdate -label Capture_L_en /tb_uart/uut2/pipo_L/enb
add wave -noupdate -divider Flags
add wave -noupdate -label Comparator_Out_Start_Flag /tb_uart/uut2/Start_Flag_Comp/out
add wave -noupdate -label Comparator_Out_End_Flag /tb_uart/uut2/end_Flag_Comp/out
add wave -noupdate -divider Start_Comp
add wave -noupdate -radix hexadecimal /tb_uart/uut2/Start_Flag_Comp/in_A
add wave -noupdate -radix hexadecimal /tb_uart/uut2/Start_Flag_Comp/in_B
add wave -noupdate -divider CU
add wave -noupdate /tb_uart/uut2/god/Edo_Actual
add wave -noupdate /tb_uart/uut2/god/Start_Flag
add wave -noupdate /tb_uart/uut2/god/End_Flag
add wave -noupdate /tb_uart/uut2/god/L_Captured
add wave -noupdate /tb_uart/uut2/god/CMD
add wave -noupdate /tb_uart/uut2/god/L_is_next
add wave -noupdate /tb_uart/uut2/god/Data_Capture_en
add wave -noupdate /tb_uart/uut2/god/count_L_times_en
add wave -noupdate /tb_uart/uut2/god/CMD_Decoder_en
add wave -noupdate /tb_uart/uut2/god/clear_RxInterrupt
add wave -noupdate /tb_uart/uut2/god/N_Value_out
add wave -noupdate -divider sipo_word_message
add wave -noupdate /tb_uart/uut2/word_message/inp
add wave -noupdate /tb_uart/uut2/word_message/enb
add wave -noupdate /tb_uart/uut2/word_message/out
add wave -noupdate -divider CMD_Deco
add wave -noupdate /tb_uart/uut2/instruction_decode/CMD_reg
add wave -noupdate /tb_uart/uut2/instruction_decode/Data
add wave -noupdate /tb_uart/uut2/instruction_decode/enable
add wave -noupdate /tb_uart/uut2/instruction_decode/CMD
add wave -noupdate /tb_uart/uut2/instruction_decode/N
add wave -noupdate /tb_uart/uut2/instruction_decode/Out_Data
add wave -noupdate /tb_uart/uut2/instruction_decode/ready
add wave -noupdate /tb_uart/uut2/instruction_decode/L
add wave -noupdate /tb_uart/uut2/instruction_decode/L_reg
add wave -noupdate -divider N_counter
add wave -noupdate /tb_uart/uut2/count_N_times/MAXCNT
add wave -noupdate /tb_uart/uut2/count_N_times/enb
add wave -noupdate /tb_uart/uut2/count_N_times/count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {8397568 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 274
configure wave -valuecolwidth 65
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {8337475 ps} {9563705 ps}
