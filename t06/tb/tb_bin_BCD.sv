// =============================================================================
// Title       	:	Implementación de un decodificador binario en sistema complemento a 2
//					a su equivalente en unidades, decenas y centenas
// Inputs		:	8 Switches
// Outputs		:	3 displays de 7 segmentos
// Project     	: 	t01
// File        	: 	tb_bin_BCD.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`timescale 1ns / 1ps
module tb_bin_BCD();
import bin_BCD_pkg::*;


// SW
dataIn_t 	BinIn;
// 7 SEG
n_segment_t n_seg;
logic signo;

bin_BCD_top uut (
	.BinIn 		(BinIn),
    .n_seg      (n_seg),
    .signo      (signo)
);

initial begin
        
        BinIn =  0;

        //32 bits
        // BinIn =  2147483647; #2
        // BinIn = -2030946364; #2
        // BinIn =     1287310; #2
        // BinIn = -1723638127; #2

        repeat (2**BIT_IN)
        #1 BinIn = BinIn + 'd1;

        #50
    $stop;
end

endmodule
