onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /tb_bin_BCD/uut/BinIn
add wave -noupdate -expand /tb_bin_BCD/uut/n_seg
add wave -noupdate /tb_bin_BCD/uut/signo
add wave -noupdate -divider {DD deconcatener}
add wave -noupdate -color Plum -radix decimal -childformat {{{/tb_bin_BCD/uut/BinIn[31]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[30]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[29]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[28]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[27]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[26]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[25]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[24]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[23]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[22]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[21]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[20]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[19]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[18]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[17]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[16]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[15]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[14]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[13]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[12]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[11]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[10]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[9]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[8]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[7]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[6]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[5]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[4]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[3]} -radix decimal} {{/tb_bin_BCD/uut/BinIn[2]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[1]} -radix unsigned} {{/tb_bin_BCD/uut/BinIn[0]} -radix unsigned}} -subitemconfig {{/tb_bin_BCD/uut/BinIn[31]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[30]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[29]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[28]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[27]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[26]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[25]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[24]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[23]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[22]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[21]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[20]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[19]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[18]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[17]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[16]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[15]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[14]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[13]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[12]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[11]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[10]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[9]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[8]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[7]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[6]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[5]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[4]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[3]} {-color Plum -radix decimal} {/tb_bin_BCD/uut/BinIn[2]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[1]} {-color Plum -radix unsigned} {/tb_bin_BCD/uut/BinIn[0]} {-color Plum -radix unsigned}} /tb_bin_BCD/uut/BinIn
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[20]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[19]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[18]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[17]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[16]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[15]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[14]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[13]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[12]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[11]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[10]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[9]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[8]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[7]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[6]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[5]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[4]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[3]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[2]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[1]/segment_s/dec_num}
add wave -noupdate -color Plum -radix unsigned -radixshowbase 0 {/tb_bin_BCD/uut/register_[0]/segment_s/dec_num}
add wave -noupdate -divider debug
add wave -noupdate /bin_BCD_pkg::SEG_OUT_N
add wave -noupdate /tb_bin_BCD/uut/BinIn_bin
add wave -noupdate /tb_bin_BCD/uut/n_dd
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3090 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 203
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {78608 ps} {101126 ps}
