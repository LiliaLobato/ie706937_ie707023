// =============================================================================
// Title       	:	Implementación de un decodificador binario en C2
//					a su equivalente en decimal con display a 7 segmentos
// Inputs		:	N bits number
// Outputs		:	N displays de 7 segmentos
// Project     	: 	t06
// File        	: 	bin_BCD_top.sv
// Date 		: 	08/03/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module bin_BCD_top
import bin_BCD_pkg::*;
(
	// SW
	input	dataIn_t BinIn,
	// 7 SEG
	output n_segment_t n_seg,
	output logic signo
);

// Wire data_seg
n_adder_t n_dd;
dataIn_t BinIn_bin;

//Conversión C2 a Binario
C22Bin #(
.DW(BIT_IN)
) C22Bin(
	.complemento2(BinIn),
	.binario     (BinIn_bin),
	.signo(signo)
);

genvar i,j;
integer k;

//init para no generar alta impedancia
always_comb begin
	for (k=1;k<SEG_OUT_N+1;k++) begin
		n_dd[BIT_IN][k] = {((BIT_IN_H*SEG_OUT_N)+BIT_IN){1'b0}};
	end
	//el primer valor es el valor binario concatenado con 0
	n_dd[0][0] = {{((BIT_IN_H*SEG_OUT_N)+1){1'b0}},BinIn_bin};
end

//Algoritmo DD
generate
	for (j=0;j<BIT_IN;j++) begin: dd_
		
		//Se requiere un adder por cifra significativa
		for (i=0;i<SEG_OUT_N;i++) begin: adder_
		//adder N
		adderN #(
			.CNT(i)
		)adderN_(
			//el valor se guarda en el siguiente valor de la 2da dimensión
			.inp(n_dd[j][i]),
			.out(n_dd[j][i+1])
		);
		end

		//Shifter
		shifter shifter_ (
			//el valor se guarda en el siguiente valor de la 1ra dimensión
			.inp(n_dd[j][SEG_OUT_N]),
			.out(n_dd[j+1][0])
		);
	end
endgenerate
	
//Se requiere un display de 7 segmentos por cada cifra significativa
generate
	for(i =0;i<SEG_OUT_N; i++) begin:register_
		bin_BCD_display segment_s
			( 
				.dec_num(n_dd [BIT_IN] [0] [(BIT_IN_H*(i+1) + BIT_IN)-1:(BIT_IN_H*i) + BIT_IN]),
				.seg_num(n_seg[i])   
			);
	end
endgenerate

endmodule