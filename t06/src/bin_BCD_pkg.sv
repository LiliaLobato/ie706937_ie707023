// =============================================================================
// Title       	:	Package
// Project     	: 	t06
// File        	: 	bin_BCD_pkg.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

`ifndef BIN_BCD_PKG_SV
`define BIN_BCD_PKG_SV

package bin_BCD_pkg;

	localparam BIT_IN = 8;//8;
	// 0 // 0  1  2  3  -> 1
	// 1 // 4  5  6 -> 2
	// 2 // 7  8  9 -> 3
	// 3 // 10 11 12 13 -> 4
	// 4 // 14 15 16 -> 5
	// 5 // 17 18 19 -> 6
	// 6 // 20 21 22 23 -> 7
	// 7 // 24 25 26 -> 8
	// 8 // 27 28 29 -> 9
	// 9 // 30 31 32 33 -> 10
	// 10 // 34 35 36 -> 11
	// 11 // 37 38 39 -> 12	
	// 12 // 40 41 42 43 -> 13
	// 13 // 44 45 46 -> 14
	// 14 // 47 48 49 -> 15
	// 15 // 50 51 52 53 -> 16
	localparam SEG_OUT = 7;
	localparam BIT_IN_H = 4;
	localparam CNT_DW = $clog2(BIT_IN-1);
	//function to calculate the number of displays.
	function int N_display(input int a,b);
		do begin
			a = (b%3 == 0)?a+4:a+3;
			b++;
		end while (a<BIT_IN+1);
		N_display = b;
	endfunction
	localparam SEG_OUT_N = N_display(0,0);
	localparam CNT_SEG_DW = $clog2(SEG_OUT_N-1);

	typedef logic [BIT_IN-1:0]					dataIn_t; 	//holds N bits
	typedef logic [BIT_IN_H-1:0]				dataDec_t; 	//holds 4 bits
	typedef logic [SEG_OUT-1:0]					segment_t;	//holds 7 bits
	typedef logic [SEG_OUT_N-1:0] [SEG_OUT-1:0] n_segment_t;//holds [M segments][7 bits]

	//types para algoritmo Double Dabbe
	//holds the size of the Double Dabble variable
	typedef logic [(BIT_IN_H*SEG_OUT_N + BIT_IN )-1:0]		dataDD_t;
	//holds [N bits] [M segments] [size of the Double Dabble variable]
	typedef logic [BIT_IN:0] [SEG_OUT_N:0] [(BIT_IN_H*SEG_OUT_N + BIT_IN )-1:0]	n_adder_t;
	
	
endpackage

`endif