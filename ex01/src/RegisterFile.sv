
module RegisterFile
#(
    parameter N=32
)(
    input           clk,
    input           reset,
    input           RegWrite,
    input   [4:0]   WriteRegister,
    input   [4:0]   ReadRegister1,
    input   [4:0]   ReadRegister2,
    input   [31:0]  WriteData,
    output  [31:0]  ReadData1,
    output  [31:0]  ReadData2
 );


typedef logic [N-1:0] [N-1:0]	dataPIPO_t;
typedef logic [N:0] RegWrite_t;
//La primera guarda el tamaño del dato de la pipo y la segunda es para el índice
dataPIPO_t PIPO_out;
RegWrite_t RegWrite_mux;


genvar j;
integer k;

//Instanciación de N PIPOS
generate
	for (j=0;j<N;j++) begin: dd_
	pipo #(
	.DW(N)
	) pipo_(
		.clk(clk),
		.rst(reset),
		.enb(RegWrite_mux[j]),
		.inp(WriteData),
		.out(PIPO_out[j])
	);
	end

endgenerate

//init para no generar alta impedancia
always_comb begin
		ReadData1 = PIPO_out[ReadRegister1];
		ReadData2 = PIPO_out[ReadRegister2];
end
				
always_comb begin
	//se genera un barrido para poder 
	//darle valor a todos los enable de los registros
	for (k=0;k<N;k++) begin
		RegWrite_mux[k] = 0;
	end

		RegWrite_mux[WriteRegister] = RegWrite? 1:0;
end


endmodule