// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module synch_block (
	input		clk,
	input 		rst,
	input		n_synch_pop,
	input		n_synch_pop2,
	input		n_synch_push,
	input		n_synch_end,
	output logic synch_pop,
	output logic synch_pop2,
	output logic synch_push,
	output logic synch_end
);

synch sync_pop (
	.clk(clk),
	.rst(rst),
	.n_synch(n_synch_pop),
	.synch(synch_pop)
);

synch sync_pop_2 (
	.clk(clk),
	.rst(rst),
	.n_synch(n_synch_pop2),
	.synch(synch_pop2)
);

synch sync_push(
	.clk(clk),
	.rst(rst),
	.n_synch(n_synch_push),
	.synch(synch_push)
);

synch sync_end_proc(
	.clk(clk),
	.rst(rst),
	.n_synch(n_synch_end),
	.synch(synch_end)
);

endmodule