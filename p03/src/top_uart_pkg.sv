`ifndef TOP_UART_PKG_SV
	`define TOP_UART_PKG_SV

package top_uart_pkg;

 ////////////////////////
//DATA TYPES FOR MXV  //
///////////////////////
// Frame size
localparam DW_PC = 8;
localparam DW_RES = 20;//(DW_PC+1)*2;
localparam HDW_PC = DW_PC/2;
localparam CNT_DW_PC = $clog2(DW_PC)+1;
localparam CNT_HDW_PC = $clog2(HDW_PC)+1;
typedef logic [DW_PC-1:0] uart_baud_t;
typedef logic signed [DW_RES-1:0] result_t;

 ////////////////////////
//      UART CMD      //
///////////////////////
localparam START_CMD	 = 8'hFE;
localparam END_CMD		 = 8'hEF;
localparam MTRX_SIZE_CMD = 8'h01;
localparam RETRMT_CMD	 = 8'h02;
localparam START_PROC_CMD	 = 8'h03;
localparam DATA_CMD		 = 8'h04;
localparam UART_COUNT_TX = 8'd18;
localparam FAIL_CYCLES	 = 18'd500;
localparam CNT_BYTE 	 = 6;

 ////////////////////////
//      UART FSM      //
///////////////////////
// tX FSM 
localparam TX_OUT_W = 2;
typedef enum logic [TX_OUT_W-1:0]
{
	START_OR_STOP 	= 2'b00,
	DATA_OUT		= 2'b01,
	PARITY_BIT		= 2'b10,
	IDLE_BIT 		= 2'b11
}tx_out_e;

//uart state
localparam SM_W = 3;
typedef enum logic [SM_W-1:0]
{
	IDLE		= 3'b000,
	START		= 3'b001,
	SHIFT		= 3'b010,
	PARITY		= 3'b011,
	STOP		= 3'b100,
	ERROR		= 3'b101
}uart_state_e;

 ////////////////////////
//     SYNCH FSM      //
///////////////////////

//Anti OneShot
localparam AOS_W = 3;
typedef enum logic [AOS_W-1:0]
{
	IDLE_AS		= 3'b000,
	CYCLE_1_AS	= 3'b001,
	CYCLE_2_AS	= 3'b010,
	CYCLE_3_AS	= 3'b011,
	CYCLE_4_AS	= 3'b100
}aos_state_e;

// oneShot
localparam OS_W = 2;
typedef enum logic [OS_W-1:0]
{
	WAIT_TRUE	= 2'b00,
	SHOT		= 2'b01,
	WAIT_FALSE	= 2'b10
}os_state_e;

 ////////////////////////
//       MXV FSM      //
///////////////////////

// mxv state
localparam MXV_W = 3;
typedef enum logic [MXV_W-1:0]
{
	IDLE_MXV   	= 3'b000,
	START_MXV 	= 3'b001,
	PROCC_MXV  	= 3'b010,
	FLUSH_MXV  	= 3'b011,
	END_MXV	 	= 3'b100,
	HOLD_MXV	= 3'b101,
	H_PUSH_MXV 	= 3'b110
}mxv_state_e;


//Flush for FIFOS
localparam FIFO_W = 3;
typedef enum logic [FIFO_W-1:0]
{
	FIFO_0	= 3'b000,
	FIFO_1	= 3'b001,
	FIFO_2	= 3'b010,
	FIFO_3	= 3'b011,
	FIFO_4	= 3'b100
}fifo_sel;

//BYTE flush
localparam BYTEF_W = 2;
typedef enum logic [BYTEF_W-1:0]
{
	BF0_SEL	= 2'b00,
	BF1_SEL	= 2'b01,
	BF2_SEL	= 2'b10
}byteF_sel;

localparam ON  = 1'b1;
localparam OFF = '0;


 ////////////////////////
//    SYSTEM FSM      //
///////////////////////
localparam FEED_W = 6;
typedef enum logic [FEED_W-1:0]
{
	IDLE_FEED			= 6'b000000,
	WAIT_LEN_FEED		= 6'b000001,
	SYS_SETUP_FEED		= 6'b000010,
	WAIT_CMD_FEED		= 6'b000011,
	CMD_FEED			= 6'b000100,
	WAIT_MATRIXL_FEED	= 6'b000101,
	MATRIX_SETUP_FEED	= 6'b000110,
	CLEAN_FEED			= 6'b000111,
	WAIT_MTRX0_FEED		= 6'b001000,
	STORE_MTRX0_FEED	= 6'b001001,
	WAIT_MTRX1_FEED 	= 6'b001010,
	STORE_MTRX1_FEED	= 6'b001011,
	WAIT_MTRX2_FEED		= 6'b001100,
	STORE_MTRX2_FEED	= 6'b001101,
	WAIT_MTRX3_FEED		= 6'b001110,
	STORE_MTRX3_FEED	= 6'b001111,
	WAIT_VECT_FEED		= 6'b010000,
	STORE_VECT_FEED		= 6'b010001,
	START_PROC_FEED		= 6'b010010,
	TRANSMIT_FE_FEED	= 6'b010011,
	WAIT_T_FE_FEED		= 6'b010100,
	TRANSMIT_L_FEED		= 6'b010101,
	WAIT_T_L_FEED		= 6'b010110,
	TRANSMIT_CMD_FEED	= 6'b010111,
	WAIT_T_CMD_FEED		= 6'b011000,
	FIFO_OUT_FEED		= 6'b011001,
	TRANSMIT_D_FEED		= 6'b011010,
	WAIT_T_D_FEED		= 6'b011011,
	TRANSMIT_EF_FEED	= 6'b011100,
	WAIT_T_EF_FEED		= 6'b011101,
	WAIT_R_EF_FEED		= 6'b011110, 
	CHECK_EF_FEED		= 6'b011111,
	ERROR_FEED			= 6'b100000
}feed_state_e;

/** Byte selector TX*/
localparam SEL_BYTE_W = 3;
typedef enum logic [SEL_BYTE_W-1:0]
{
	START_CMD_B		= 3'b000,
	LEN_B			= 3'b001,
	CMD_B			= 3'b010,
	DATA_B			= 3'b011,
	END_CMD_B		= 3'b100
}byteCMD_sel;

endpackage
`endif