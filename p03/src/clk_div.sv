// =============================================================================
// Title       	:	Implementacion de un cambio de reloj
// Inputs		:	System clk and rst 
// Outputs		:	clock generated
// Project     	: 	t03
// File        	: 	clk_div.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module clk_div
import clk_div_pkg::*;
#(
	parameter REFERENCE_CLK = REFERENCE_CLKFPGA,
	parameter NEW_CLK = REFERENCE_CLKTB
)(
	input  logic	clk,
	input  logic	rst,
	output logic	clk_gen   
);

localparam WIDTH = $clog2(REFERENCE_CLK)+1;
localparam REFERENCE_CLKDIV = REFERENCE_CLK / 2;

logic [WIDTH-1'b1:0] clk_compare; //Variable to compare the counter

always_ff@(posedge clk, negedge rst) begin
	if(!rst) begin
			clk_compare <= NEW_CLK;
			clk_gen<= '0;
		end

	else begin
			clk_compare <= clk_compare + NEW_CLK;
			//division is to set the pulse width in 50%
			if(clk_compare >= REFERENCE_CLKDIV) begin
					clk_compare <= NEW_CLK;
					//Toggle the output signal
					clk_gen = ~clk_gen;
				end
		end
end

endmodule

