// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;
module cnt_block_uart (
	input		clk,
	input 		rst,
	input		clean_cnt,
	input		clean_sys,
	input 		uart_fail_restart,
	input		count_uart_mxv_enb,
	input		uart_fail_enb,
	output logic count_uart_mxv_ovf,
	output logic uart_fail_ovf
);

//UART counter for MXV
bin_counter_ovf_clean #(
	.DW(DW_PC-2),
	.MAXCNT(UART_COUNT_TX)
) count_uart_mxv (
	.clk(clk),
	.rst(rst),
	.enb(count_uart_mxv_enb),
	.clean(clean_sys),
	.ovf(count_uart_mxv_ovf),
	.count()
);

// UART fail counter
bin_counter_ovf_clean #(
	.DW(DW_RES),
	.MAXCNT(FAIL_CYCLES)
) uart_fail (
	.clk(clk),
	.rst(rst),
	.enb(uart_fail_enb),
	.clean(clean_cnt | uart_fail_restart),
	.count(),
	.ovf(uart_fail_ovf)
);

endmodule