// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;
module cnt_block_feed (
	input		clk,
	input 		rst,
	input		clean_cnt,
	input		cnt_data_sent_enb,
	input		cnt_rows_enb,
	input		cnt_N_bytes_enb,
	input uart_baud_t cnt_data_sent_MAXovf,
	input uart_baud_t cnt_rows_MAXovf,
	input uart_baud_t cnt_N_bytes_MAXovf,
	output logic cnt_data_sent_ovf,
	output logic cnt_rows_ovf,
	output logic cnt_N_bytes_ovf
);

//OVF at N values of FIFO
dinamMAX_counter_ovf #(
	.DW(DW_PC)
) cnt_data_sent (
	.clk(clk),
	.rst(rst),
	.enb(cnt_data_sent_enb),
	.clean(clean_cnt),
	.max_ovf(cnt_data_sent_MAXovf),
	.count(),
	.ovf(cnt_data_sent_ovf)
);

// Store N values at every FIFO*/
dinamMAX_counter_ovf #(
	.DW(DW_PC)
) cnt_rows (
	.clk(clk),
	.rst(rst),
	.enb(cnt_rows_enb),
	.clean(clean_cnt | cnt_rows_ovf),
	.max_ovf(cnt_rows_MAXovf),
	.count (),
	.ovf(cnt_rows_ovf)
);

// store N bytes 
dinamMAX_counter_ovf #(
	.DW(DW_PC),
	.RST_VAL(1)
) cnt_N_bytes (
	.clk(clk),
	.rst(rst),
	.enb(cnt_N_bytes_enb),
	.clean(clean_cnt),
	.max_ovf(cnt_N_bytes_MAXovf),
	.count (),
	.ovf(cnt_N_bytes_ovf)
);

endmodule