// =============================================================================
// Title        : FSM RX UART
//                  edo_siguiente machine for reception
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
import sdp_sc_ram_pkg::*;

module fsm_fifo (
	input		 clk,
	input 		 rst,
	//flags
	input 		 push,
	input 		 pop,
	input 		 clean, 
	output logic full,
	output logic empty,
	output logic en_out, //pop logic (TOP ADDR, BOTTOM ADDR)
	//we ram interface
	sdp_sc_ram_if.cln_fu 	mem_if
);

addr_t	last_fifo_temp; //valor mas bajo
count_t	fifo_cnt;
logic	we_t; //WE RAM
logic	en_out_t; //out enable para no poder sacar mas valores de los debidos
logic	last; //clean del último valor

//asignaciones por default y siguiente
always_ff@(posedge clk, negedge rst) 
begin
	if(!rst) begin
		mem_if.wr_addr 	<= '0;
		mem_if.rd_addr 	<= '0;
		last_fifo_temp 	<= '0;
		fifo_cnt		<= '0;
		we_t		 	<= '0;
		en_out_t		<= '0;
		last			<= '0;
	end else begin
		if(pop) begin // read from RAM
			if(!empty)
			begin
				en_out_t <= '1; //enable out
				fifo_cnt <= fifo_cnt - 1'b1;
				// ultimo pop?
				if((mem_if.rd_addr == mem_if.wr_addr) && last) begin
					last 		 	<= '0;
					mem_if.rd_addr  <= last_fifo_temp;
					mem_if.wr_addr 	<= last_fifo_temp;
				end else begin
					//addr update
					mem_if.rd_addr	  <= (mem_if.rd_addr == (W_DEPTH - 1'b1))?
										  ('0) : (mem_if.rd_addr + 1'b1);
				end					  
			end
		end	else if (push) begin // write to RAM
			if (!full) begin
				//escribo solo si no está llena
				last 	 <= '1;
				we_t 	 <= '1; //enable WRITE
				//addr update
				fifo_cnt <= fifo_cnt + 1'b1;
				mem_if.wr_addr	<= last_fifo_temp;
				last_fifo_temp	<= ((last_fifo_temp != '0) && (mem_if.wr_addr == (W_DEPTH - 1'b1)))?
										('0) : (last_fifo_temp + 1'b1);
			end
		end else if(clean) begin //limpieza de ultimo valor
			mem_if.wr_addr 	<= '0;
			mem_if.rd_addr 	<= '0;
			last_fifo_temp 	<= '0;
			fifo_cnt		<= '0;
			we_t		 	<= '0;
			en_out_t		<= '0;
			last			<= '0;
		end	else begin // values para no metaestabilidad
			we_t		<= '0;
			en_out_t 	<= '0;
		end
	end
end



always_comb begin
	full  = (fifo_cnt == W_DEPTH)?'1:'0; //full logic
	empty = ((mem_if.rd_addr == mem_if.wr_addr) && (!last))?'1:'0; //empty logic
	///////////////////////////
	mem_if.we 	 = we_t;
	en_out 		 = en_out_t;
end

endmodule
