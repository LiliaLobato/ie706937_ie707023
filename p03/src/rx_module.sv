// =============================================================================
// Title        : 	RX module
//                  Reception of data
// Project      :   t09
// File         :   rx_module.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module rx_module
import top_uart_pkg::*;
(	
	input				clk,
	input				rst,
	input 				data, //data received
	input				clear,
	output uart_baud_t	data_parallel, //data parallel
	output 				RX_interrupt,
	output 				parity_error
);

//ovf flag
logic 			bit_cnt_ovf;
logic 			time_cnt_ovf;
//statr flag
logic 			start_signal;
logic 			bit_cnt_start;
logic 			time_cnt_start;
//Enables
logic 			en_sipo;
logic 			en_parity;
logic 			en_interrupt;
logic 			en_error;
//clean flags
logic			clean;
logic 			clean_out;
logic			ready; //ready
//data from sipo
uart_baud_t		sipo_out;
//parity
logic 			parity_rcvd; //received
logic			error_calc; //error signal
//interrupts
logic			interrupt; //flag with one_shot

//rx fsm
fsm_rx fsm_rx (
	.clk(clk), 
	.rst(rst),
	.bit_cnt_ovf(bit_cnt_ovf),
	.time_cnt_ovf(time_cnt_ovf),
	.data_in(data),
	.clean_in(clear),
	.start_signal(start_signal),
	.bit_cnt_start(bit_cnt_start),
	.time_cnt_start(time_cnt_start),
	.en_sipo(en_sipo),
	.en_parity(en_parity),
	.en_interrupt(en_interrupt),
	.clean_out(clean_out),
	.en_error(en_error),
	.clean(clean),
	.ready(ready)
);

//Bit counter
dinamic_counter_ovf # (
	.DW    (CNT_HDW_PC+1'b1),
	.MAXCNT_1(DW_PC+1'b1),
	.MAXCNT_2(DW_PC)
) bit_counter(
	.clk(clk),
	.rst(rst),
	.enb(bit_cnt_start),
	.count(),
	.ovf(bit_cnt_ovf)
);

//Cycle for uart
double_counter_ovf # (
	.DW    (CNT_HDW_PC),
	.MAXCNT(HDW_PC)
) cycle_counter(
	.clk(clk),
	.rst(rst),
	.enb_1(time_cnt_start),
	.enb_2(start_signal),
	.count(),
	.ovf(time_cnt_ovf)
);

//parallel data
sipo_msb #(
	.DW(DW_PC)
)rx_parallel(
	.clk(clk), 
	.rst(rst),
	.enb(en_sipo),
	.inp(data),
	.out(sipo_out)
);

//interrupt
pipo_double interrupt_pipo(
	.clk(clk),
	.rst(rst),
	.enb(en_interrupt),
	.clean(clean_out),
	.inp(ready),
	.out(interrupt)	
);

//data recieved
pipo_clean #(
.DW(DW_PC)
)pipo_data(
	.clk(clk),
	.rst(rst),
	.enb(ready), //error on interrupt OFF|ready
	.clean(clean),
	.inp(sipo_out),
	.out(data_parallel)	
);

//get correct interrupt
anti_oneShot
anti_one_shot(
	.clk(clk),
	.rst(rst),
	.inp(interrupt),
	.out(RX_interrupt)
);

//calculated only on a ff
comparator comparator_parity(
	.clk(clk),
	.rst(rst),
	.enb(en_error),
	.in_A(ON),
	.in_B(ON),
	.out(error_calc)
);

//parity error
assign parity_error = error_calc;

endmodule
