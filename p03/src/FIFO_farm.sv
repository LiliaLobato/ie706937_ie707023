
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

import top_uart_pkg::*;

module FIFO_farm (
    input           clk_tx,
    input           rst,
    input   push_0,
    input   push_1,
    input   push_2,
    input   push_3,
    input   push_v,
    input   push_out,
    input   pop_out,
    input   pop,
    input   pop_v2,
    input   push_fifo_retro,
    input   clean,
    input   second_run,
    input   sel_mux_fifo,
    input uart_baud_t received_result,
    input uart_baud_t received_data,
    output uart_baud_t fifo_out_0,
    output uart_baud_t fifo_out_1,
    output uart_baud_t fifo_out_2,
    output uart_baud_t fifo_out_3,
    output uart_baud_t fifo_out,
    output uart_baud_t out_fifo_v
);

uart_baud_t fifo_out_v;
uart_baud_t fifo_out_v2;
uart_baud_t      mux_fifo_out;

//FIFO 0
fifo fifo_0 (
    .clk(clk_tx),
    .rst(rst),
    .push(push_0),
    .pop(pop),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_0)
);

//FIFO 1
fifo fifo_1 (
    .clk(clk_tx),
    .rst(rst),
    .push(push_1),
    .pop(pop),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_1)
);

//FIFO 2
fifo fifo_2 (
    .clk(clk_tx),
    .rst(rst),
    .push(push_2),
    .pop(pop),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_2)
);

//FIFO 3
fifo fifo_3 (
    .clk(clk_tx),
    .rst(rst),
    .push(push_3),
    .pop(pop),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_3)
);

//FIFO TO SELECT VECTOR
assign out_fifo_v = second_run?fifo_out_v2:fifo_out_v;

//FIFO VECTOR
fifo fifo_v (
    .clk(clk_tx),
    .rst(rst),
    .push(push_v),
    .pop(pop),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_v)
);

//FIFO SECOND VECTOR
fifo fifo_v2 (
    .clk(clk_tx),
    .rst(rst),
    .push(push_v),
    .pop(pop_v2),
    .DataInput(received_data),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out_v2)
);

//FEEDBACK FOR FIFO
assign mux_fifo_out = sel_mux_fifo?fifo_out:received_result;

//FIFO OUT
fifo fifo_out_m (
    .clk(clk_tx),
    .rst(rst),
    .push(push_out | push_fifo_retro),
    .pop(pop_out),
    .DataInput(mux_fifo_out),
    .clean(clean),
    .full(),
    .empty(),
    .DataOutput(fifo_out)
);

endmodule

