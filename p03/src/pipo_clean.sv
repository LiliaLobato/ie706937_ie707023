// =============================================================================
// Title        : Pipo with clean flag
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   t09
// File         :   pipo.sv
// Date         :   05/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module pipo_clean #(
parameter DW = 1
) (
input               clk,
input               rst,
input               enb,
input  [DW-1:0]     inp,
output [DW-1:0]     out,
input 				clean
);

logic [DW-1:0]      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= inp;
end:rgstr_label

assign out  = clean?'0:rgstr_r;

endmodule

