// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;
module uart_frame_detect (
	input	byteCMD_sel	byte_out_sel,
	input 	uart_baud_t cnt_rows_MAXovf,
	input 	uart_baud_t fifo_out,
	output 	uart_baud_t parallel_data_tx,
	output	uart_baud_t cnt_data_sent_MAXovf
);

assign	cnt_data_sent_MAXovf = 	(cnt_rows_MAXovf*2'd3)+2'd2;;

always_comb begin
	case (byte_out_sel)
		START_CMD_B: begin
			parallel_data_tx = START_CMD;
		end
		LEN_B: begin //length might be wrong
			parallel_data_tx = cnt_data_sent_MAXovf;
		end
		CMD_B: begin
			parallel_data_tx = DATA_CMD;
		end
		DATA_B: begin
			parallel_data_tx = fifo_out;
		end
		END_CMD_B: begin
			parallel_data_tx = END_CMD;
		end
		default: begin

		end
	endcase
end


endmodule