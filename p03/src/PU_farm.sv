
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

import top_uart_pkg::*;

module PU_farm (
    input clk_mxv,
    input rst,
    input uart_baud_t   fifo_out_0,
    input uart_baud_t   fifo_out_1,
    input uart_baud_t   fifo_out_2,
    input uart_baud_t   fifo_out_3,
    input uart_baud_t   out_fifo_v,
    input               en_acc,
    input               clean_acc,
    input fifo_sel      count_fifo,
    input byteF_sel     byte_sel,
    output uart_baud_t  received_result
);

/** MXV results to accumulators*/
result_t  result_acc_0;
result_t  result_acc_1;
result_t  result_acc_2;
result_t  result_acc_3;

/** MXV results to store*/
result_t  result_0;
result_t  result_1;
result_t  result_2;
result_t  result_3;

result_t  result_flush_val;

//PU0
proc_unit pu0 (
    .in_a(fifo_out_0),
    .in_b(out_fifo_v),
    .in_acc(result_0),
    .out(result_acc_0)
);

holder #(
    .DW(DW_RES)
)acc_0 (
    .clk(clk_mxv),
    .rst(rst),
    .enb(en_acc),
    .clean(clean_acc),
    .inp(result_acc_0),
    .out(result_0)
);

//PU1
proc_unit pu1 (
    .in_a(fifo_out_1),
    .in_b(out_fifo_v),
    .in_acc(result_1),
    .out(result_acc_1)
);

holder #(
    .DW(DW_RES)
)acc_1_m (
    .clk(clk_mxv),
    .rst(rst),
    .enb(en_acc),
    .clean(clean_acc),
    .inp(result_acc_1),
    .out(result_1)
);

//PU2
proc_unit pu2 (
    .in_a(fifo_out_2),
    .in_b(out_fifo_v),
    .in_acc(result_2),
    .out(result_acc_2)
);

holder #(
    .DW(DW_RES)
)acc_2_m (
    .clk(clk_mxv),
    .rst(rst),
    .enb(en_acc),
    .clean(clean_acc),
    .inp(result_acc_2),
    .out(result_2)
);

//PU3
proc_unit pu3 (
    .in_a(fifo_out_3),
    .in_b(out_fifo_v),
    .in_acc(result_3),
    .out(result_acc_3)
);

holder #(
    .DW(DW_RES)
) acc_3_m (
    .clk(clk_mxv),
    .rst(rst),
    .enb(en_acc),
    .clean(clean_acc),
    .inp(result_acc_3),
    .out(result_3)
);

/** MUX to select FIFO to flush*/
always_comb begin
    case (count_fifo)
        FIFO_0: begin
            result_flush_val = result_0;
        end
        FIFO_1: begin
            result_flush_val = result_1;
        end
        FIFO_2: begin
            result_flush_val = result_2;
        end
        default: begin
            result_flush_val = result_3;
        end
    endcase
end

//BYTES TO FLUSH
always_comb begin
    case (byte_sel)
        BF0_SEL: begin
            received_result = result_flush_val[DW_PC-1 : 0];
        end
        BF1_SEL: begin
            received_result = result_flush_val[(DW_PC*2)-1 : DW_PC];
        end
        default: begin
            received_result = {4'b0, result_flush_val[DW_RES-1:(DW_PC*2)]};
        end
    endcase
end

endmodule
