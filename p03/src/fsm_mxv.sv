// =============================================================================
// Title        : FSM RX UART
//                  edo_siguiente machine for reception
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;

module fsm_mxv (
	input		clk,
	input 		rst,
	input 		start_proc, 	 //inicio
	output logic 	end_proc, 	 //final
	output logic	start_flush, //start flush
	// flags de contadores
	input uart_baud_t	n_val,
	input			ovf_pop, //fifos vacias
	input			ovf_pu,  //fifos llenas
	
	//FIFO
	output fifo_sel	num_fifos, //selector de FIFO
	// pop for fifos 
	output logic	start_pop, //flag para iniciar los pop
	output logic	pop,	//pop fifo V
	output logic	pop_v2, //pop fifo V2
	output uart_baud_t	n_val_out,
	// push for fifos
	output logic	push_out, //push fifo out
	
	//ACUMULADOR
	// flags para acumuladores/registros
	output logic	en, //enable
	output logic	clean_acc,

	//FLAG PARA SEGUNDO PROCESO
	output logic	second_run
);

mxv_state_e edo_actual;	
mxv_state_e edo_siguiente;
uart_baud_t	n_val_r; //New value
logic		finished; //flag for termination
logic 		second_run_next;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      edo_actual  <= IDLE_MXV;
   else 
      edo_actual  <= edo_siguiente;
end


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(edo_actual)
		IDLE_MXV: begin
			finished   <= OFF;
			second_run <= OFF;
			if(start_proc) //Espera el llenado de las fifos
				edo_siguiente <= START_MXV;
			else
				edo_siguiente <= IDLE_MXV;
		end
		START_MXV: begin	//limpieza de los accumuladores
			edo_siguiente 	<= PROCC_MXV;
				n_val_r 	<= n_val;
		end
		PROCC_MXV: begin //procedimiento para 4 procesadores
			if(ovf_pop)
				edo_siguiente <= FLUSH_MXV;
			else
				edo_siguiente <= HOLD_MXV;
		end
		HOLD_MXV: begin //Hold necesario para el pop de las fifos
			edo_siguiente <= PROCC_MXV;
		end
		FLUSH_MXV: begin //Flush al FIFO de salida
			if(ovf_pu) 
				edo_siguiente <= END_MXV;
			else 
				edo_siguiente <= H_PUSH_MXV;
		end
		H_PUSH_MXV: begin // Hold para el siguiente valor de la FIFO de salida
			if(ovf_pu) 
				edo_siguiente <= END_MXV;
			else 
				edo_siguiente <= FLUSH_MXV;
		end
		END_MXV: begin //Revisa si se necesita una segunda corrida
			if((n_val_r > FIFO_4) && (!second_run)) begin
				second_run  	<= ON;
				edo_siguiente 	<= START_MXV; //Empieza la segunda corrida
			end else if (!start_proc) begin
				edo_siguiente 	<= IDLE_MXV;	 //Proceso terminado
				finished   		<= ON;
				second_run 		<= OFF;
			end else begin
				edo_siguiente <= END_MXV;	//Sync con top (evita metaestabilidad)
					finished <= ON;
			end
		end
		default: begin
			edo_siguiente <= IDLE_MXV;
		end
	endcase
end


always_comb begin
	case(edo_actual)
		IDLE_MXV: begin
			start_pop	= OFF;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= ON;
			end_proc  	= OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = OFF;
		end
		START_MXV: begin	//limpieza de los accumuladores
			start_pop	= ON;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= ON;
			end_proc  	= OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = n_val_r;
		end
		PROCC_MXV: begin //procedimiento para 4 procesadores
			start_pop	= ON;
			pop		 	= ON;//ovf_pop?OFF:ON;
			push_out  	= OFF;
			pop_v2    	= second_run?ON:OFF;
			`ifndef SIMULATION
				en		= ovf_pop?OFF:ON;
			`else
				en		= ON;
			`endif
			clean_acc 	= OFF;
			end_proc  	= OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = n_val_r;
		end
		HOLD_MXV: begin //Hold necesario para el pop de las fifos
			start_pop	= OFF;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= OFF;
			end_proc  	= OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = OFF;
		end
		FLUSH_MXV: begin //Flush al FIFO de salida
			start_pop	= OFF;
			pop		 	= ovf_pop?OFF:ON; //OFF;
			push_out  	= ovf_pu?OFF:ON;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= OFF;
			end_proc  	= OFF;
			start_flush = ON;
			if(!second_run) begin
				num_fifos   = 	(n_val_r == FIFO_1) ? FIFO_0 :
								(n_val_r == FIFO_2) ? FIFO_1 :
								(n_val_r == FIFO_3) ? FIFO_2 : FIFO_3;
			end else begin
				num_fifos   = 	((n_val_r - FIFO_4) == FIFO_1)? FIFO_0 :
								((n_val_r - FIFO_4) == FIFO_2)? FIFO_1 :
								((n_val_r - FIFO_4) == FIFO_3)? FIFO_2 : FIFO_3;				
			end
			n_val_out   = OFF;
		end
		H_PUSH_MXV: begin // Hold para el siguiente valor de la FIFO de salida
			start_pop	= OFF;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= OFF;
			end_proc  	= OFF;
			start_flush = ON;
			if(!second_run) begin
				num_fifos   = 	(n_val_r == FIFO_1)? FIFO_0 :
								(n_val_r == FIFO_2)? FIFO_1 :
								(n_val_r == FIFO_3)? FIFO_2 : FIFO_3;
			end else begin
				num_fifos  	= 	((n_val_r - FIFO_4) == FIFO_1)? FIFO_0 :
								((n_val_r - FIFO_4) == FIFO_2)? FIFO_1 :
								((n_val_r - FIFO_4) == FIFO_3)? FIFO_2 : FIFO_3;				
			end
			n_val_out   = OFF;
		end
		END_MXV: begin //Revisa si se necesita una segunda corrida
			start_pop	= OFF;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= OFF;
			end_proc  	= finished?ON:OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = OFF;
		end
		default: begin
			start_pop	= OFF;
			pop		 	= OFF;
			push_out  	= OFF;
			pop_v2    	= OFF;
			en        	= OFF;
			clean_acc 	= ON;
			end_proc  	= OFF;
			start_flush = OFF;
			num_fifos	= FIFO_0;
			n_val_out   = OFF;
		end
	endcase
end

endmodule