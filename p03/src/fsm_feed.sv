// =============================================================================
// Title        : FSM RX UART
//                  edo_siguiente machine for reception
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

//this fsm has a fsm inside of it on the cmd recognition edo_siguiente
module fsm_feed
import top_uart_pkg::*;
(
	//SYSTEM INPUT
	input		clk,
	input 		rst,
	input uart_baud_t	data_in, //data received
	input	recieved_int, //recieved somethtin
	input 	end_proc, //end
	input 	mtrx_stored_in, //1 = mtrx stored

	//CNT OVERFLOW FLAGS
	input 	uart_fail_ovf, //ovf if frame was incomplete
	input 	data_cnt_ovf, //ovf for received data
	input 	row_cnt_ovf, //ovf of recieved rows
	input 	uart_cnt_ovf, //ovf for cmd transmited
	input	send_cnt_ovf, //ovf for bites transmited

	//MAX FOR CNT
	output uart_baud_t	data_cnt, //new MAX for data
	output uart_baud_t	row_cnt, //new MAX for rows
	
	//FIFO
	output logic 	push_0,
	output logic 	push_1,
	output logic 	push_2,
	output logic 	push_3,
	output logic	push_v, //vector fifo
	output logic	pop_out, //ouput fifo
	output logic	push_retr, //retransmition push
	output logic	en_FFcapture, //pipo data capture

	//SYSTEM SIGNALS
	output logic 	start_proc, //start pricc (fsm msv start)
	output logic	transmit, //transmit uart flag
	output logic	mtrx_stored_out, //matrix stores successfully
	output byteCMD_sel	sel_b_out, //selector for byte frame to send
	output logic	new_or_retr,  //1=NEW RESULT 0=RETRANSMITION

	//CLEAN ENABLES
	output logic	clean, //clean for reg & fifos
	output logic 	uart_clear, //uart clean interrupt
	output logic	clean_cnters, //clean for cnt (dirty rst)

	//CNT START SIGNALS
	output logic 	uart_cnt_start, //en for transmition
	output logic 	en_row_cnt, //en cnt for rows
	output logic	en_data_cnt, //en cnt for data 

	//COUNTER ADD EN
	output logic 	enAdd_data_rcv, //en add for data recieved cnt 
	output logic 	enAdd_byte_sent, //en add for sent data cnt
	output logic	enAdd_row, //en add for row cnt

	//UART FAIL TIMER
	output logic 	uart_fail_start,
	output logic	uart_fail_feed

);

feed_state_e edo_actual;	
feed_state_e edo_siguiente;
feed_state_e edo_cmd_actual;
feed_state_e edo_cmd_siguiente;
logic pop_flag;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) begin
      edo_actual  <= IDLE_FEED;
      edo_cmd_actual <= IDLE_FEED;
   end else begin
      edo_actual  <= edo_siguiente;
      edo_cmd_actual <= edo_cmd_siguiente;
    end
end

// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
	case(edo_actual)
		IDLE_FEED: begin //detects FE
			edo_siguiente = (recieved_int && (START_CMD == data_in))?WAIT_LEN_FEED:IDLE_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_LEN_FEED: begin //waits for data
			edo_siguiente = uart_fail_ovf?ERROR_FEED:recieved_int?SYS_SETUP_FEED:WAIT_LEN_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		SYS_SETUP_FEED: begin //gets all cntr and ovf and max for cnts
			edo_siguiente = WAIT_CMD_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_CMD_FEED: begin //waits any command
			edo_siguiente = uart_fail_ovf?ERROR_FEED:recieved_int?CMD_FEED:WAIT_CMD_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		CMD_FEED: begin //after the size is received
			if(MTRX_SIZE_CMD == data_in) begin //waits size for mtrx
				edo_siguiente = WAIT_MATRIXL_FEED;
				edo_cmd_siguiente = IDLE_FEED;
			end else if(RETRMT_CMD == data_in) begin //starts retransmition
				edo_siguiente = WAIT_R_EF_FEED;
				edo_cmd_siguiente = TRANSMIT_FE_FEED;
			end else if(START_PROC_CMD == data_in) begin //starts procc and fsm_mxv
				edo_siguiente = WAIT_R_EF_FEED;
				edo_cmd_siguiente = CLEAN_FEED;
			end else if((DATA_CMD == data_in) && !mtrx_stored_in) begin //starts feed for matrx
				edo_siguiente = WAIT_MTRX0_FEED;
				edo_cmd_siguiente = IDLE_FEED;
			end else if((DATA_CMD == data_in) && mtrx_stored_in) begin //starts feed for vector
				edo_siguiente = WAIT_VECT_FEED;
				edo_cmd_siguiente = START_PROC_FEED;
			end else begin //error
				edo_siguiente = ERROR_FEED;
				edo_cmd_siguiente = edo_cmd_actual;
			end
		end
		WAIT_MATRIXL_FEED: begin //starts matrix feed to fifo
			edo_siguiente = uart_fail_ovf?ERROR_FEED:recieved_int?MATRIX_SETUP_FEED:WAIT_MATRIXL_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		MATRIX_SETUP_FEED: begin
			edo_siguiente = WAIT_R_EF_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		CLEAN_FEED: begin //waits termination
			edo_siguiente = IDLE_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_MTRX0_FEED: begin //stores matrix 0 or enables error
			edo_siguiente = uart_fail_ovf?ERROR_FEED:data_cnt_ovf?WAIT_R_EF_FEED:row_cnt_ovf?WAIT_MTRX1_FEED:recieved_int?STORE_MTRX0_FEED:WAIT_MTRX0_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		STORE_MTRX0_FEED: begin
			edo_siguiente = WAIT_MTRX0_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_MTRX1_FEED: begin//stores matrix 1 or enables error
			edo_siguiente = uart_fail_ovf?ERROR_FEED:data_cnt_ovf?WAIT_R_EF_FEED:row_cnt_ovf?WAIT_MTRX2_FEED:recieved_int?STORE_MTRX1_FEED:WAIT_MTRX1_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		STORE_MTRX1_FEED: begin
			edo_siguiente = WAIT_MTRX1_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_MTRX2_FEED: begin//stores matrix 2 or enables error
			edo_siguiente = uart_fail_ovf?ERROR_FEED:data_cnt_ovf?WAIT_R_EF_FEED:row_cnt_ovf?WAIT_MTRX3_FEED:recieved_int?STORE_MTRX2_FEED:WAIT_MTRX2_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		STORE_MTRX2_FEED: begin
			edo_siguiente = WAIT_MTRX2_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_MTRX3_FEED: begin//stores matrix 3 or enables error
			edo_siguiente = uart_fail_ovf?ERROR_FEED:data_cnt_ovf?WAIT_R_EF_FEED:row_cnt_ovf?WAIT_MTRX0_FEED:recieved_int?STORE_MTRX3_FEED:WAIT_MTRX3_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		STORE_MTRX3_FEED: begin
			edo_siguiente = WAIT_MTRX3_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_VECT_FEED: begin//stores vector 0 or enables error
			edo_siguiente = uart_fail_ovf?ERROR_FEED:data_cnt_ovf?WAIT_R_EF_FEED:recieved_int?STORE_VECT_FEED:WAIT_VECT_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		STORE_VECT_FEED: begin
			edo_siguiente = WAIT_VECT_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		START_PROC_FEED: begin //starts transmision
			edo_siguiente = end_proc?TRANSMIT_FE_FEED:START_PROC_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		TRANSMIT_FE_FEED: begin //init FE
			edo_siguiente = WAIT_T_FE_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_T_FE_FEED: begin
			edo_siguiente = uart_cnt_ovf?TRANSMIT_L_FEED:WAIT_T_FE_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		TRANSMIT_L_FEED: begin //size transmit
			edo_siguiente = WAIT_T_L_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_T_L_FEED: begin
			edo_siguiente = uart_cnt_ovf?TRANSMIT_CMD_FEED:WAIT_T_L_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		TRANSMIT_CMD_FEED: begin //transmit cmd
			edo_siguiente = WAIT_T_CMD_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_T_CMD_FEED: begin
			edo_siguiente = uart_cnt_ovf?FIFO_OUT_FEED:WAIT_T_CMD_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		FIFO_OUT_FEED: begin //gets correct data out
			edo_siguiente = TRANSMIT_D_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		TRANSMIT_D_FEED: begin //transmit data
			edo_siguiente = WAIT_T_D_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end
		WAIT_T_D_FEED: begin //pops next data
			edo_cmd_siguiente = edo_cmd_actual;
			edo_siguiente = uart_cnt_ovf?(send_cnt_ovf?TRANSMIT_EF_FEED:FIFO_OUT_FEED):WAIT_T_D_FEED;
		end 
		TRANSMIT_EF_FEED: begin //end EF
			edo_siguiente = WAIT_T_EF_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		WAIT_T_EF_FEED: begin
			edo_siguiente = uart_cnt_ovf?IDLE_FEED:WAIT_T_EF_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		WAIT_R_EF_FEED: begin
			edo_siguiente = uart_fail_ovf?ERROR_FEED:recieved_int?CHECK_EF_FEED:WAIT_R_EF_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		CHECK_EF_FEED: begin//waits termination or error
			edo_siguiente = (END_CMD == data_in)?edo_cmd_actual:ERROR_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		ERROR_FEED: begin//clean system
			edo_siguiente = IDLE_FEED;
			edo_cmd_siguiente = edo_cmd_actual;
		end 
		default: begin 
			edo_siguiente = IDLE_FEED;
			edo_cmd_siguiente = IDLE_FEED;
		end
	endcase
end


always_comb begin
	//Para hacer el codigo mas pequeño, mandamos todo a default
	//solo cambiamos las que sea necesarecieved_into
	clean			= OFF;
	push_0			= OFF;
	push_1			= OFF;
	push_2			= OFF;
	push_3			= OFF;
	push_v			= OFF;
	pop_out			= OFF;
	start_proc		= OFF;
	uart_clear		= recieved_int;
	transmit		= OFF;
	en_FFcapture	= OFF;
	en_row_cnt		= OFF;
	row_cnt			= OFF;
	enAdd_row		= OFF;
	data_cnt		= OFF;
	clean_cnters	= OFF;
	en_data_cnt		= OFF;
	uart_fail_start	= OFF;
	uart_fail_feed	= OFF;
	uart_cnt_start 	= OFF;
	mtrx_stored_out	= OFF;
	push_retr		= OFF;
	new_or_retr		= OFF;
	sel_b_out		= START_CMD_B;
	enAdd_byte_sent	= OFF;
	enAdd_data_rcv 	= OFF;

	case(edo_actual)
		IDLE_FEED: begin
			clean_cnters 	= ON;
		end
		WAIT_LEN_FEED: begin
			uart_fail_start	= ON;
		end
		SYS_SETUP_FEED: begin
			uart_fail_start	= ON;	
			uart_fail_feed	= ON;
			en_data_cnt 	= ON;
			data_cnt 		= data_in[DW_PC-2:0];
		end
		WAIT_CMD_FEED: begin
			uart_fail_start	= ON;
		end
		CMD_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
		end
		WAIT_MATRIXL_FEED: begin
			uart_fail_start	= ON;
		end
		MATRIX_SETUP_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			row_cnt 		= data_in[(DW_PC/2)-1:0];
			en_row_cnt 		= ON;
		end
		CLEAN_FEED: begin
			clean 			= ON;
		end
		WAIT_MTRX0_FEED: begin
			uart_fail_start	= ON;
		end
		STORE_MTRX0_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			enAdd_row		= ON;
			push_0			= ON;
			en_FFcapture	= ON;
			mtrx_stored_out	= ON;
		end
		WAIT_MTRX1_FEED: begin
			uart_fail_start	= ON;
		end
		STORE_MTRX1_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			enAdd_row		= ON;
			push_1 			= ON;
		end
		WAIT_MTRX2_FEED: begin
			uart_fail_start	= ON;
		end
		STORE_MTRX2_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			enAdd_row		= ON;
			push_2 			= ON;
		end
		WAIT_MTRX3_FEED: begin
			uart_fail_start	= ON;
		end
		STORE_MTRX3_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			enAdd_row		= ON;
			push_3 			= ON;
		end
		WAIT_VECT_FEED: begin
			uart_fail_start	= ON;
		end
		STORE_VECT_FEED: begin
			uart_fail_start	= ON;
			uart_fail_feed	= ON;
			enAdd_data_rcv	= ON;
			enAdd_row		= ON;
			push_v 			= ON;
			en_FFcapture	= ON;
			mtrx_stored_out	= OFF;
		end
		START_PROC_FEED: begin
			start_proc 		= ON;
		end
		TRANSMIT_FE_FEED: begin
			sel_b_out 		= START_CMD_B;
			transmit 		= ON;
		end
		WAIT_T_FE_FEED: begin
			sel_b_out 		= START_CMD_B;
			uart_cnt_start 	= ON;
		end
		TRANSMIT_L_FEED: begin
			sel_b_out		= LEN_B;
			transmit 		= ON;
			enAdd_byte_sent	= ON;
		end
		WAIT_T_L_FEED: begin
			sel_b_out		= LEN_B;
			uart_cnt_start 	= ON;
		end
		TRANSMIT_CMD_FEED: begin
			sel_b_out 		= CMD_B;
			transmit		= ON;
			enAdd_byte_sent = ON;
		end
		WAIT_T_CMD_FEED: begin
			sel_b_out 		= CMD_B;
			uart_cnt_start 	= ON; 
			pop_out			= uart_cnt_ovf?ON:OFF; //no existia
		end
		FIFO_OUT_FEED: begin
			sel_b_out 		= DATA_B;
			new_or_retr		= ON;
			//pop_out			= ON;
		end
		TRANSMIT_D_FEED: begin
			sel_b_out 		= DATA_B;
			new_or_retr		= ON;
			push_retr		= ON;
			transmit		= ON;
			enAdd_byte_sent	= ON;
		end
		WAIT_T_D_FEED: begin
			sel_b_out 		= DATA_B;
			new_or_retr		= ON;
			uart_cnt_start 	= ON;
			pop_out			= uart_cnt_ovf?ON:OFF; //no existia
		end
		TRANSMIT_EF_FEED: begin
			sel_b_out		= END_CMD_B;
			transmit		= ON;
		end
		WAIT_T_EF_FEED: begin
			sel_b_out		= END_CMD_B;
			uart_cnt_start 	= ON;
		end
		ERROR_FEED: begin
			clean 			= ON;
		end
		default: begin end
	endcase
end

endmodule



