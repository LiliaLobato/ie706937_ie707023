// =============================================================================
// Title        :   UART
// Inputs       :   clk, rst, rx data, tx flag, tx data 
// Outputs      :   rx data, rx interrupt, parity error
// Project      :   uart
// File         :   top_uart.sv
// Date         :   09/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;
import clk_div_pkg::*;

module top_mxv
(
	input 	clk,
	input 	rst,
	input 	serial_data_rx, //uart rx
	output 	serial_data_tx, //uart tx
	output 	HeatBit_signal
);


 ////////////////////////
//       CLK          //
///////////////////////
`ifndef SIMULATION
	logic 	clk_mxv;
`else
	logic	clk_mxv;
	logic	clk_mxv_v2;
`endif
logic	clk_pll; //114.3kHz*4
logic	clk_tx;  

 ////////////////////////
//   UART SIGNALS     //
///////////////////////
//in
logic 	clear_interrupt;
logic 	clear_interrupt_out;
logic 	transmit; //begin transmition on uart
uart_baud_t	parallel_data_tx; //parallel data tx (data_uart_tx)
//out
uart_baud_t	received_data; //parallel data rx
logic	rx_interrupt; //data receives if ON
logic	parity_error;

 ////////////////////////
//   FSM SIGNALS      //
///////////////////////
//CNT OVERFLOW FLAGS
logic	uart_fail_ovf;
logic	cnt_N_bytes_ovf;
logic 	cnt_rows_ovf;
logic	count_uart_mxv_ovf;
logic	cnt_data_sent_ovf;
logic	cnt_fifo_pop_ovf;
logic	cnt_fifo_flush_ovf;
uart_baud_t	cnt_fifo_pop_MAXovf;
uart_baud_t	cnt_data_sent_MAXovf;
fifo_sel	cnt_fifo_flush_MAXovf;
uart_baud_t	cnt_N_bytes_MAXovf;
uart_baud_t	cnt_rows_MAXovf;
//MAX FOR CNT
uart_baud_t	data_cnt;
uart_baud_t	row_cnt;
//CNT VALUES
uart_baud_t cnt_byte_sel;
uart_baud_t  	count_fifo_val;
byteF_sel	cnt_Bselector_count;
fifo_sel   	cnt_fifo_flush_count;
//FIFO
logic	push_0;
logic	push_1;
logic	push_2;
logic	push_3;
logic	push_v;
logic 	push_fifo_retro;
logic	pop_out;
logic 	holder_matrix_enb;
logic	clear;
uart_baud_t  out_fifo_v;
logic 	mtrx_stored_out;
uart_baud_t	fifo_out_0;
uart_baud_t	fifo_out_1;
uart_baud_t	fifo_out_2;
uart_baud_t	fifo_out_3;
uart_baud_t	fifo_out;
uart_baud_t	received_result;
//SYSTEM SIGNALS
logic	start_proc;
logic 	mtrx_stored;
logic	sel_mux_fifo;
logic	cnt_Bselector_enb;
byteCMD_sel byte_out_sel;
//CLEAN ENABLES
logic	clean_sys;
logic	clean_cnt;
//CNT START SIGNALS
logic 	pipo_row_enb;
logic 	pipo_N_byte_enb;
logic	count_uart_mxv_enb;
//COUNTER ADD EN
logic 	cnt_N_bytes_enb;
logic 	cnt_data_sent_enb;
logic	cnt_rows_enb;
logic	cnt_fifo_pop_enb;
//UART FAIL TIMER
logic	uart_fail_restart;
logic	uart_fail_enb;
//ACUMULADOR
logic	en_acc;
logic	clean_acc;
//FLAG PARA SEGUNDO PROCESO
logic	second_run;
//SYNCH SIGNAL
logic	n_synch_pop;
logic	synch_pop;
logic	n_synch_pop2;
logic	synch_pop2;
logic	n_synch_push;
logic	synch_push;
logic	n_synch_end;
logic	synch_end;


 ///////////////////////////////////////////////////////////////
// INSTANCIATION                                             //
//////////////////////////////////////////////////////////////

`ifndef SIMULATION
		//TODO: GENERATE PLL FOR 114.3KHZ*4
		//UART CLK
		//pll_uart pll
		//(
		//	.arst(!rst),
		//	.inclk0(clk),
		//	.c0(clk_pll)
		//);

		//MXV CLK
		clk_div #(
			.NEW_CLK(FREQUENCY_1KHZ)
		) clk_mxv (
			.clk(pll_clk),
			.rst(rst),
			.clk_gen(clk_mxv)
		);

`else
	assign clk_pll = clk; //no need for pll

	clk_div #(
		.REFERENCE_CLK(REFERENCE_CLKUART),
		.NEW_CLK(NEW_CLKUART)
	) clk_mxv_1 (
		.clk(clk_pll),
		.rst(rst),
		.clk_gen(clk_mxv_v2)
	);

	clk_div #(
		.REFERENCE_CLK(REFERENCE_CLKUART),
		.NEW_CLK(NEW_CLKUART)
	) clk_mxv_2 (
		.clk(clk_mxv_v2),
		.rst(rst),
		.clk_gen(clk_mxv)
	);
`endif

//1KHz signal to show life
assign HeatBit_signal = clk_mxv;
clk_div #(
	.REFERENCE_CLK(REFERENCE_CLKUART),
	.NEW_CLK(NEW_CLKUART)
) clk_uart (
	.clk(clk_pll),
	.rst(rst),
	.clk_gen(clk_tx)
);

 ////////////////////////
//       UART         //
///////////////////////
top_uart uart(
	.clk_pll(clk_pll),
	.clk_tx(clk_tx),
	.rst(rst),
	//in
	.serial_data_rx(serial_data_rx),
	.clear_interrupt(clear_interrupt),
	.transmit(transmit),
	.parallel_data_tx(parallel_data_tx),
	//out
	.parallel_data_rx(received_data),
	.rx_interrupt(rx_interrupt),
	.parity_error(parity_error),
	.serial_data_tx(serial_data_tx)
);

//CNTR block for UART
cnt_block_uart cnt_block_uart (
	.clk(clk_tx),
	.rst(rst),
	.clean_cnt(clean_cnt),
	.clean_sys(clean_sys),
	.uart_fail_restart(uart_fail_restart),
	.count_uart_mxv_enb(count_uart_mxv_enb),
	.uart_fail_enb(uart_fail_enb),
	.count_uart_mxv_ovf(count_uart_mxv_ovf),
	.uart_fail_ovf(uart_fail_ovf)
);

// UART interrupt
pipo_Nenb pipo_uart_interrupt (
	.clk(clk_tx),
	.rst(rst),
	.inp(clean_sys?'0:clear_interrupt),
	.out(clear_interrupt_out)
);

//UART data
pipo_clean #(
	.DW(DW_PC)
) pipo_N_byte (
	.clk(clk_tx),
	.rst(rst),
	.enb(pipo_N_byte_enb),
	.clean(clean_sys),
	.inp(data_cnt),
	.out(cnt_N_bytes_MAXovf)
);

 ////////////////////////
//     FSM FEED       //
///////////////////////
fsm_feed fsm_feed (	
	//SYSTEM INPUT
	.clk(clk_tx),
	.rst(rst),
	.data_in(received_data),
	.recieved_int(rx_interrupt),
	.end_proc(synch_end),
	.mtrx_stored_in(mtrx_stored_out),
	//CNT OVERFLOW FLAGS
	.uart_fail_ovf('0),//uart_fail_ovf),
	.data_cnt_ovf(cnt_N_bytes_ovf),
	.row_cnt_ovf(cnt_rows_ovf),
	.uart_cnt_ovf(count_uart_mxv_ovf),
	.send_cnt_ovf(cnt_data_sent_ovf),
	//MAX FOR CNT
	.data_cnt(data_cnt),
	.row_cnt(row_cnt),
	//FIFO
	.push_0(push_0),
	.push_1(push_1),
	.push_2(push_2),
	.push_3(push_3),
	.push_v(push_v),
	.pop_out(pop_out),
	.push_retr(push_fifo_retro),
	.en_FFcapture(holder_matrix_enb),
	//SYSTEM SIGNALS
	.start_proc(start_proc),
	.transmit(transmit),
	.mtrx_stored_out(mtrx_stored),
	.sel_b_out(byte_out_sel),
	.new_or_retr(sel_mux_fifo),
	//CLEAN ENABLES
	.clean(clean_sys),
	.uart_clear(clear_interrupt),
	.clean_cnters(clean_cnt),
	//CNT START SIGNALS
	.en_row_cnt(pipo_row_enb),
	.en_data_cnt(pipo_N_byte_enb),
	.uart_cnt_start(count_uart_mxv_enb),
	//COUNTER ADD EN
	.enAdd_data_rcv(cnt_N_bytes_enb),	
	.enAdd_byte_sent(cnt_data_sent_enb),
	.enAdd_row(cnt_rows_enb),
	//UART FAIL TIMER
	.uart_fail_start(uart_fail_enb),
	.uart_fail_feed(uart_fail_restart)
);

//CNTRS FOR FSM FEED
cnt_block_feed cnt_block_feed (
	.clk(clk_tx),
	.rst(rst),
	.cnt_data_sent_enb(cnt_data_sent_enb),
	.cnt_rows_enb(cnt_rows_enb),
	.cnt_N_bytes_enb(cnt_N_bytes_enb),
	.cnt_data_sent_MAXovf(cnt_data_sent_MAXovf),
	.cnt_rows_MAXovf(cnt_rows_MAXovf),
	.cnt_N_bytes_MAXovf(cnt_N_bytes_MAXovf),
	.clean_cnt(clean_cnt),
	.cnt_data_sent_ovf(cnt_data_sent_ovf),
	.cnt_rows_ovf(cnt_rows_ovf),
	.cnt_N_bytes_ovf(cnt_N_bytes_ovf)
);

//Matrix stored flag
holder holder_matrix (
	.clk(clk_tx),
	.rst(rst),
	.enb(holder_matrix_enb),
	.clean(clean_sys),
	.inp(mtrx_stored),
	.out(mtrx_stored_out)
);

// N rows stored
pipo_clean #(
	.DW(DW_PC)
) pipo_row (
	.clk(clk_tx),
	.rst(rst),
	.enb(pipo_row_enb),
	.clean(clean_sys),
	.inp(row_cnt),
	.out(cnt_rows_MAXovf)
);

 ////////////////////////
//      FSM MXV       //
///////////////////////
fsm_mxv fsm_mxv (
	.clk(clk_mxv),
	.rst(rst),
	.start_proc(start_proc),
	.end_proc(n_synch_end),
	.start_flush(cnt_Bselector_enb),
	// flags de contadores
	.n_val(cnt_rows_MAXovf),
	.ovf_pop(cnt_fifo_pop_ovf),
	.ovf_pu(cnt_fifo_flush_ovf),
	//FIFO
	.num_fifos(cnt_fifo_flush_MAXovf),
	// pop for fifos 
	.start_pop(cnt_fifo_pop_enb),
	.pop(n_synch_pop),
	.pop_v2(n_synch_pop2),
	.n_val_out(cnt_fifo_pop_MAXovf),
	// push for fifos
	.push_out(n_synch_push),
	//ACUMULADOR
	.en(en_acc),
	.clean_acc(clean_acc),
	//FLAG PARA SEGUNDO PROCESO
	.second_run(second_run)
);

// BLOQUE DE SYNCH //
synch_block synch_block(
	.clk(clk_tx),
	.rst(rst),
	.n_synch_pop(n_synch_pop),
	.n_synch_pop2(n_synch_pop2),
	.n_synch_push(n_synch_push),
	.n_synch_end(n_synch_end),
	.synch_pop(synch_pop),
	.synch_pop2(synch_pop2),
	.synch_push(synch_push),
	.synch_end(synch_end)
);

//CNT for FSM MXV
cnt_block_mxv cnt_block_mxv (
	.clk(clk_mxv),
	.rst(rst),
	.clean_acc(clean_acc),
	.cnt_fifo_pop_enb(cnt_fifo_pop_enb),
	.cnt_Bselector_enb(cnt_Bselector_enb),
	.cnt_fifo_pop_MAXovf(cnt_fifo_pop_MAXovf),
	.cnt_fifo_flush_MAXovf(cnt_fifo_flush_MAXovf),
	.cnt_fifo_pop_ovf(cnt_fifo_pop_ovf),
	.cnt_fifo_flush_ovf(cnt_fifo_flush_ovf),
	.cnt_fifo_flush_count(cnt_fifo_flush_count),
	.cnt_Bselector_count(cnt_Bselector_count)
);

//PROCESSOR AND ACCUMULATORS
PU_farm PU_farm (
	.clk_mxv(clk_mxv),
	.rst(rst),
    .fifo_out_0(fifo_out_0),
    .fifo_out_1(fifo_out_1),
    .fifo_out_2(fifo_out_2),
    .fifo_out_3(fifo_out_3),
    .out_fifo_v(out_fifo_v),
    .clean_acc(clean_acc),
    .en_acc(en_acc),
    .count_fifo(cnt_fifo_flush_count),
    .byte_sel(cnt_Bselector_count),
    .received_result(received_result)
);

//FIFO FARM
FIFO_farm FIFO_farm (
	.clk_tx(clk_tx),
	.rst(rst),
    .push_0(push_0),
    .push_1(push_1),
    .push_2(push_2),
    .push_3(push_3),
    .push_v(push_v),
    .push_out(synch_push),
    .pop_out(pop_out),
    .pop(synch_pop),
    .pop_v2(synch_pop2),
    .push_fifo_retro(push_fifo_retro),
    .clean(clean_sys),
    .sel_mux_fifo(sel_mux_fifo),
    .second_run(second_run),
    .received_result(received_result),
    .received_data(received_data),
    .fifo_out_0(fifo_out_0),
    .fifo_out_1(fifo_out_1),
    .fifo_out_2(fifo_out_2),
    .fifo_out_3(fifo_out_3),
    .fifo_out(fifo_out),
    .out_fifo_v(out_fifo_v)
);

//UART SEND FRAME
uart_frame_detect uart_frame_detect (
	.byte_out_sel	(byte_out_sel),
	.cnt_rows_MAXovf (cnt_rows_MAXovf),
	.fifo_out        (fifo_out),
	.parallel_data_tx(parallel_data_tx),
	.cnt_data_sent_MAXovf(cnt_data_sent_MAXovf)
);


endmodule : top_mxv
