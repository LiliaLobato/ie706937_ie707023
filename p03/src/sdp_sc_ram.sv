//Coder:          DSc Abisai Ramirez Perez
//Date:           03/31/2019
//Name:           sdp_sc_ram.sv
//Description:    This is the HDL of a single dual-port dual-clock random access memory. 

`ifndef SDP_SC_RAM_SV
    `define SDP_SC_RAM_SV
module sdp_sc_ram 
import sdp_sc_ram_pkg::*;
(
// Core clock a
input   					clk,
// Data Input
input data_t			data,
// Memory interface
sdp_sc_ram_if.mem 	mem_if
);

// Declare a RAM variable 
data_t  ram [W_DEPTH-1:0];

always_ff@(posedge clk) begin
if(mem_if.we)
    ram[mem_if.wr_addr] <= data;

mem_if.rd_data <= ram [mem_if.rd_addr];
end

endmodule
`endif

