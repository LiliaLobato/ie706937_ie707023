
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

module dinamMAX_counter_ovf #(
parameter DW      = 4,
parameter RST_VAL = 0
)(
input           clk,
input           rst,
input           enb,
input           clean,
input  [DW-1:0] max_ovf,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;

always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  RST_VAL[DW-1:0];
    else if (enb)
        count_r     <= count_nxt;
    else if (clean)
        count_r     <= RST_VAL[DW-1:0];
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
    if (count_r == max_ovf) begin
        ovf       = 1'b1;
        count_nxt = RST_VAL[DW-1:0];
    end else begin
        ovf     =   1'b0;
        count_nxt =  clean?'0:count_r + 1'b1;
    end
end

assign count    =   clean?RST_VAL[DW-1:0]:count_r;

endmodule

