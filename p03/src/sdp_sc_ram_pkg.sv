// Coder:           DSc Abisai Ramirez Perez
// Date:            31 April 2019
// Name:            sdp_sc_ram_pkg.sv
// Description:     This is the package of single-port RAM
`ifndef SDP_SC_RAM__PKG_SV
    `define SDP_SC_RAM_PKG_SV
package sdp_sc_ram_pkg ;

    localparam  W_DATA      = top_uart_pkg::DW_PC;
    localparam  W_ADDR      = top_uart_pkg::CNT_DW_PC;
    localparam  W_DEPTH     = 2**W_ADDR;

    typedef logic [W_DATA-1:0]        data_t;
    typedef logic [W_ADDR-1:0]        addr_t;
	typedef logic [W_ADDR  :0]        count_t;

endpackage
`endif
