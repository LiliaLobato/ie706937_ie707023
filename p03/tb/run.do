if [file exists work] {vdel -all}
vlib work
vlog +define+SIMULATION -f files.f
#vlog -f files.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.tb_p03
do wave.do
run 600us
