`timescale 10ns / 1ps

module tb_p03;
import top_uart_pkg::*;

logic 	clk;
logic 	rst;
logic	serial_data_rx;
logic	serial_data_tx;
logic	mxv_clk;

top_mxv uut (
	.clk(clk),
	.rst(rst),
	.serial_data_rx(serial_data_rx),
	.serial_data_tx(serial_data_tx),
	.HeatBit_signal(mxv_clk)
);

initial begin
	clk = OFF;
	rst = OFF;

	#6 rst = ON;
	
	//http://es.onlinemschool.com/math/assistance/matrix/multiply/
	//matrix_ejemplo(); //ejemplo practica
	//send_retransmition(); //retransmision
	//matrix_8x8(); 
	//REAL: 000611 001A1D 002E29 004235 0011EE 001428 002E29
	//LSB:  110600 1D1A00 292E00 354200 EE1100 281400 292E00
	//matrix_7x7(); //14C8 4504 7540 A57C D5B8 105F4 1F0C 3521
	/*
	'0611'
    '1A1D'
    '2E29'
    '4235'

    '11EE'
    '1428'
    '2E29'
    '3521'
    */
	matrix_5x5(); //filled with FF

	//matrix_0x0();
	//matrix_1x1();
	//send_invalid_data(); 
	//send_parity_error(); 

	//$stop;
end

always begin
    #1 clk <= ~clk;
end

task matrix_ejemplo();
	send_put_on_shoot();
	delay_clk(10);
	send_N(8'h04);
	delay_clk(10);
	send_mtrx_ejemplo();
	delay_clk(10);
	send_vect_ejemplo();
	delay_clk(4000);
endtask

task matrix_5x5();
	send_put_on_shoot();
	delay_clk(10);
	send_N(8'h05);
	delay_clk(10);
	send_mtrx_5x5();
	delay_clk(10);
	send_vect_5x1();
	delay_clk(4000);
endtask

task matrix_7x7();
	send_put_on_shoot();
	delay_clk(10);
	send_N(8'h07);
	delay_clk(10);
	send_mtrx_7x7();
	delay_clk(10);
	send_vect_7x1();
	delay_clk(4000);
endtask

task matrix_8x8();
	send_put_on_shoot();
	delay_clk(10);
	send_N(8'h08);
	delay_clk(10);
	send_mtrx_8x8();
	delay_clk(10);
	send_vect_8x1();
	delay_clk(4000);
endtask


//size configuration
task send_N(uart_baud_t	data_in = 8'h01);
	data_send(8'hFE); data_send(8'h03);
	data_send(8'h01); data_send(data_in);
	data_send(8'hEF);
endtask

//init cmd
task send_put_on_shoot();
	data_send(8'hFE); data_send(8'h02); data_send(8'h03); data_send(8'hEF);
endtask

task send_parity_error();
	data_send_parity(8'hFE); data_send_parity(8'h02); 
	data_send_parity(8'h03); data_send_parity(8'hEF);
	delay_clk(4000);
endtask

//send retransmition
task send_retransmition();
	data_send(8'hFE); data_send(8'h02); data_send(8'h02); data_send(8'hEF);
	delay_clk(4000);
endtask

//send invalid data
task send_invalid_data(); //AA FE 02 13 25 65 5
	data_send(8'hAA); data_send(8'hFE); data_send(8'h02); data_send(8'h13);
	data_send(8'h25); data_send(8'h65); data_send(8'h05); 
	delay_clk(4000);
endtask

//matrxi
task send_mtrx_ejemplo();
	data_send(8'hFE); data_send(8'h12); data_send(8'h04);
	
	//ROW1
	data_send(8'h00); data_send(8'h01); data_send(8'h02); data_send(8'h03);
	
	//ROW2
	data_send(8'h04); data_send(8'h05); data_send(8'h06); data_send(8'h07);	

	//ROW3
	data_send(8'h08); data_send(8'h09); data_send(8'h0A); data_send(8'h0B);

	//ROW4
	data_send(8'h0C); data_send(8'h0D); data_send(8'h0E); data_send(8'h0F);
	
	data_send(8'hEF);
endtask

task send_mtrx_5x5();
	data_send(8'hFE); data_send(8'h1B); data_send(8'h04);
	
	//ROW1
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);
	
	//ROW2
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);

	//ROW3
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);

	//ROW4
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);
	
	//ROW5
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);

	data_send(8'hEF);
endtask

task send_mtrx_7x7();
	data_send(8'hFE); data_send(8'h34); data_send(8'h04);
	
	//ROW1
	data_send(8'h00); data_send(8'h01); data_send(8'h02); data_send(8'h03);
	data_send(8'h04); data_send(8'h05); data_send(8'h06);
	
	//ROW2
	data_send(8'h07); data_send(8'h08); data_send(8'h09); data_send(8'h0A);
	data_send(8'h0B); data_send(8'h0C); data_send(8'h0D);

	//ROW3
	data_send(8'h0E); data_send(8'h0F); data_send(8'h10); data_send(8'h11);
	data_send(8'h12); data_send(8'h13); data_send(8'h14);

	//ROW4
	data_send(8'h15); data_send(8'h16); data_send(8'h17); data_send(8'h18);
	data_send(8'h19); data_send(8'h1A); data_send(8'h1B);
	
	//ROW5
	data_send(8'h1C); data_send(8'h1D); data_send(8'h1E); data_send(8'h1F);
	data_send(8'h20); data_send(8'h21); data_send(8'h22);

	//ROW6
	data_send(8'h23); data_send(8'h24); data_send(8'h25); data_send(8'h26);
	data_send(8'h27); data_send(8'h28); data_send(8'h29);

	//ROW7
	data_send(8'h2A); data_send(8'h2B); data_send(8'h2E); data_send(8'h2F);
	data_send(8'h30); data_send(8'h31); data_send(8'h32);

	data_send(8'hEF);
endtask
/** Send Matrix 8x8*/
task send_mtrx_8x8();
	data_send(8'hFE); data_send(8'h42); data_send(8'h04);
	
	//ROW1
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW2
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW3
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW4
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW5
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW6
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW7
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	//ROW8
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	data_send(8'hEF);
endtask

//vector
task send_vect_ejemplo();
	data_send(8'hFE); data_send(8'h06); data_send(8'h04);
	
	//ROW1
	data_send(8'h01); data_send(8'h02); data_send(8'h03); data_send(8'h04);
	
	data_send(8'hEF);
endtask

task send_vect_7x1();
	data_send(8'hFE); data_send(8'h09); data_send(8'h04);
	
	//ROW1
	data_send(8'hF9); data_send(8'hFA); data_send(8'hFB); data_send(8'hFC);
	data_send(8'hFD); data_send(8'hFE); data_send(8'hFF);
	
	data_send(8'hEF);
endtask

/** Send Vector 5*/
task send_vect_5x1();
	data_send(8'hFE); data_send(8'h07); data_send(8'h04);
	
	//ROW1
	data_send(8'hFF); data_send(8'hFF); data_send(8'hFF);
	data_send(8'hFF); data_send(8'hFF);
	
	data_send(8'hEF);
endtask

/** Send Vector 8*/
task send_vect_8x1();
	data_send(8'hFE); data_send(8'h0A); data_send(8'h04);

	//ROW1
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	data_send(8'h01); data_send(8'h01); data_send(8'h01);  data_send(8'h01);
	
	data_send(8'hEF);
endtask

//send each byte 
task data_send(uart_baud_t	uart_in = 8'h01);

	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = uart_in[0];
	#8 serial_data_rx = uart_in[1];
	#8 serial_data_rx = uart_in[2];
	#8 serial_data_rx = uart_in[3];
	#8 serial_data_rx = uart_in[4];
	#8 serial_data_rx = uart_in[5];
	#8 serial_data_rx = uart_in[6];
	#8 serial_data_rx = uart_in[7];

	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = 1'b1;

endtask

//send each byte 
task data_send_parity(uart_baud_t	uart_in = 8'h01);

	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = uart_in[0];
	#8 serial_data_rx = uart_in[1];
	#8 serial_data_rx = uart_in[2];
	#8 serial_data_rx = uart_in[3];
	delay_clk(500);
	#8 serial_data_rx = uart_in[4];
	#8 serial_data_rx = uart_in[5];
	#8 serial_data_rx = uart_in[6];
	#8 serial_data_rx = uart_in[7];

	#8 serial_data_rx = 1'b0;

	#8 serial_data_rx = 1'b1;

endtask


task delay_clk(int dly = 1);
    repeat(dly)
        @(posedge clk);
endtask

endmodule
