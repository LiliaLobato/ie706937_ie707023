onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_p03/clk
add wave -noupdate /tb_p03/uut/uart/clk_tx
add wave -noupdate /tb_p03/uut/HeatBit_signal
add wave -noupdate -divider TOP
add wave -noupdate /tb_p03/uut/rst
add wave -noupdate /tb_p03/uut/serial_data_rx
add wave -noupdate /tb_p03/uut/serial_data_tx
add wave -noupdate /tb_p03/uut/HeatBit_signal
add wave -noupdate -divider UART
add wave -noupdate /tb_p03/uut/uart/clear_interrupt
add wave -noupdate /tb_p03/uut/uart/rx_interrupt
add wave -noupdate -radix hexadecimal /tb_p03/uut/uart/parallel_data_rx
add wave -noupdate /tb_p03/uut/uart/transmit
add wave -noupdate -radix hexadecimal /tb_p03/uut/uart/parallel_data_tx
add wave -noupdate -divider {FSM FEED}
add wave -noupdate /tb_p03/uut/fsm_feed/edo_actual
add wave -noupdate /tb_p03/uut/fsm_feed/edo_cmd_actual
add wave -noupdate /tb_p03/uut/byte_out_sel
add wave -noupdate /tb_p03/uut/fsm_feed/clean
add wave -noupdate /tb_p03/uut/fsm_feed/uart_fail_ovf
add wave -noupdate /tb_p03/uut/fsm_feed/end_proc
add wave -noupdate /tb_p03/uut/fsm_feed/recieved_int
add wave -noupdate /tb_p03/uut/fsm_feed/pop_out
add wave -noupdate /tb_p03/uut/fsm_feed/push_retr
add wave -noupdate /tb_p03/uut/fsm_feed/start_proc
add wave -noupdate /tb_p03/uut/fsm_feed/row_cnt_ovf
add wave -noupdate /tb_p03/uut/fsm_feed/mtrx_stored_in
add wave -noupdate /tb_p03/uut/fsm_feed/push_0
add wave -noupdate /tb_p03/uut/fsm_feed/push_1
add wave -noupdate /tb_p03/uut/fsm_feed/push_2
add wave -noupdate /tb_p03/uut/fsm_feed/push_3
add wave -noupdate /tb_p03/uut/fsm_feed/push_v
add wave -noupdate /tb_p03/uut/fsm_feed/transmit
add wave -noupdate /tb_p03/uut/fsm_feed/uart_cnt_ovf
add wave -noupdate /tb_p03/uut/fsm_feed/send_cnt_ovf
add wave -noupdate -divider SYNCH
add wave -noupdate /tb_p03/uut/synch_block/clk
add wave -noupdate /tb_p03/uut/synch_block/rst
add wave -noupdate /tb_p03/uut/synch_block/n_synch_pop
add wave -noupdate /tb_p03/uut/synch_block/synch_pop
add wave -noupdate /tb_p03/uut/synch_block/n_synch_pop2
add wave -noupdate /tb_p03/uut/synch_block/synch_pop2
add wave -noupdate /tb_p03/uut/synch_block/n_synch_push
add wave -noupdate /tb_p03/uut/synch_block/synch_push
add wave -noupdate /tb_p03/uut/synch_block/n_synch_end
add wave -noupdate /tb_p03/uut/synch_block/synch_end
add wave -noupdate -divider {FSM MXV}
add wave -noupdate /tb_p03/uut/fsm_mxv/start_proc
add wave -noupdate /tb_p03/uut/fsm_mxv/edo_actual
add wave -noupdate /tb_p03/uut/fsm_mxv/edo_siguiente
add wave -noupdate /tb_p03/uut/fsm_mxv/end_proc
add wave -noupdate /tb_p03/uut/fsm_mxv/ovf_pu
add wave -noupdate /tb_p03/uut/fsm_mxv/start_flush
add wave -noupdate /tb_p03/uut/fsm_mxv/pop
add wave -noupdate /tb_p03/uut/fsm_mxv/pop_v2
add wave -noupdate /tb_p03/uut/fsm_mxv/push_out
add wave -noupdate /tb_p03/uut/fsm_mxv/second_run
add wave -noupdate -divider FIFO_0
add wave -noupdate -radix decimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[15]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[14]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[13]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[12]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[11]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[10]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[9]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[8]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[15]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[14]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[13]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[12]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[11]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[10]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[9]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[8]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_0/ram/ram[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_0/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_0
add wave -noupdate /tb_p03/uut/FIFO_farm/pop
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/fifo_out_0
add wave -noupdate -divider FIFO_1
add wave -noupdate -radix decimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[15]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[14]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[13]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[12]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[11]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[10]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[9]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[8]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[15]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[14]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[13]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[12]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[11]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[10]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[9]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[8]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_1/ram/ram[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_1/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_1
add wave -noupdate /tb_p03/uut/FIFO_farm/pop
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/fifo_out_1
add wave -noupdate -divider FIFO_2
add wave -noupdate -radix decimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[15]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[14]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[13]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[12]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[11]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[10]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[9]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[8]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[15]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[14]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[13]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[12]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[11]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[10]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[9]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[8]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_2/ram/ram[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_2/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_2
add wave -noupdate /tb_p03/uut/FIFO_farm/pop
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/fifo_out_2
add wave -noupdate -divider FIFO_3
add wave -noupdate -radix decimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[15]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[14]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[13]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[12]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[11]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[10]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[9]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[8]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[15]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[14]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[13]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[12]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[11]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[10]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[9]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[8]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_3/ram/ram[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_3/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_3
add wave -noupdate /tb_p03/uut/FIFO_farm/pop
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/fifo_out_3
add wave -noupdate -divider FIFO_V
add wave -noupdate -radix hexadecimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[15]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[14]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[13]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[12]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[11]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[10]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[9]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[8]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[15]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[14]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[13]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[12]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[11]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[10]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[9]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[8]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_v/ram/ram[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_v/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_v
add wave -noupdate /tb_p03/uut/FIFO_farm/pop
add wave -noupdate /tb_p03/uut/FIFO_farm/fifo_out_v
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/out_fifo_v
add wave -noupdate -divider FIFO_V2
add wave -noupdate -radix hexadecimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[15]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[14]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[13]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[12]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[11]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[10]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[9]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[8]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[7]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[6]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[5]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[4]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[3]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[2]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[1]} -radix decimal} {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[0]} -radix decimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[15]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[14]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[13]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[12]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[11]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[10]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[9]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[8]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[7]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[6]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[5]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[4]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[3]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[2]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[1]} {-height 15 -radix decimal} {/tb_p03/uut/FIFO_farm/fifo_v2/ram/ram[0]} {-height 15 -radix decimal}} /tb_p03/uut/FIFO_farm/fifo_v2/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_v
add wave -noupdate /tb_p03/uut/FIFO_farm/pop_v2
add wave -noupdate /tb_p03/uut/FIFO_farm/fifo_out_v2
add wave -noupdate -radix hexadecimal /tb_p03/uut/FIFO_farm/out_fifo_v
add wave -noupdate -divider PU
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/fifo_out_0
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/fifo_out_1
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/fifo_out_2
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/fifo_out_3
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/out_fifo_v
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/clean_acc
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/count_fifo
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/byte_sel
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/received_result
add wave -noupdate -radix unsigned -childformat {{{/tb_p03/uut/PU_farm/result_acc_0[17]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[16]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[15]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[14]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[13]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[12]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[11]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[10]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[9]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[8]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[7]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[6]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[5]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[4]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[3]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[2]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[1]} -radix unsigned} {{/tb_p03/uut/PU_farm/result_acc_0[0]} -radix unsigned}} -subitemconfig {{/tb_p03/uut/PU_farm/result_acc_0[17]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[16]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[15]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[14]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[13]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[12]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[11]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[10]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[9]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[8]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[7]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[6]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[5]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[4]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[3]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[2]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[1]} {-radix unsigned} {/tb_p03/uut/PU_farm/result_acc_0[0]} {-radix unsigned}} /tb_p03/uut/PU_farm/result_acc_0
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_acc_1
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_acc_2
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_acc_3
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_0
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_1
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_2
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_3
add wave -noupdate -radix unsigned /tb_p03/uut/PU_farm/result_flush_val
add wave -noupdate -radix hexadecimal /tb_p03/uut/PU_farm/en_acc
add wave -noupdate -divider FIFO_OUT
add wave -noupdate /tb_p03/uut/FIFO_farm/clk_tx
add wave -noupdate /tb_p03/uut/FIFO_farm/fifo_out_m/ram/ram
add wave -noupdate /tb_p03/uut/FIFO_farm/push_out
add wave -noupdate /tb_p03/uut/FIFO_farm/pop_out
add wave -noupdate -radix hexadecimal -childformat {{{/tb_p03/uut/FIFO_farm/fifo_out[7]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[6]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[5]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[4]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[3]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[2]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[1]} -radix hexadecimal} {{/tb_p03/uut/FIFO_farm/fifo_out[0]} -radix hexadecimal}} -subitemconfig {{/tb_p03/uut/FIFO_farm/fifo_out[7]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[6]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[5]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[4]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[3]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[2]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[1]} {-height 15 -radix hexadecimal} {/tb_p03/uut/FIFO_farm/fifo_out[0]} {-height 15 -radix hexadecimal}} /tb_p03/uut/FIFO_farm/fifo_out
add wave -noupdate -divider cnt
add wave -noupdate /tb_p03/uut/cnt_block_uart/clk
add wave -noupdate /tb_p03/uut/cnt_block_uart/rst
add wave -noupdate /tb_p03/uut/cnt_block_uart/clean_cnt
add wave -noupdate /tb_p03/uut/cnt_block_uart/clean_sys
add wave -noupdate /tb_p03/uut/cnt_block_uart/uart_fail_restart
add wave -noupdate /tb_p03/uut/cnt_block_uart/count_uart_mxv_enb
add wave -noupdate /tb_p03/uut/cnt_block_uart/uart_fail_enb
add wave -noupdate /tb_p03/uut/cnt_block_uart/count_uart_mxv_ovf
add wave -noupdate /tb_p03/uut/cnt_block_uart/uart_fail_ovf
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {591114228 ps} 0} {{Cursor 2} {46061871 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 92
configure wave -valuecolwidth 74
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {41043204 ps} {51813462 ps}
