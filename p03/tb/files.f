../src/top_uart_pkg.sv
../src/clk_div_pkg.sv
../src/dbcr_pkg.sv
../src/sdp_sc_ram_pkg.sv

//UART
../src/tx_module.sv
../src/top_uart.sv
../src/top_test.sv
../src/rx_module.sv
../src/fsm_rx.sv
../src/fsm_tx.sv

//FIFO
../src/fifo.sv
../src/fsm_fifo.sv
../src/sdp_sc_ram.sv
../src/sdp_sc_ram_if.sv

//MXV
../src/proc_unit.sv
../src/fsm_feed.sv
../src/fsm_mxv.sv
../src/top_mxv.sv
../src/PU_farm.sv
../src/FIFO_farm.sv
../src/synch_block.sv
../src/cnt_block_feed.sv
../src/cnt_block_mxv.sv
../src/cnt_block_uart.sv
../src/uart_frame_detect.sv

//SYSTEM
../src/bin_counter_ovf_clean.sv
../src/holder.sv
../src/dinamMAX_counter_ovf.sv
../src/cntr_mod_n_ovf.sv
../src/oneShot_dbcr.sv
../src/sipo_msb.sv
../src/pipo_double.sv
../src/double_counter_ovf.sv
../src/dinamic_counter_ovf.sv
../src/piso_lsb.sv
../src/pipo_Nenb.sv
../src/pipo_clean.sv
../src/comparator.sv
../src/bin_counter_ovf.sv
../src/anti_oneShot.sv
../src/oneShot.sv
../src/synch.sv
../src/clk_div.sv

tb_uart.sv
tb_p03.sv
