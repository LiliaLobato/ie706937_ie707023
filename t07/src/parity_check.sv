// =============================================================================
// Title       	:	PArity Checker
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t07
// File        	: 	parity_check.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

module parity_check 
import UART_pkg::*;
(
	input DATA_BUS_t data,
	output logic parity_bit
);

	
	logic bit_0;  
	logic bit_1;
	logic bit_2;
	logic bit_3;
	logic bit_4;
	logic bit_5;
	logic bit_6;
	logic bit_7;
	
	assign bit_0 = data[0]; 
	assign bit_1 = data[1];
	assign bit_2 = data[2];
	assign bit_3 = data[3];
	assign bit_4 = data[4];
	assign bit_5 = data[5];
	assign bit_6 = data[6];
	assign bit_7 = data[7];
	
	CNTR_t parity_cntr = '0;
	CNTR_t aux = '0;

	always_comb begin
	
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_0)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_1)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_2)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_3)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_4)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_5)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_6)? (aux + 1'b1) : aux;
//		
//		aux = parity_cntr; //Last value of parity_cntr
//		parity_cntr = (bit_7)? (aux + 1'b1) : aux;

		parity_cntr = bit_0 + bit_1 + bit_2 + bit_3 + bit_4 + bit_5 + bit_6 + bit_7;
		
		parity_bit = (parity_cntr%2)? 1'b1 : '0; //counter%2 determines if we have a pair lenght of bits, if its pair result is 0 so parity bit must be 0 and vice versa.
	
	end

endmodule 