// =============================================================================
// Title       	:	Implementacion de un divisor de frecuencia
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t03
// File        	: 	clk_div.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module clk_gen
import clk_div_pkg::*;
#(
	parameter FREQUENCY = FREQUENCY_TB,
	parameter REFERENCE_CLK = REFERENCE_CLKFPGA //50_000_000;
)(
	input  logic	clk,
	input  logic	rst,
	output logic	clk_out   
);

logic clk_pll;

`ifdef SIM_ON
	assign clk_pll = clk;
`else
	pl pll_inst (
		.areset ( !rst ),
		.inclk0 ( clk ),
		.c0 ( clk_pll )
	);
`endif

clk_div #(
		.FREQUENCY      (FREQUENCY_CLKDIV)
	`ifdef SIM_ON
		,.REFERENCE_CLK  (REFERENCE_CLKTB)
	`else
		,.REFERENCE_CLK  (REFERENCE_CLKPLL)
	`endif	
) clk_divisor (
	.clk    (clk_pll),
	.rst    (rst),
	.clk_gen(clk_out)
);

endmodule