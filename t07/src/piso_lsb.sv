// =============================================================================
// Title        : Piso lsb
//                  The parallel input will be shifted starting on the lsb
//                  in order to generate a serial output 
// Inputs       : System clk, reset, enb,load/shift, parallel input
// Outputs      : Serial output
// Project      :   t05
// File         :   piso_lsb.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module piso_lsb #(
parameter DW = 8
) (
input               clk,    // Clock
input               rst,    // asynchronous reset low active
input               enb,    // Enable
input               l_s,    // load or shift
input  [DW-1:0]     inp,    // data input
output              out     // Serial output
);

logic [DW-1:0]      rgstr_r     ;
logic [DW-1:0]      rgstr_nxt   ;

// Combinational module
always_comb begin
    if (l_s)
    	rgstr_nxt  = inp;
    else 
    	rgstr_nxt  = {rgstr_r[0], rgstr_r[DW-1:1]};
end

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (enb) 
        rgstr_r     <= rgstr_nxt    ;
end:rgstr_label

assign out  = rgstr_r[DW-1];    // MSB bit is the first to leave
//TODO: try to design a piso register, where the LSB bit leave the register first and then the LSB bit+1.
endmodule
