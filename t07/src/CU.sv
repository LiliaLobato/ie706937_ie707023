// =============================================================================
// Title       	: Control Unit
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t07
// File        	: 	CU.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

module CU
import UART_pkg::*;
(
	input				clk,
	input         	rst,
	input				Clear_Interrrupt,
	input				Transmit_flag,
	input				parity_bit,
	input				Rx_parity_bit,
	input				start_bit,
	input				stop_bit,
	output logic	in_en,
	output logic	out_en,
	output logic	Rx_en,
	output logic	Tx_en,
	output logic	Parity_error_flag,
	output logic	Rx_Interrupt,
	output logic	l_s
);

UART_state Edo_Actual;
UART_state Edo_Siguiente;
logic previous_lsb;
logic interrupt_aux;
	

//Counter to end the addition
CNTR_t cntr = '0;

always_ff@(posedge clk or negedge rst) begin
	if (!rst)
		cntr <=  '0;
	else begin 
		case(Edo_Actual)
			IDLE_STATE:	cntr <= '0;
			Rx_STATE:	cntr <= cntr + 1'b1;
			Tx_STATE: 	cntr <= cntr + 1'b1;
		endcase
	end
end 

always_comb begin 
	case (Edo_Actual)
		IDLE_STATE:begin 
			if (Transmit_flag == 1)
				Edo_Siguiente =  Tx_STATE; // Start Serial Transmission
			else if (start_bit == 0)
				Edo_Siguiente =  Rx_STATE; // Start bit detected
			else 
				Edo_Siguiente =  IDLE_STATE; // Continue in the same state
		end

		Rx_STATE:begin 
			if ( (start_bit == 0) && (stop_bit == 1) && (cntr >=22))	// Start and Stop bit correct //Cntr is Double of N bits because we are sampling at 2 times the frequency.
				Edo_Siguiente =  IDLE_STATE;
			else 
				Edo_Siguiente =  Rx_STATE; // Continue in the same state
		end 
		
		Tx_STATE:begin 
			if (Tx_en) //Double of N bits because we are sampling at 2 times the frequency.
				Edo_Siguiente =  IDLE_STATE;
			else 
				Edo_Siguiente =  Tx_STATE; // Continue in the same state
		end 
	endcase 
end 


// Registros or flop array
always_ff@(posedge clk or negedge rst) begin// your next state is now your current state
   if (!rst)
      Edo_Actual  <= IDLE_STATE;
   else
      Edo_Actual  <= Edo_Siguiente ;
end

// CTO combinacional de salida.
always_comb begin
	case(Edo_Actual)
		IDLE_STATE:begin
			in_en	= 1'b1;
			out_en	= '0;
			Rx_en	= 1'b1;
			Tx_en	= '0;
			Parity_error_flag	= '0;
			Rx_Interrupt	= '0;
			l_s		= '0;
		end 
	 
		Rx_STATE:begin
			in_en	= '0;
			out_en	= 1'b1;
			Rx_en	= 1'b1;
			Tx_en	= '0;
			l_s		= 1'b1;
			
			if(Rx_parity_bit == parity_bit)
				Parity_error_flag	= '0;
			else
				Parity_error_flag	= '0;
			
			if ( cntr == 22) //Double of N bits because we are sampling at 2 times the frequency.
				Rx_Interrupt = 1'b1;
			else
				Rx_Interrupt = '0;
			
		end
		
		Tx_STATE:begin
			in_en	= '0;
			out_en	= '0;
			Rx_en	= '0;
			l_s		= '0;
			Parity_error_flag	= '0;
			Rx_Interrupt	= '0;
			if (cntr == 22 )
				Tx_en	= 1'b1;
			else
				Tx_en	= '0;
		end
	endcase
end

assign interrupt_aux = Rx_Interrupt;

//always_ff@(posedge clk or negedge rst or posedge Clear_Interrrupt) begin
//
//	if(Clear_Interrrupt)
//		Rx_Interrupt <= '0;
//
//
//end


endmodule
