// =============================================================================
// Title       	:	UART Top
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t07
// File        	: 	UART.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

module UART 
import UART_pkg::*;
(
	input clk,
	input rst,
	input Serial_Data_Rx,
	input Transmit_flag,
	input Clear_Interrrupt, 
	input DATA_BUS_t Received_Data,
	output Serial_Data_Tx,
	output Rx_Interrupt,
	output Parity_error_flag,
	output DATA_BUS_t Data_2_Transmit
	
);

	DATA_BUS_t Received_Data_2_PISO_LOGIC;
	UART_BUS_t SD_Rx_SIPO_2_PARITY_LOGIC;
	DATA_BUS_t RD_PISO_2_PARITY_GEN_LOGIC;
	DATA_BUS_t PARITY_CHECK_2_SIPO_LOGIC;
	UART_BUS_t	UART_BUS_Rx;
	UART_BUS_t	UART_BUS_Tx;
	logic parity_bit_logic;
	logic Tx_parity_bit_logic;
	logic PARITY_ERROR_LOGIC;
	logic in_en_logic;
	logic out_en_logic;
	logic Rx_en_logic;
	logic Tx_en_logic;
	logic l_s_logic;
	//Clk_gen
logic clk_gen;
logic clk_pll;

// =============================================================================
// Clk generator for baudrate 
// =============================================================================

clk_gen clk_gen_50M_5M (
	.clk    (clk),
	.rst    (rst),
	.clk_out(clk_gen)
);

	CU
	Control_Unit(
		.clk(clk_gen),
		.rst(rst),
		.Clear_Interrrupt(Clear_Interrrupt),
		.Transmit_flag(Transmit_flag),
		.parity_bit(parity_bit_logic),
		.Rx_parity_bit(SD_Rx_SIPO_2_PARITY_LOGIC[9]), //Stop,PARITY,8'b(data),Start
		.in_en(in_en_logic),
		.start_bit(SD_Rx_SIPO_2_PARITY_LOGIC[0]),
		.stop_bit(SD_Rx_SIPO_2_PARITY_LOGIC[10]),
		.out_en(out_en_logic),
		.Rx_en(Rx_en_logic),
		.Tx_en(Tx_en_logic),
		.Parity_error_flag(Parity_error_flag),
		.Rx_Interrupt(Rx_Interrupt),
		.l_s(l_s_logic)
	);

	//Input PIPO for Received Data
	pipo #(
		.DW	(DW)
	)in_RD_PIPO(
		.clk	(clk_gen),
		.rst	(rst),
		.enb	(in_en_logic),
		.inp	(Received_Data),
		.out	(Received_Data_2_PISO_LOGIC)
	);
	
		parity_check
	Tx_parity_checker(
	
		.data(Received_Data_2_PISO_LOGIC), 
		.parity_bit(Tx_parity_bit_logic)
	
	);
		
	piso_lsb #(
		.DW(UART_EXTENSION) //DW+3
	)in_RD_PISO(
	.clk(clk_gen),    // Clock
	.rst(rst),    // asynchronous reset low active
	.enb(Tx_en_logic),    // Enable
	.l_s(l_s_logic),    // load or shift
	.inp({1'b1, Tx_parity_bit_logic, Received_Data_2_PISO_LOGIC,'0}),    //Stop,PARITY,8'b(data),Start
	.out(Serial_Data_Tx)     // PISO to Serial Output Tx
	);
	
	sipo_msb #(
		.DW(UART_EXTENSION) //DW+3
	)in_SD_SIPO(
    .clk(clk_gen),
    .rst(rst),
    .enb(Rx_en_logic),
    .inp(Serial_Data_Rx),
    .out(SD_Rx_SIPO_2_PARITY_LOGIC)
	);
	
	
	parity_check
	Rx_parity_checker(
	
		.data(SD_Rx_SIPO_2_PARITY_LOGIC[8:1]), //Stop,PARITY,8'b(data),Start
		.parity_bit(parity_bit_logic)
	
	);
	
//	assign PARITY_ERROR_LOGIC = (SD_Rx_SIPO_2_PARITY_LOGIC[9] == parity_bit_logic)? '0: 1'b1; //Stop,PARITY,8'b(data),Start
	
			//Output PIPO Data 2 Transmit
	pipo #(
		.DW	(DW) //DW
	)out_D2T_PIPO(
		.clk	(clk_gen),
		.rst	(rst),
		.enb	(out_en_logic),
		.inp	(SD_Rx_SIPO_2_PARITY_LOGIC[8:1]),
		.out	(Data_2_Transmit)
	);
	
	

endmodule 