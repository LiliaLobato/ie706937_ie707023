// =============================================================================
// Title       	:	Package
// Project     	: 	t03
// File        	: 	clk_div_pkg.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

`ifndef CLK_DIV_PKG_SV
`define CLK_DIV_PKG_SV

package clk_div_pkg;

	localparam FREQUENCY_TB = 100;
	localparam FREQUENCY_1HZ = 1;
	localparam REFERENCE_CLKTB = 2;
	localparam REFERENCE_CLKFPGA = 50_000_000;

endpackage

`endif