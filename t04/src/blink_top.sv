// =============================================================================
// Title       	:	Implementacion de un fsmb para generar blinking
// Inputs		:	System clk, reset and start
// Outputs		:	clock generated and blink status
// Project     	: 	t04
// File        	: 	blink_top.sv
// Date 		: 	12/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

import fsm_moore_pkg::*;

module blink_top
import clk_div_pkg::*;
(
	input  logic	clk,
	input  logic	rst,
	input  logic	start,
	output logic	out,
	output logic	clk_gen   
);

logic Delay30ms_ready; 
logic EnableCounter; 
logic dbcr_start;
logic [W_IN-1:0] state_cntr;
logic enable;
logic [2:0] n_times;

// =============================================================================
// Clk generator for 1Hz
// =============================================================================

clk_div #(
	.FREQUENCY      (FREQUENCY_1HZ)
	`ifdef SIM_ON
		,.REFERENCE_CLK  (REFERENCE_CLKTB)
	`endif	
) clk_divisor (
	.clk    (clk),
	.rst    (rst),
	.clk_gen(clk_gen)
);

// =============================================================================
// Debouncer
// =============================================================================

fsm_dbcr  i_fsm_dbcr (
    .clk            (clk),
	.rst_n          (rst),
    .Din            (!start),
    .Delay30ms_ready(Delay30ms_ready),
    .EnableCounter  (EnableCounter),
    .one_shot       (dbcr_start)
);

always_ff@(posedge clk, negedge rst)begin: counter
   if (!rst )
        enable     <=  '0 ;
   else if (n_times == MAX_TIMES+1)
        enable     <=  '0 ;    
   else if (dbcr_start != '0)
        enable <= dbcr_start;    
   else
        enable <= enable;
end:counter

cntr_mod_n_ovf #(
`ifdef SIM_ON
.FREQ  (REFERENCE_CLKTB),
.DLY(0.5)
`else 
.FREQ(REFERENCE_CLKFPGA),
.DLY(0.03)
`endif
) i_cntr_mod_n (
    .clk    ( clk               ),
    .rst    ( rst             	),
    .enb    ( EnableCounter     ),
    .ovf    ( Delay30ms_ready   ),
    .count  ()
);
// =============================================================================
// Maquina de estados
// =============================================================================
bin_counter_ovf #(
.DW(W_IN), 
.MAXCNT(11)
) cntr (
	.clk(clk_gen),
	.rst(rst),
	.enb(enable),
	.ovf(),
	.count(state_cntr)
);


fsm_moore state_OnOff(
    .X(state_cntr),
    .En(enable),
    .Rst(rst),
    .Clk(clk_gen),
    .Cuenta(out),
    .times(n_times)
);


endmodule