`ifndef FSM_MOORE_PKG_SV
    `define FSM_MOORE_PKG_SV
package fsm_moore_pkg;

    localparam W_IN = 5;
    localparam W_ST = 2;
    localparam W_VAL = 1;
    localparam MAX_TIMES = 2;
    typedef logic [3:0]         times_t;
    typedef logic [W_IN-1:0]    data_t;
    typedef logic [W_ST-1:0]    state_t;
    typedef logic [W_VAL-1:0]   value_t; 
    typedef enum state_t{
        STATE_ON    = 2'b01,
        STATE_OFF   = 2'b10,
        IDLE        = 2'b11
    } state_e;

    typedef enum value_t{
        ON  = 1'b1,
        OFF   = 1'b0
    } value_e;

endpackage
`endif
 
