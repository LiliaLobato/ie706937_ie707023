`timescale 1ns / 1ps
// =============================================================================
// Title        : Fsmb de tres estados:
//                  -> Idle
//                  -> ON
//                  -> OFF
//                Genera una secuencia de 6 on y 4 off 
// Inputs       : System clk, reset and start
// Outputs      : clock generated and blink status
// Project      :   t04
// File         :   fsm_moore.sv
// Date         :   12/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          Gabriel Paz    ie707023
// =============================================================================

module fsm_moore
import fsm_moore_pkg::*;
(
    input data_t        X,
    input               En,
    input               Rst,
    input               Clk,
    output value_e      Cuenta,
    output times_t      times
    );

state_e Edo_Actual;	
state_e edo_siguiente;
times_t n_times;
times_t n_times_next;

// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(Edo_Actual)
      STATE_ON:begin // LED encendido
      	if (X<6) begin
      		edo_siguiente=STATE_ON;
          n_times_next = n_times;
      	end else begin
      		edo_siguiente=STATE_OFF;
          n_times_next = n_times;
      	end end
      STATE_OFF:begin // LED apagadp
      	if (X<10) begin
      		edo_siguiente=STATE_OFF;
          n_times_next = n_times;
      	end else if (n_times == MAX_TIMES) begin
          edo_siguiente=IDLE;
          n_times_next = n_times;
        end else begin
      		edo_siguiente=STATE_ON;
          n_times_next = n_times+1;
      	end end
      IDLE:begin // LED apagaado sin hacer algo
        if (X<6 && n_times < MAX_TIMES) begin
          edo_siguiente=STATE_ON;
          n_times_next = '0;
        end else begin
          edo_siguiente=IDLE;
          n_times_next = (n_times_next==5 || n_times_next==0)?'0:n_times+1;
        end end
   endcase
end

//asignaciones por default y siguiente
always_ff@(posedge Clk or negedge Rst) begin // Circuito Secuenicial en un proceso always.
   if (!Rst) begin 
      Edo_Actual  <= IDLE;
      n_times <= '0; 
   end
   else if (En) begin
      Edo_Actual  <= edo_siguiente;
      n_times <= n_times_next; 
    end else begin
      Edo_Actual  <= Edo_Actual;
      n_times <= n_times_next;
end end

// CTO combinacional de salida.
always_comb begin
  case(Edo_Actual)
      STATE_ON   :    Cuenta = ON; 
      STATE_OFF  :    Cuenta = OFF;
      IDLE       :    Cuenta = OFF;
  endcase
  times = n_times;
end

endmodule
