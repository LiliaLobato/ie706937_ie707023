if [file exists work] {vdel -all}
vlib work
vlog +define+SIM_ON -f files.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.tb_blink
do wave.do
run 20000ms
