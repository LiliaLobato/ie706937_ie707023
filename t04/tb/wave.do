onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_blink/rst
add wave -noupdate -color Magenta /tb_blink/clk
add wave -noupdate -color Magenta /tb_blink/clk_gen
add wave -noupdate -color {Medium Slate Blue} /tb_blink/start
add wave -noupdate /tb_blink/uut/dbcr_start
add wave -noupdate /tb_blink/uut/enable
add wave -noupdate -color {Medium Slate Blue} /tb_blink/out
add wave -noupdate -divider state_machine
add wave -noupdate -radix decimal /tb_blink/uut/state_OnOff/X
add wave -noupdate /tb_blink/uut/state_OnOff/En
add wave -noupdate /tb_blink/uut/state_OnOff/Cuenta
add wave -noupdate /tb_blink/uut/state_OnOff/Edo_Actual
add wave -noupdate -radix decimal /tb_blink/uut/state_OnOff/times
add wave -noupdate -divider cntr
add wave -noupdate /tb_blink/uut/cntr/ovf
add wave -noupdate -radix decimal /tb_blink/uut/cntr/count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1237992 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 242
configure wave -valuecolwidth 39
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {942406 ps}
