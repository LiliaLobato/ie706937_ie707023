// =============================================================================
// Title        :   Implementacion de un fsmb para generar blinking
// Inputs       :   System clk, reset and start
// Outputs      :   clock generated and blink status
// Project      :   t04
// File         :   blink_top.sv
// Date         :   12/02/2020



`timescale 1ns / 1ps
module tb_blink();
import clk_div_pkg::*;
import dbcr_pkg::*;

logic    clk;
logic    rst;
logic    start;
logic    out;
logic    clk_gen;

blink_top uut (
    .clk        (clk),
    .rst        (rst),
    .start      (start),
    .out        (out),
    .clk_gen    (clk_gen)
);


initial begin
    clk = 0;
end

always begin
        #5  clk = ~clk;
end

initial begin 
    rst = 0;
    #200 rst = 1;
    start = 1;

    #200

    //double start
    #20 start = 0;
    #20 start = 1;
    #20 start = 0;
    #20 start = 1;

    //second start
    #3000
    #20 start = 0;
    #20 start = 1;
    
    #5000
    $stop;
end

endmodule
