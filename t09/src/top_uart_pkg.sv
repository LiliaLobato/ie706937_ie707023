`ifndef TOP_UART_PKG_SV
	`define TOP_UART_PKG_SV

package top_uart_pkg;

// Frame size
localparam DW_PC = 8;
localparam CNT_DW_PC = $clog2(DW_PC)+1;
typedef logic [DW_PC-1:0] uart_baud_t;
localparam HDW_PC = DW_PC/2;
localparam CNT_HDW_PC = $clog2(HDW_PC)+1;

// tX FSM 
localparam TX_OUT_W = 2;
typedef enum logic [TX_OUT_W-1:0]
{
	START_OR_STOP 	= 2'b00,
	DATA_OUT		= 2'b01,
	PARITY_BIT		= 2'b10,
	IDLE_BIT 		= 2'b11
}tx_out_e;

//uart state
localparam SM_W = 3;
typedef enum logic [SM_W-1:0]
{
	IDLE		= 3'b000,
	START		= 3'b001,
	SHIFT		= 3'b010,
	PARITY		= 3'b011,
	STOP		= 3'b100,
	ERROR		= 3'b101
}uart_state_e;

//Anti OneShot
localparam AOS_W = 3;
typedef enum logic [AOS_W-1:0]
{
	IDLE_AS		= 3'b000,
	CYCLE_1_AS	= 3'b001,
	CYCLE_2_AS	= 3'b010,
	CYCLE_3_AS	= 3'b011,
	CYCLE_4_AS	= 3'b100
}aos_state_e;

localparam ON  = 1'b1;
localparam OFF = 1'b0;

endpackage
`endif