
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

module double_counter_ovf #(
parameter 
DW      = 4, 
MAXCNT  = 5,
RST_VAL = 0
)(
input           clk,
input           rst,
input           enb_1,
input           enb_2,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;
logic enb;
assign enb = enb_1 | enb_2;

always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  RST_VAL[DW-1:0];
    else if (enb)
        count_r     <= count_nxt    ;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
    if (enb_1) begin
        if (count_r == MAXCNT-1'b1) begin
            count_nxt = RST_VAL[DW-1:0];
        end else begin
            count_nxt =  count_r + 1'b1;
        end
    end else if (enb_2) begin
            count_nxt =  MAXCNT[DW-1:0]-2'b10;
    end else 
        count_nxt =  MAXCNT[DW-1:0]-2'b10;

    if (count_r == MAXCNT-1'b1)
            ovf  = 1'b1;
    else
            ovf  = 1'b0;
end

assign count    =   count_r;

endmodule

