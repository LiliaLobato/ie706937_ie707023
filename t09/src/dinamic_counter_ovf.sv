
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset


module dinamic_counter_ovf #(
parameter 
DW      = 4,            //4
MAXCNT_1  = 10,         //9
MAXCNT_2  = MAXCNT_1,   //8
RST_VAL = 0
)(
input           clk,
input           rst,
input           enb,
output logic    ovf,
output [DW-1:0] count
);

logic [DW-1:0] count_r, count_nxt;
logic first;

always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst) begin
        count_r     <=  RST_VAL[DW-1:0];
    end else if (enb)
        count_r     <= count_nxt;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
    if (count_r == (first?MAXCNT_1:MAXCNT_2)-1'b1) begin
        ovf       = 1'b1;
        count_nxt = RST_VAL[DW-1:0];
        first = '0;
    end else begin
        ovf     =   1'b0;
        first       <= '1;
        count_nxt =  count_r + 1'b1;
    end
end

assign count    =   count_r;

endmodule