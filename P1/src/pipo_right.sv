module pipo_right 
import P1_pkg::*;
#(
	parameter DW = 4
)(
input               clk,
input               rst,
input					  l_s,
input               enb,
input  [DW-1:0]     inp,
output [(2*DW)-1:0]     out
);

logic [DW-1:0]      rgstr_r     ;
logic [DW-1:0]      rgstr_nxt   ;

always_comb begin
    if (l_s)
    	rgstr_nxt  = inp;
    else 
    	rgstr_nxt  = {'0, rgstr_r[DW-1:1]}; //corregir
end

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= rgstr_nxt;
end:rgstr_label

assign out  = rgstr_r;

endmodule
