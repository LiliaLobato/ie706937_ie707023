// =============================================================================
// Title        : One Shot
// Inputs       : System clk, reset, n_times, dbcrs_start
// Outputs      : enable
// Project      :   t05
// File         :   oneShot.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module oneShot #(
	parameter MAX_TIMES = 4
) (
	input  clk,
	input  rst,
	input  dbcr_start,
	input  [3:0] n_times,
	output enable
);

logic enable_r;

always_ff@(posedge clk, negedge rst)begin
   if (!rst )
        enable_r     <=  '0 ;
   else if (n_times == MAX_TIMES)
        enable_r     <=  '0 ;    
   else if (dbcr_start != '0)
        enable_r <= dbcr_start;    
   else
        enable_r <= enable_r;
end

assign enable  = enable_r;

endmodule

