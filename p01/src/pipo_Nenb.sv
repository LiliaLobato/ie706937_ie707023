// =============================================================================
// Title        : Pipo
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, parallel input
// Outputs      : Parallel output
// Project      :   t05
// File         :   pipo_Nenb.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module pipo_Nenb #(
parameter DW = 4
) (
input               clk,
input               rst,
input  [DW-1:0]     inp,
output [DW-1:0]     out
);

logic [DW-1:0]      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else
        rgstr_r  <= inp;
end:rgstr_label

assign out  = rgstr_r;

endmodule

