// =============================================================================
// Title        :   Package
// Project      :   t03
// File         :   fsm_moore_pkg.sv
// Date         :   01/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`ifndef MULT_TOP_PKG_SV
    `define MULT_TOP_PKG_SV
package mult_top_pkg;

    `ifdef SIM_ON
        localparam IN_DW = 4;
    `else
        localparam IN_DW = 9;
    `endif


    localparam WIDTH_CNT = $clog2(IN_DW)+1;

    typedef logic [IN_DW-1:0]        int_t;
    typedef logic [(IN_DW*2)-1:0]    out_t; 
    
endpackage
`endif
 
