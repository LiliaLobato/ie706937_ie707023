`timescale 1ns / 1ps
// =============================================================================
// Title        : Fsmb de dos estados:
//                  -> Idle
//                  -> SHIFT
//                Genera una secuencia de DW shifts  
// Inputs       : System clk, reset, data_in, enable and start
// Outputs      : pipo_en, done_en, load_en, times
// Project      :   p01
// File         :   fsm_moore.sv
// Date         :   12/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module fsm_moore
import fsm_moore_pkg::*;
#(
  parameter DW_TIMES = 4
)(
    input               En,
    input               Rst,
    input               Clk,
    input               ovf,
    output value_t      pipo_en,
    output value_t      done_en,
    output value_t      load_en,
    output value_t      ready_en
    );

state_e Edo_Actual;	
state_e edo_siguiente;


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(Edo_Actual)
      STATE_SHIFT:begin // LED encendido
      	if ( ovf ) begin
          edo_siguiente=IDLE;
      	end else begin
          edo_siguiente=STATE_SHIFT;
        end 
      end
      IDLE:begin // LED apagaado sin hacer algo
        if ( En ) begin
          edo_siguiente=STATE_SHIFT;
        end else begin
          edo_siguiente=IDLE;
        end
      end
   endcase
end

//asignaciones por default y siguiente
always_ff@(posedge Clk or negedge Rst) begin // Circuito Secuenicial en un proceso always.
   if (!Rst) 
      Edo_Actual  <= IDLE;
   else 
      Edo_Actual  <= edo_siguiente;
end

// CTO combinacional de salida.
always_comb begin
case(Edo_Actual)
    IDLE : begin
        pipo_en=OFF;
        done_en=ON;
        load_en=ON;
    end
    STATE_SHIFT: begin
        pipo_en=ON;
        done_en=OFF;
        load_en=ovf?ON:OFF;
    end
endcase 
    ready_en = (done_en & load_en);
end

endmodule


