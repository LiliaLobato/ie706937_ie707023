// =============================================================================
// Title       	:	Package
// Project     	: 	t03
// File        	: 	clk_div_pkg.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

`ifndef CLK_DIV_PKG_SV
`define CLK_DIV_PKG_SV

package clk_div_pkg;
	
	localparam FREQUENCY_TB = 100;
	localparam FREQUENCY_1HZ = 1;
	localparam FREQUENCY_10HZ = 10;
	localparam FREQUENCY_1KHZ = 1_000;
	localparam FREQUENCY_10KHZ = 10_000;
    localparam FREQUENCY_CLKDIV = FREQUENCY_10KHZ;

	localparam REFERENCE_CLKTB = 2;
	localparam REFERENCE_CLKFPGA = 50_000_000;
	localparam REFERENCE_CLKPLL = 1_000_000;	


endpackage

`endif