// =============================================================================
// Title       	:	Implementacion de un multiplicador comportamental
//					de dos numeros C2
// Inputs		:	System clk, reset, start, multiplier and multiplicand
// Outputs		:	ready and product
// Project     	: 	p01
// File        	: 	mult_Nseq_top.sv
// Date 		: 	01/03/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

import fsm_moore_pkg::*;
import clk_div_pkg::*;

localparam FREQUENCY_REAL1 = REFERENCE_CLKFPGA;

module mult_Nseq_top
#(
	parameter IN_DW = 9
)(
	input  logic	clk,
	input  logic	rst,
	input  logic	start,
	input  logic signed [IN_DW-1:0]	multiplier,
	input  logic signed [IN_DW-1:0]	multiplicand,
	output logic	ready,
	output logic signed [(IN_DW*2)-1:0]	product   
);

//Debouncer y clk_gen
logic Delay30ms_ready; 
logic EnableCounter; 
logic dbcr_start;
logic enable;
logic [3:0] n_times;
logic out;
logic clk_gen;
logic clk_pll;

logic [IN_DW-1:0]	multiplicand_wr;
logic [IN_DW-1:0]	multiplier_wr;
logic [IN_DW-1:0]	multiplicand_bin;
logic [IN_DW-1:0]	multiplier_bin;
logic 				sign;
logic [(IN_DW*2)-1:0] product_wr_next;
logic [(IN_DW*2)-1:0] product_bin;

// =============================================================================
// Clk generator for 1Hz
// =============================================================================

assign clk_pll = clk;

clk_div #(
		.FREQUENCY      (FREQUENCY_REAL1)
	`ifdef SIM_ON
		,.REFERENCE_CLK  (REFERENCE_CLKTB)
	`else
		,.REFERENCE_CLK  (REFERENCE_CLKPLL)
	`endif	
) clk_divisor (
	.clk    (clk_pll),
	.rst    (rst),
	.clk_gen(clk_gen)
);

// =============================================================================
// Debouncer
// =============================================================================

fsm_dbcr i_fsm_dbcr (
    .clk            (clk_gen),
	.rst_n          (rst),
    .Din            (!start),
    .Delay30ms_ready(Delay30ms_ready),
    .EnableCounter  (EnableCounter),
    .one_shot       (dbcr_start)
);

oneShot #(
.MAX_TIMES(MAX_TIMES)
) oneShot (
	.clk       (clk_gen),
	.rst       (rst),
	.dbcr_start(dbcr_start),
	.n_times   (n_times),
	.enable    (enable)
);

cntr_mod_n_ovf #(
`ifdef SIM_ON
	.FREQ  (REFERENCE_CLKTB),
	.DLY(0.5)
`else 
	.FREQ(FREQUENCY_REAL1),
	.DLY(0.003)
`endif
) i_cntr_mod_n (
    .clk    ( clk_gen           ),
    .rst    ( rst             	),
    .enb    ( EnableCounter     ),
    .ovf    ( Delay30ms_ready   ),
    .count  ()
);


// =============================================================================
// PIPO Entradas
// =============================================================================

pipo_Nenb #(
.DW(IN_DW)
) pipo_multiplicand(
	.clk(clk_gen),
	.rst(rst),
	.inp(multiplicand),
	.out(multiplicand_wr)
);

pipo_Nenb #(
.DW(IN_DW)
) pipo_multiplier(
	.clk(clk_gen),
	.rst(rst),
	.inp(multiplier),
	.out(multiplier_wr)
);

// =============================================================================
// C2 to Binary
// =============================================================================

C22Bin #(
.DW(IN_DW)
) conv_multiplicand(
	.complemento2(multiplicand_wr),
	.done_en     (enable),
	.binario     (multiplicand_bin)
);

C22Bin #(
.DW(IN_DW)
) conv_multiplier(
	.complemento2(multiplier_wr),
	.done_en     (enable),
	.binario     (multiplier_bin)
);

assign sign = multiplier_wr[IN_DW-1] ^ multiplicand_wr[IN_DW-1];

// =============================================================================
// Multiplier
// =============================================================================

always_ff @(posedge clk_gen or negedge rst) begin
	if(~rst) begin
		 product_wr_next <= 0;
	end else begin
		 product_wr_next <= multiplier_bin * multiplicand_bin;
	end
end

// =============================================================================
// Binary to C2 
// =============================================================================

Bin2C2 #(
.DW(IN_DW*2)
) conv_product(
	.signo			(sign),
	.binario     	(product_wr_next),
	.complemento2	(product_bin)
);

// =============================================================================
// PIPO Salidas
// =============================================================================

pipo_Nenb #(
.DW(IN_DW*2)
) pipo_product(
	.clk(clk_gen),
	.rst(rst),
	.inp(product_bin),
	.out(product)
);

endmodule