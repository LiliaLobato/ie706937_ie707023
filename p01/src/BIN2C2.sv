// =============================================================================
// Title        : Bin2C2
//                  The binary value is transformed to C2
// Inputs       : System binario, signo
// Outputs      : complemento2
// Project      :   p01
// File         :   Bin2C2.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module Bin2C2 #(
parameter DW = 9
) (
input  [DW-1:0] binario,
input 			signo, //0 es positivo
output [DW-1:0]	complemento2
);

assign complemento2 = signo?((~binario)+1):binario;

endmodule

