// =============================================================================
// Title       	:	Implementacion de un divisor de frecuencia
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t03
// File        	: 	clk_div.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module clk_div
import clk_div_pkg::*;
#(
	parameter FREQUENCY = FREQUENCY_TB,
	parameter REFERENCE_CLK = REFERENCE_CLKFPGA //50_000_000;
)(
	input  logic	clk,
	input  logic	rst,
	output logic	clk_gen   
);

localparam DIV = REFERENCE_CLK/(2*FREQUENCY);
localparam WIDTH = $clog2(DIV)+1;

logic [WIDTH-1:0] clk_counter;
logic [WIDTH-1:0] clk_nxt;
logic clk_track;

always_ff @(posedge clk or negedge rst)
begin
		if (~rst) begin
			clk_track <= 1'b0;
			clk_counter <= 0;
		end 
		else if (clk_nxt == DIV) begin
			clk_counter <= 0;
			clk_track <= ~clk_track;
			end
		else
			clk_counter <= clk_nxt;
end

assign clk_nxt = clk_counter + 1;
assign clk_gen = clk_track;

endmodule