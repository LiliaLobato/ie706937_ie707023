// =============================================================================
// Title        : Pipo Shift
//                  The parallel input will be the parallel output shifted
//                  Implements algorithm for multiplication
// Inputs       : System clk, reset, enb, sum, mutiplier, inp
// Outputs      : Parallel output
// Project      :   t05
// File         :   pipo_shift.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module pipo_shift #(
parameter DW = 4
) (
input               clk,
input               rst,
input               enb,
input  [(DW/2)-1:0]	sum,
input  [(DW/2)-1:0]	multiplier,
input  [DW-1:0]     inp,
output [DW-1:0]     out
);

logic [DW-1:0]      rgstr_r;

always_ff@(posedge clk, negedge rst)begin
   if (!rst ) begin
        rgstr_r <=  '0 ;
   end else begin
   		if (enb) begin 
   			if (rgstr_r[0])
   				rgstr_r <= {sum,inp[(DW/2)-1:0]}>>1;
   			else
   				rgstr_r <= inp>>1;
   		end else 
   			rgstr_r <= {4'b0,multiplier};
    end 
end

assign out  = rgstr_r;

endmodule

