// =============================================================================
// Title       	:	Implementacion de un multiplicador secuencial
//					de dos numeros C2
// Inputs		:	System clk, reset, start, multiplier and multiplicand
// Outputs		:	ready and product
// Project     	: 	p01
// File        	: 	mult_Nseq_top.sv
// Date 		: 	01/03/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

import fsm_moore_pkg::*;
import clk_div_pkg::*;
import mult_top_pkg::*;

module mult_top(
	input  logic	clk,
	input  logic	rst,
	input  logic	start,
	input  int_t	multiplier,
	input  int_t	multiplicand,
	output logic	ready,
	output out_t	product   
);

//Debouncer y clk_gen
logic Delay30ms_ready; 
logic EnableCounter; 
logic dbcr_start;
data_t state_cntr;
logic enable;
times_t n_times;
logic out;
logic pipo_en;
logic done_en;
logic load_en;
logic ready_en;
logic clk_gen;
logic clk_pll;

int_t	multiplicand_wr;
int_t	multiplicand_wr_next;
int_t	multiplier_wr;
int_t	multiplier_wr_next;
int_t	multiplicand_bin;
int_t	multiplier_bin;
logic 	sign;
logic 	ovf;
logic 	multiplier_sign;
logic 	multiplicand_sign;
out_t 	product_wr_next;
out_t 	product_bin;

// =============================================================================
// Clk generator for 1Hz
// =============================================================================
`ifdef SIM_ON
	assign clk_pll = clk;
`else
	pll_50M_1M	pll_inst (
		.areset ( !rst ),
		.inclk0 ( clk ),
		.c0 ( clk_pll )
	);
`endif

`ifdef SIM_ON
	assign clk_gen = clk_pll;
`else
	clk_div #(
			.FREQUENCY      (FREQUENCY_CLKDIV)
			,.REFERENCE_CLK  (REFERENCE_CLKPLL)

	) clk_divisor (
		.clk    (clk_pll),
		.rst    (rst),
		.clk_gen(clk_gen)
	);
`endif	

// =============================================================================
// Debouncer
// =============================================================================

`ifdef SIM_ON
	assign dbcr_start = !start;
`else
fsm_dbcr i_fsm_dbcr (
    .clk            (clk_gen),
	.rst_n          (rst),
    .Din            (!start),
    .Delay30ms_ready(Delay30ms_ready),
    .EnableCounter  (EnableCounter),
    .one_shot       (dbcr_start)
);


cntr_mod_n_ovf #(
`ifdef SIM_ON
	.FREQ  (REFERENCE_CLKTB),
	.DLY(0.5)
`else 
	.FREQ(FREQUENCY_CLKDIV),
	.DLY(0.003)
`endif
) i_cntr_mod_n (
    .clk    ( clk_gen           ),
    .rst    ( rst             	),
    .enb    ( EnableCounter     ),
    .ovf    ( Delay30ms_ready   ),
    .count  ()
);
`endif	


// =============================================================================
// Maquina de estados
// =============================================================================

fsm_moore#(
	.DW_TIMES(IN_DW)
) state_Mult(
    .En(dbcr_start),
    .Rst(rst),
    .Clk(clk_gen),
    .ovf(ovf),
    .pipo_en(pipo_en),
    .done_en(done_en),
    .load_en(load_en),
    .ready_en(ready_en)
);

bin_counter_ovf #(
.DW(WIDTH_CNT), 
.MAXCNT(IN_DW)
) cntr (
	.clk(clk_gen),
	.rst(rst),
	.enb(!load_en),
	.ovf(ovf),
	.count()
);

// =============================================================================
// PIPO Entradas
// =============================================================================

pipo_Nenb #(
.DW(IN_DW)
) pipo_multiplicand(
	.clk(clk_gen),
	.rst(rst),
	.inp(multiplicand),
	.out(multiplicand_wr)
);

pipo_Nenb #(
.DW(IN_DW)
) pipo_multiplier(
	.clk(clk_gen),
	.rst(rst),
	.inp(multiplier),
	.out(multiplier_wr)
);

// =============================================================================
// C2 to Binary
// =============================================================================

C22Bin #(
.DW(IN_DW)
) conv_multiplicand(
	.complemento2(multiplicand_wr),
	.done_en     (done_en),
	.binario     (multiplicand_bin),
	.signo(multiplicand_sign)
);

C22Bin #(
.DW(IN_DW)
) conv_multiplier(
	.complemento2(multiplier_wr),
	.done_en     (done_en),
	.binario     (multiplier_bin),
	.signo(multiplier_sign)
);

assign sign = rst? (multiplier_sign ^ multiplicand_sign):'0 ;

// =============================================================================
// PIPO Shifter | ADDER
// =============================================================================

multSec #(
.DW(IN_DW)
) shift_adder(
	.clk(clk_gen),
	.rst(rst),
	.pipo_en(pipo_en),
	.done_en(done_en),
	.multiplier(multiplier_bin),
	.multiplicand(multiplicand_bin),
	.out(product_wr_next)
);

// =============================================================================
// Binary to C2 
// =============================================================================

Bin2C2 #(
.DW(IN_DW*2)
) conv_product(
	.signo			(sign),
	.binario     	(product_wr_next),
	.complemento2	(product_bin)
);

// =============================================================================
// PIPO Salidas
// =============================================================================

pipo_Nenb #(
.DW(IN_DW*2)
) pipo_product(
	.clk(clk_gen),
	.rst(rst),
	.inp(product_bin),
	.out(product)
);

pipo_Nenb #(
.DW(1)
) pipo_ready(
	.clk(clk_gen),
	.rst(rst),
	.inp( ready_en ),
	.out(ready)
);

endmodule