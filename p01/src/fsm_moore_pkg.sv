// =============================================================================
// Title        :   Package
// Project      :   t03
// File         :   fsm_moore_pkg.sv
// Date         :   01/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

`ifndef FSM_MOORE_PKG_SV
    `define FSM_MOORE_PKG_SV
package fsm_moore_pkg;

    localparam W_IN = 5;
    localparam W_ST = 1;
    localparam W_VAL = 1;
    localparam MAX_TIMES = 1;
    typedef logic [3:0]         times_t;
    typedef logic [W_IN-1:0]    data_t;
    typedef logic [W_ST-1:0]    state_t;
    typedef logic [W_VAL-1:0]   value_t; 
    typedef enum state_t{ 
        STATE_SHIFT     = 1'b0, //genera los shift
        IDLE            = 1'b1
    } state_e;

    typedef enum value_t{
        ON  = 1'b1,
        OFF   = 1'b0
    } value_e;

endpackage
`endif
 
