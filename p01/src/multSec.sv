// =============================================================================
// Title        : Multiplicador Secuencial
// Inputs       : System clk, reset, pipo_en, donde_en, enable,
//					multiplier and multiplicand
// Outputs      : out
// Project      :   p01
// File         :   multSec.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module multSec #(
parameter DW = 4
) (
	input               clk,
	input               rst,
	input               pipo_en,
	input               done_en,
	input  [DW-1:0]	multiplier,
	input  [DW-1:0]	multiplicand,
	output [(DW*2)-1:0]     out
);


logic [DW-1:0]		sum;
logic [(DW*2)-1:0]	n_product;
logic [(DW*2)-1:0]	out_wr;
logic [(DW*2)-1:0]	out_next;

pipo_shift #(
.DW(DW*2)
) shift_adder(
	.clk(clk),
	.rst(rst),
	.enb(pipo_en),
	.sum(sum),
	.inp(n_product),
	.multiplier(multiplier),
	.out(out_next)
);

assign n_product = done_en?{4'b0,multiplier}:out;
assign sum = done_en?'0:(out[(DW*2)-1:DW] + multiplicand);

assign out_wr = out;
assign out = done_en?out_wr:out_next;


endmodule

