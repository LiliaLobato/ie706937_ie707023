// =============================================================================
// Title        : C22Bin
//                  The C2 is transformed to a binary value 
// Inputs       : done_en, complemento2
// Outputs      : binario, signo
// Project      :   p01
// File         :   C22Bin.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module C22Bin #(
parameter DW = 4
) (
input  [DW-1:0] complemento2,
input 			done_en,
output [DW-1:0]	binario,
output 			signo
);

logic signo_wr;

assign signo_wr = signo;
assign signo = done_en?signo_wr:complemento2[DW-1];
assign binario = complemento2[DW-1]?((~complemento2)+1):complemento2;


endmodule

