onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mult/uut/clk_gen
add wave -noupdate /tb_mult/uut/start
add wave -noupdate /tb_mult/uut/dbcr_start
add wave -noupdate /tb_mult/uut/enable
add wave -noupdate -divider TOP
add wave -noupdate -radix binary -childformat {{{/tb_mult/uut/multiplier[3]} -radix binary} {{/tb_mult/uut/multiplier[2]} -radix binary} {{/tb_mult/uut/multiplier[1]} -radix binary} {{/tb_mult/uut/multiplier[0]} -radix binary}} -subitemconfig {{/tb_mult/uut/multiplier[3]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[2]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[1]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[0]} {-height 15 -radix binary}} /tb_mult/uut/multiplier
add wave -noupdate -color Violet -radix decimal -childformat {{{/tb_mult/multiplier[3]} -radix decimal} {{/tb_mult/multiplier[2]} -radix decimal} {{/tb_mult/multiplier[1]} -radix decimal} {{/tb_mult/multiplier[0]} -radix decimal}} -subitemconfig {{/tb_mult/multiplier[3]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[2]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[1]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[0]} {-color Violet -height 15 -radix decimal}} /tb_mult/multiplier
add wave -noupdate -radix binary /tb_mult/uut/multiplicand
add wave -noupdate -color Violet -radix decimal /tb_mult/multiplicand
add wave -noupdate -radix binary /tb_mult/uut/product
add wave -noupdate -color Violet -radix decimal /tb_mult/uut/product
add wave -noupdate /tb_mult/uut/sign
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {49162598 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 242
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {51491564 ps} {51753076 ps}
