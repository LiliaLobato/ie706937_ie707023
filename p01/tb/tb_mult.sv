// =============================================================================
// Title        :   Implementacion de un fsmb para generar blinking
// Inputs       :   System clk, reset and start
// Outputs      :   clock generated and blink status
// Project      :   t04
// File         :   blink_top.sv
// Date         :   12/02/2020



`timescale 1ns / 1ps
module tb_mult();
import clk_div_pkg::*;
import dbcr_pkg::*;
import mult_top_pkg::*;

logic    clk;
logic    rst;
logic    start;
int_t    multiplier;
int_t    multiplicand;
logic    ready;
out_t    product; 

mult_top uut(
    .clk         (clk),
    .rst         (rst),
    .start       (start),
    .multiplier  (multiplier),
    .multiplicand(multiplicand),
    .ready       (ready),
    .product     (product)
);

initial begin
    clk = 0;
end

always begin
        #2  clk = ~clk;
end

initial begin 
    rst = 0;    #20
    rst = 1;
    start = 1;  #20

        multiplier   = 0;
        multiplicand = 0;

    repeat (2**IN_DW) begin
    //repeat (1) begin
        multiplier = multiplier+1;
        $display("------Tabla del %d------",$signed(multiplier));
        repeat (2**IN_DW) begin
        //repeat (1) begin
            multiplicand = multiplicand+1; #50

            start = 0;  #50
            start = 1; 

            #1000

            $display("%d * %d = %d",$signed(multiplier),$signed(multiplicand),$signed(product));
            if (($signed(multiplier)*$signed(multiplicand) != $signed(product)) ) begin
                $display("MISMATCH AT %d * %d = %d | FOUND = %d",$signed(multiplier),$signed(multiplicand),$signed(multiplicand*multiplier),$signed(product));
            end
        end
    end     
    
    #500
    $stop;
end

endmodule
