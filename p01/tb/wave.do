onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mult/uut/clk_gen
add wave -noupdate /tb_mult/uut/start
add wave -noupdate /tb_mult/uut/dbcr_start
add wave -noupdate /tb_mult/uut/enable
add wave -noupdate -divider TOP
add wave -noupdate -radix binary -childformat {{{/tb_mult/uut/multiplier[8]} -radix binary} {{/tb_mult/uut/multiplier[7]} -radix binary} {{/tb_mult/uut/multiplier[6]} -radix binary} {{/tb_mult/uut/multiplier[5]} -radix binary} {{/tb_mult/uut/multiplier[4]} -radix binary} {{/tb_mult/uut/multiplier[3]} -radix binary} {{/tb_mult/uut/multiplier[2]} -radix binary} {{/tb_mult/uut/multiplier[1]} -radix binary} {{/tb_mult/uut/multiplier[0]} -radix binary}} -subitemconfig {{/tb_mult/uut/multiplier[8]} {-radix binary} {/tb_mult/uut/multiplier[7]} {-radix binary} {/tb_mult/uut/multiplier[6]} {-radix binary} {/tb_mult/uut/multiplier[5]} {-radix binary} {/tb_mult/uut/multiplier[4]} {-radix binary} {/tb_mult/uut/multiplier[3]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[2]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[1]} {-height 15 -radix binary} {/tb_mult/uut/multiplier[0]} {-height 15 -radix binary}} /tb_mult/uut/multiplier
add wave -noupdate -color Violet -radix decimal -childformat {{{/tb_mult/multiplier[8]} -radix decimal} {{/tb_mult/multiplier[7]} -radix decimal} {{/tb_mult/multiplier[6]} -radix decimal} {{/tb_mult/multiplier[5]} -radix decimal} {{/tb_mult/multiplier[4]} -radix decimal} {{/tb_mult/multiplier[3]} -radix decimal} {{/tb_mult/multiplier[2]} -radix decimal} {{/tb_mult/multiplier[1]} -radix decimal} {{/tb_mult/multiplier[0]} -radix decimal}} -subitemconfig {{/tb_mult/multiplier[8]} {-color Violet -radix decimal} {/tb_mult/multiplier[7]} {-color Violet -radix decimal} {/tb_mult/multiplier[6]} {-color Violet -radix decimal} {/tb_mult/multiplier[5]} {-color Violet -radix decimal} {/tb_mult/multiplier[4]} {-color Violet -radix decimal} {/tb_mult/multiplier[3]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[2]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[1]} {-color Violet -height 15 -radix decimal} {/tb_mult/multiplier[0]} {-color Violet -height 15 -radix decimal}} /tb_mult/multiplier
add wave -noupdate -radix binary /tb_mult/uut/multiplicand
add wave -noupdate -color Violet -radix decimal /tb_mult/multiplicand
add wave -noupdate -radix binary /tb_mult/uut/product
add wave -noupdate -color Violet -radix decimal /tb_mult/uut/product
add wave -noupdate -color Yellow /tb_mult/uut/done_en
add wave -noupdate -color Yellow /tb_mult/uut/ready
add wave -noupdate -divider multiplication
add wave -noupdate /tb_mult/uut/cntr/DW
add wave -noupdate /tb_mult/uut/cntr/MAXCNT
add wave -noupdate /tb_mult/uut/cntr/enb
add wave -noupdate /tb_mult/uut/cntr/ovf
add wave -noupdate /tb_mult/uut/cntr/count
add wave -noupdate /tb_mult/uut/sign
add wave -noupdate /tb_mult/uut/state_Mult/pipo_en
add wave -noupdate /tb_mult/uut/load_en
add wave -noupdate -divider state_machine
add wave -noupdate /tb_mult/uut/state_Mult/En
add wave -noupdate /tb_mult/uut/state_Mult/pipo_en
add wave -noupdate /tb_mult/uut/state_Mult/done_en
add wave -noupdate /tb_mult/uut/state_Mult/load_en
add wave -noupdate /tb_mult/uut/state_Mult/edo_siguiente
add wave -noupdate /tb_mult/uut/state_Mult/Edo_Actual
add wave -noupdate -divider {PIPO ADDER}
add wave -noupdate -color {Cornflower Blue} /tb_mult/uut/shift_adder/multiplier
add wave -noupdate -color {Cornflower Blue} /tb_mult/uut/shift_adder/multiplicand
add wave -noupdate -color {Cornflower Blue} /tb_mult/uut/shift_adder/sum
add wave -noupdate -radix binary -childformat {{{/tb_mult/uut/shift_adder/out[17]} -radix binary} {{/tb_mult/uut/shift_adder/out[16]} -radix binary} {{/tb_mult/uut/shift_adder/out[15]} -radix binary} {{/tb_mult/uut/shift_adder/out[14]} -radix binary} {{/tb_mult/uut/shift_adder/out[13]} -radix binary} {{/tb_mult/uut/shift_adder/out[12]} -radix binary} {{/tb_mult/uut/shift_adder/out[11]} -radix binary} {{/tb_mult/uut/shift_adder/out[10]} -radix binary} {{/tb_mult/uut/shift_adder/out[9]} -radix binary} {{/tb_mult/uut/shift_adder/out[8]} -radix binary} {{/tb_mult/uut/shift_adder/out[7]} -radix binary} {{/tb_mult/uut/shift_adder/out[6]} -radix binary} {{/tb_mult/uut/shift_adder/out[5]} -radix binary} {{/tb_mult/uut/shift_adder/out[4]} -radix binary} {{/tb_mult/uut/shift_adder/out[3]} -radix binary} {{/tb_mult/uut/shift_adder/out[2]} -radix binary} {{/tb_mult/uut/shift_adder/out[1]} -radix binary} {{/tb_mult/uut/shift_adder/out[0]} -radix binary}} -subitemconfig {{/tb_mult/uut/shift_adder/out[17]} {-radix binary} {/tb_mult/uut/shift_adder/out[16]} {-radix binary} {/tb_mult/uut/shift_adder/out[15]} {-radix binary} {/tb_mult/uut/shift_adder/out[14]} {-radix binary} {/tb_mult/uut/shift_adder/out[13]} {-radix binary} {/tb_mult/uut/shift_adder/out[12]} {-radix binary} {/tb_mult/uut/shift_adder/out[11]} {-radix binary} {/tb_mult/uut/shift_adder/out[10]} {-radix binary} {/tb_mult/uut/shift_adder/out[9]} {-radix binary} {/tb_mult/uut/shift_adder/out[8]} {-radix binary} {/tb_mult/uut/shift_adder/out[7]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[6]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[5]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[4]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[3]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[2]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[1]} {-height 15 -radix binary} {/tb_mult/uut/shift_adder/out[0]} {-height 15 -radix binary}} /tb_mult/uut/shift_adder/out
add wave -noupdate -color Violet -radix decimal -childformat {{{/tb_mult/uut/shift_adder/out[17]} -radix decimal} {{/tb_mult/uut/shift_adder/out[16]} -radix decimal} {{/tb_mult/uut/shift_adder/out[15]} -radix decimal} {{/tb_mult/uut/shift_adder/out[14]} -radix decimal} {{/tb_mult/uut/shift_adder/out[13]} -radix decimal} {{/tb_mult/uut/shift_adder/out[12]} -radix decimal} {{/tb_mult/uut/shift_adder/out[11]} -radix decimal} {{/tb_mult/uut/shift_adder/out[10]} -radix decimal} {{/tb_mult/uut/shift_adder/out[9]} -radix decimal} {{/tb_mult/uut/shift_adder/out[8]} -radix decimal} {{/tb_mult/uut/shift_adder/out[7]} -radix decimal} {{/tb_mult/uut/shift_adder/out[6]} -radix decimal} {{/tb_mult/uut/shift_adder/out[5]} -radix decimal} {{/tb_mult/uut/shift_adder/out[4]} -radix decimal} {{/tb_mult/uut/shift_adder/out[3]} -radix decimal} {{/tb_mult/uut/shift_adder/out[2]} -radix decimal} {{/tb_mult/uut/shift_adder/out[1]} -radix decimal} {{/tb_mult/uut/shift_adder/out[0]} -radix decimal}} -subitemconfig {{/tb_mult/uut/shift_adder/out[17]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[16]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[15]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[14]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[13]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[12]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[11]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[10]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[9]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[8]} {-color Violet -radix decimal} {/tb_mult/uut/shift_adder/out[7]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[6]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[5]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[4]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[3]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[2]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[1]} {-color Violet -height 15 -radix decimal} {/tb_mult/uut/shift_adder/out[0]} {-color Violet -height 15 -radix decimal}} /tb_mult/uut/shift_adder/out
add wave -noupdate /tb_mult/uut/shift_adder/n_product
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {138000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 242
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {261512 ps}
