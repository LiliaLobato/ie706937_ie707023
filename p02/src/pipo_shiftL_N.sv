/// =============================================================================
// Title        : Pipo Shift
//                  The parallel input will be the parallel output shifted
//                  N times to left
// Project      :   p02
// File         :   pipo_shiftL_N.sv
// Date         :   01/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module pipo_shiftL_N #(
parameter DW = 4,
parameter N = 1
) (
input               clk,
input               rst,
input               enb,
input  signed [DW:0]     inp,
output [DW:0]            out
);

logic  signed [DW:0]      rgstr_r;

always_ff@(posedge clk, negedge rst)begin
   if (!rst ) begin
        rgstr_r <=  '0 ;
   end else begin
   		if (enb) begin 
   			rgstr_r <= inp<<N;
   		end else 
        rgstr_r <= inp;
    end 
end

assign out  = rgstr_r;

endmodule

