// =============================================================================
// Title       	:	Implementacion de un multiplicador secuencial
//					de dos numeros C2
// Inputs		:	System clk, reset, start, load, data, and op
// Outputs		:	signo, load_x, load_y, error, result, remainder and N 7segments
// Project     	: 	p02
// File        	: 	MA_top.sv
// Date 		: 	01/04/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================

import fsm_moore_pkg::*;
import clk_div_pkg::*;
import mult_top_pkg::*;
import bin_BCD_pkg::*;

module MA_top(
	input  logic	clk,
	input  logic	rst,
	input  logic	start,
	input  logic	load,
	input  int_t	data,
	input  op_t		op,
	output logic	ready,
	output logic	signo,
	output logic	load_x,
	output logic	load_y,
	output logic	error,
	output int_t	result,
	output int_t	remainder, 
	output n_segment_t n_seg
);

//Clk_gen
logic clk_gen;
logic clk_pll;

//Debouncer
logic dbcr_start;
logic dbcr_load;

//logic control signals
logic y_en;
logic x_en;
logic op_en;
logic pipo_en;
logic done_en;
logic load_en;
logic ready_en;

//error detection
logic error_en;

int_t 	r_sqrt;
int_t	data_wr;
int_t	data_wr_reg;
int_t	data_y;
int_t	data_x;
int_t	data_y_bin;
int_t	data_x_bin;
logic 	x_sign;
logic 	y_sign;
op_t	data_op_wr;
op_t	op_wr;
op_e	data_op;
int_t	multiplicand_wr;
int_t	multiplicand_wr_next;
int_t	multiplier_wr;
int_t	multiplier_wr_next;
int_t	multiplicand_bin;
int_t	multiplier_bin;
logic 	sign;
logic 	sign_wr;
logic 	sign_rem;
logic	sign_mult;
logic 	ovf;
logic 	ovf_mult;
logic 	ovf_div;
logic	ovf_sqrt;

logic [WIDTH_CNT-1:0] i_cntr_sqrt;
logic   sqrt_sel_wire;
logic 	multiplier_sign;
logic 	multiplicand_sign;
mult_t 	product_wr_next;
out_t 	result_bin;
out_t 	result_c2;
int_t 	result_wr;
int_t 	result_7seg;
out_t 	remainder_bin;
out_t 	remainder_c2;
int_t 	remainder_wr;

// =============================================================================
// Clk generator for 5MHz 
// =============================================================================
`ifdef SIM_ON_TB
	assign clk_gen = clk;
`else
	clk_gen clk_gen_50M_5M (
		.clk    (clk),
		.rst    (rst),
		.clk_out(clk_gen)
	);

`endif

// =============================================================================
// Debouncer
// =============================================================================
`ifdef SIM_ON_TB
	pipo_Nenb pipo_start(
		.clk(clk_gen),
		.rst(rst),
		.inp(!start),
		.out(dbcr_start)
	);

	pipo_Nenb pipo_load(
		.clk(clk_gen),
		.rst(rst),
		.inp(!load),
		.out(dbcr_load)
	);
`else
	oneShot start_dbcr (
	.clk       	(clk_gen),
	.rst       	(rst),
	.in 		(start),
	.out 		(dbcr_start)
	);

	oneShot load_dbcr (
	.clk       	(clk_gen),
	.rst       	(rst),
	.in 		(load),
	.out 		(dbcr_load)
	);

`endif

// =============================================================================
// Maquina de estados
// =============================================================================

fsm_moore state_Mult(
    .En(dbcr_start),
    .Rst(rst),
    .Clk(clk_gen),
    .ovf(ovf),
    .load(dbcr_load),
    .op(data_op),
    .y_en(y_en),
    .x_en(x_en),
    .op_en(op_en),
    .pipo_en(pipo_en),
    .done_en(done_en),
    .load_en(load_en),
    .ready_en(ready_en)
);

bin_counter_ovf #(
.DW(WIDTH_CNT), 
.MAXCNT(IN_DW)
) cntr (
	.clk(clk_gen),
	.rst(rst),
	.enb(!load_en),
	.ovf(ovf),
	.count(),
	.count_2(i_cntr_sqrt)
);

// =============================================================================
// PIPO Entradas
// =============================================================================


pipo_Nenb #(
.DW(IN_DW)
) pipo_data(
	.clk(clk_gen),
	.rst(rst),
	.inp(data), 
	.out(data_wr_reg)
);

pipo_Nenb #(
.DW(OP)
) pipo_op(
	.clk(clk_gen),
	.rst(rst),
	.inp(op), 
	.out(op_wr)
);

// =============================================================================
// Selectors
// =============================================================================

pipo #(
.DW(IN_DW)
) dataX (
	.clk(clk_gen),
	.rst(rst),
	.enb(x_en),
	.inp(data_wr_reg),
	.out(data_x)
);

pipo #(
.DW(IN_DW)
) dataY (
	.clk(clk_gen),
	.rst(rst),
	.enb(y_en),
	.inp(data_wr_reg),
	.out(data_y)
);

pipo #(
.DW(OP)
) operation (
	.clk(clk_gen),
	.rst(rst),
	.enb(op_en),
	.inp(op_wr),
	.out(data_op_wr)
);

assign data_op = op_e'(data_op_wr);

// =============================================================================
// C2 to Binary
// =============================================================================

C22Bin #(
.DW(IN_DW)
) conv_y(
	.complemento2(data_y),
	.done_en     (done_en),
	.binario     (data_y_bin),
	.signo		 (y_sign)
);

C22Bin #(
.DW(IN_DW)
) conv_x(
	.complemento2(data_x),
	.done_en     (done_en),
	.binario     (data_x_bin),
	.signo		 (x_sign)
);

sign_det sign_detection(
	.y_sign  (y_sign),
	.x_sign  (x_sign),
	.rst     (rst),
	.sign    (sign),
	.sign_rem(sign_rem),
	.data_op (data_op),
	.sign_mult(sign_mult)
);

// =============================================================================
// PIPO Shifter | ADDER
// =============================================================================

MDR #(
.DW(IN_DW)
) MDR_inst (
	.clk(clk_gen),
	.rst(rst),
	.pipo_en(pipo_en),
	.done_en(done_en),
	.load_en(load_en),
	.r_sqrt   (r_sqrt),
	.i_count(i_cntr_sqrt),
	.x_bin(data_x_bin),
	.y_bin(data_y_bin),
	.out(product_wr_next),
	.sign_mult(sign_mult),
	.op(data_op)
);

// =============================================================================
// Binary to C2 
// =============================================================================

result_det result_detection(
	.product_wr_next	(product_wr_next),
	.data_op 			(data_op),
	.result_bin 		(result_bin),
	.remainder_bin 		(remainder_bin),
	.r_sqrt 			(r_sqrt)
);

Bin2C2 #(
.DW(IN_DW*2)
) conv_result(
	.signo			(sign),
	.binario     	(result_bin),
	.complemento2	(result_c2)
);

Bin2C2 #(
.DW(IN_DW*2)
) conv_remainder(
	.signo			(sign_rem),
	.binario     	(remainder_bin),
	.complemento2	(remainder_c2)
);

// =============================================================================
// ERROR Detection
// =============================================================================

error_det error_detection (
	.data_y_bin  (data_y_bin),
	.result_c2   (result_c2),
	.remainder_c2(remainder_c2),
	.data_op     (data_op),
	.result_wr   (result_wr),
	.x_sign		 (x_sign),
	.remainder_wr(remainder_wr),
	.error       (error_en)
);

// =============================================================================
// PIPO Salidas
// =============================================================================

pipo_Nenb #(
.DW(IN_DW)
) pipo_result(
	.clk(clk_gen),
	.rst(rst),
	.inp(ready_en?result_wr:'0), 
	.out(result)
);

pipo_Nenb #(
.DW(IN_DW)
) pipo_remainder(
	.clk(clk_gen),
	.rst(rst),
	.inp(ready_en?remainder_wr:'0), 
	.out(remainder)
);

pipo_Nenb pipo_ready(
	.clk(clk_gen),
	.rst(rst),
	.inp(ready_en), 
	.out(ready)
);

pipo_Nenb pipo_loadX(
	.clk(clk_gen),
	.rst(rst),
	.inp(x_en), 
	.out(load_x)
);

pipo_Nenb pipo_loadY(
	.clk(clk_gen),
	.rst(rst),
	.inp(y_en), 
	.out(load_y)
);

pipo_Nenb pipo_error(
	.clk(clk_gen),
	.rst(rst),
	.inp(error_en), 
	.out(error)
);

// =============================================================================
// 7 Segmentos
// =============================================================================

bin_BCD_top Salida7Seg (
	.BinIn(result),
	.n_seg(n_seg),
	.signo(signo)
);


endmodule