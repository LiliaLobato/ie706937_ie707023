// =============================================================================
// Title        :   Package
// Project      :   p02
// File         :   mult_top_pkg.sv
// Date         :   01/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`ifndef MULT_TOP_PKG_SV
    `define MULT_TOP_PKG_SV
package mult_top_pkg;

`ifdef SIM_ON
    localparam IN_DW = 4;
`else
    localparam IN_DW = 16;
`endif

    localparam WIDTH_CNT = $clog2(IN_DW*2)+1;

    typedef logic [IN_DW-1:0]       int_t;
    typedef logic [(IN_DW*2)-1:0]   out_t;
    typedef logic [IN_DW*2:0]       mult_t;

    localparam UPPER_LIMIT = {{IN_DW+1{1'b1}},{IN_DW-1{1'b0}}}; 
    localparam LOWER_LIMIT = {{IN_DW+1{1'b0}},{IN_DW-1{1'b1}}};


    localparam OP = 2;
    typedef logic [OP-1:0]	op_t; 
    typedef enum op_t{ 
        MULTIPLICATION    = 2'b00, //Load Y
        DIVITION          = 2'b01, //Load X
        SQUARE            = 2'b10 //genera los shift
    } op_e;
    
endpackage
`endif
 
