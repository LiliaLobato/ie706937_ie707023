// =============================================================================
// Title        : Adder N
//                  The adder checks if the actual significant figure is higher
//                  than 4 and add 3
// Parameter    : CNT (significant figure number)
// Inputs       : Parallel inp
// Outputs      : Parallel output
// Project      :   t06
// File         :   adderN.sv
// Date         :   08/03/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module adderN 
import bin_BCD_pkg::*;#
( 
	parameter CNT = 1
)(
input  dataDD_t     inp,
output dataDD_t     out
);
	
	//revisa si el rango correspondiente a la cifra significativa es mayor a 4 si lo es, le suma 3
	assign out = (inp [(BIT_IN_H*(CNT+1) + BIT_IN)-1:(BIT_IN_H*CNT + BIT_IN)] > 4) ? (inp + {3'b11 , {(BIT_IN+(BIT_IN_H*(CNT))){1'b0}}}):inp;

endmodule
