//`timescale 1ns / 1ps
// =============================================================================
// Title        : Fsmb de dos estados:
//                  -> Idle
//                  -> Load X
//                  -> Load Y
//                  -> SHIFT
//                Genera una secuencia de DW shifts  
// Project      :   p02
// File         :   fsm_moore.sv
// Date         :   01/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import mult_top_pkg::*;

module fsm_moore
import fsm_moore_pkg::*;
(
    input               En,
    input               Rst,
    input               Clk,
    input               ovf,
    input               load,
    input  op_e         op,
    output value_t      y_en,
    output value_t      x_en,
    output value_t      op_en,
    output value_t      pipo_en,
    output value_t      done_en,
    output value_t      load_en,
    output value_t      ready_en
);

state_e Edo_Actual;	
state_e edo_siguiente;


// Circuito combinacional entrada, define ssiguiente estado
always_comb begin: cto_comb_entrada
   case(Edo_Actual)
      STATE_LOAD_Y:begin 
        if ( !load ) begin
          edo_siguiente=STATE_LOAD_Y;
        end else begin
          edo_siguiente=STATE_SHIFT;
        end 
      end
      STATE_LOAD_X:begin 
        if ( !load ) begin
          edo_siguiente=STATE_LOAD_X;
        end else begin
          if ( op != SQUARE ) begin          
            edo_siguiente=STATE_LOAD_Y;
          end else begin
            edo_siguiente=STATE_SHIFT;
          end
        end 
      end

      STATE_SHIFT:begin 
      	if ( ovf ) begin
          edo_siguiente=IDLE;
      	end else begin
          edo_siguiente=STATE_SHIFT;
        end 
      end
      IDLE:begin 
        if ( En ) begin
          edo_siguiente=STATE_LOAD_X;
        end else begin
          edo_siguiente=IDLE;
        end
      end
   endcase
end

//asignaciones por default y siguiente
always_ff@(posedge Clk or negedge Rst) begin // Circuito Secuenicial en un proceso always.
   if (!Rst) 
      Edo_Actual  <= IDLE;
   else 
      Edo_Actual  <= edo_siguiente;
end

// CTO combinacional de salida.
always_comb begin
case(Edo_Actual)
    IDLE : begin
        y_en      = OFF;
        x_en      = OFF;
        op_en     = OFF;
        done_en   = ON;
        load_en   = ON;
        pipo_en   = OFF;
    end
    STATE_SHIFT: begin
        y_en      = OFF;
        x_en      = OFF;
        op_en     = OFF;
        done_en   = OFF;
        load_en   = ovf?ON:OFF;
        pipo_en   = ON;
    end
    STATE_LOAD_X: begin
        y_en      = OFF;
        x_en      = ON;
        op_en     = ON;
        done_en   = OFF;
        load_en   = ON;
        pipo_en   = OFF;
    end
    STATE_LOAD_Y: begin
        y_en      = ON;
        x_en      = OFF;
        op_en     = OFF;
        done_en   = OFF;
        load_en   = ON;
        pipo_en   = OFF;
    end
endcase 
    ready_en = (done_en & load_en);
end

endmodule