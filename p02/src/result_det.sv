// =============================================================================
// Title       	:	Implementacion de un detector de resultado 
//					para las 3 operaciones
// Project     	: 	p02
// File        	: 	result_det.sv
// Date 		: 	01/04/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module result_det
import mult_top_pkg::*;
(
	input  mult_t	product_wr_next,
	input  int_t	r_sqrt,
	input  op_e		data_op,
	output out_t 	result_bin,
	output out_t 	remainder_bin
);

always_comb begin
	case(data_op)
	    MULTIPLICATION: begin
	        result_bin 		= product_wr_next[(IN_DW*2):1];
	        remainder_bin 	= '0;
	    end
	    DIVITION : begin
	        result_bin 		= { {IN_DW{1'b0}},product_wr_next[IN_DW:1] };
	        remainder_bin 	= { {IN_DW+1{1'b0}},product_wr_next[(IN_DW*2):IN_DW+2] };
	    end
	    SQUARE : begin
			result_bin 		= product_wr_next[(IN_DW*2):1];
	        remainder_bin 	= r_sqrt[IN_DW-1]? r_sqrt + ((product_wr_next[(IN_DW*2):1] << 1) | 1) : r_sqrt;
	    end
    	default: begin
			result_bin 		= '0; 
	        remainder_bin 	= '0;
    	end
	endcase 
end

endmodule