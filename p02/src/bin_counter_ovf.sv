
// Coder:       Abisai Ramirez Perez
// Date:        January 28th, 2020
// Name:        bin_counter_ovf.sv
// Description: This is a binary counter with overflow and counter reset

module bin_counter_ovf #(
parameter 
DW      = 4, 
MAXCNT  = 5
)(
input           clk,
input           rst,
input           enb,
output logic    ovf,
output [DW-1:0] count,
output [DW-1:0] count_2
);

logic [DW-1:0] count_r, count_nxt, count_rst;

always_ff@(posedge clk, negedge rst)begin: counter
    if (!rst)
        count_r     <=  '0          ;
    else if (!enb)
        count_r     <=  '0          ;
    else if (enb)
        count_r     <= count_nxt    ;
end:counter

// Combinational modules: adder and comparator
always_comb begin: comparator
   count_nxt = count_r + 1'b1 ;
    if (count_r == MAXCNT-1'b1) begin
        ovf       = 1'b1;
        count_nxt = 1'b0;
    end else
        ovf     =   1'b0;
        count_nxt =  count_nxt;
end

assign count_2 = (MAXCNT - (count_r + 1'b1)) + (MAXCNT - (count_r + 1'b1));
assign count    =   count_r;

endmodule

