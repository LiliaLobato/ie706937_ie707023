// =============================================================================
// Title       	:	Implementacion de un detetcor de errores
// Project     	: 	p02
// File        	: 	error_det.sv
// Date 		: 	01/04/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module error_det
import mult_top_pkg::*;
(
	input  int_t	data_y_bin,
	input  out_t	result_c2,
	input  out_t	remainder_c2,
	input  op_e		data_op,
	input  logic 	x_sign,
	output int_t 	result_wr,
	output int_t 	remainder_wr,
	output logic	error
);

logic error_en;

always_comb begin
	case(data_op)
	    MULTIPLICATION: begin
	        error_en = ( result_c2 > LOWER_LIMIT & result_c2 < UPPER_LIMIT)?1'b1:1'b0; 
	    end
	    DIVITION : begin
	        error_en = (data_y_bin==0)?1'b1:1'b0;
	    end
	    SQUARE : begin
	        error_en = (x_sign!=0)?1'b1:1'b0;
	    end
    	default: begin
	        error_en = 1'b0;
    	end
	endcase 
end 

assign error = error_en;
assign result_wr 	= error_en?{IN_DW{1'b1}}:result_c2[IN_DW-1:0];
assign remainder_wr = error_en?{IN_DW{1'b1}}:remainder_c2[IN_DW-1:0];

endmodule