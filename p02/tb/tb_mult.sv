// =============================================================================
// Title        :   Implementacion de un pseudo checker
// Project      :   p02
// File         :   tb_mult.sv
// Date         :   01/04/2020



`timescale 1ns / 1ps
module tb_mult();
import clk_div_pkg::*;
import dbcr_pkg::*;
import mult_top_pkg::*;
import bin_BCD_pkg::*;


logic    clk;
logic    rst;
logic    start;
logic    load;
int_t    data;
op_e     op;
logic    ready;
logic    load_x;
logic    load_y;
logic    error;
int_t    result;
int_t    remainder;
n_segment_t n_seg;
logic    signo;

int_t   dataX;
int_t    dataY;

MA_top uut(
    .clk         (clk),
    .rst         (rst),
    .start       (start),
    .load        (load),
    .data        (data),
    .op          (op),
    .ready       (ready),
    .load_x      (load_x),
    .load_y      (load_y),
    .error       (error),
    .result      (result),
    .remainder   (remainder),
    .signo       (signo),
    .n_seg       (n_seg)
);

initial begin
    clk = 0;
    start = 1;
    rst = 1;
    load = 1;
end

always begin
        #2  clk = ~clk;
end

initial begin 
    rst = 0;    #20
    rst = 1;

    //op = MULTIPLICATION;
    //op = DIVITION;
    op = SQUARE;
    //op = op_e'(3);

    start = 0;  #50
    start = 1;  #10

    data = -8; 

    load = 0;  #20
    load = 1;

    if(SQUARE != op) begin
        data = 6; #50

        load = 0;  #20
        load = 1;
    end

#1000

    rst = 0;    #20
    rst = 1;

#1000


// =============================================================================
// MULTIPLICATION
// =============================================================================

    op = MULTIPLICATION;
    dataY = 0;
    dataX = 0;

    repeat (2**IN_DW) begin
        dataY = dataY+1;
        $display("------Tabla del %d------",$signed(dataY));
        repeat (2**IN_DW) begin

            start = 0;  #50
            start = 1;  #10
            
            dataX = dataX+1;
            data = dataY;
            
            load = 0;  #20
            load = 1;   
            
            data = dataX; #50

            load = 0;  #20
            load = 1;   

            #1000

            $display("%d * %d = %d | ERROR = %d",$signed(dataY),$signed(dataX),$signed(result),error);
            if (($signed(dataY)*$signed(dataX) != $signed(result)) ) begin
                if ( error && (-1 != $signed(result)) ) begin
                    $display("OVERFLOW AT %d * %d | FOUND = %d",$signed(dataY),$signed(dataX),$signed(result));
                end else if ( !error) begin
                    $display("MISMATCH AT %d * %d = %d | FOUND = %d",$signed(dataY),$signed(dataX),$signed(dataY*dataX),$signed(result));
                end
            end
        end
    end   
#1000


// =============================================================================
// DIVITION
// =============================================================================

    op = DIVITION;
    dataY = 0;
    dataX = 0;

    repeat (2**IN_DW) begin
        dataY = dataY+1;
        $display("------ Division de %d------",$signed(dataY));
        repeat (2**IN_DW) begin

            start = 0;  #50
            start = 1;  #10
            
            dataX = dataX+1;
            data = dataY;
            
            load = 0;  #20
            load = 1;   
            
            data = dataX; #50

            load = 0;  #20
            load = 1;   

            #1000

            $display("%d / %d = %d | REMAINDER = %d | ERROR = %d",$signed(dataX),$signed(dataY),$signed(result),$signed(remainder),error);
            if (($signed(dataY)/$signed(dataX) != $signed(result)) ) begin
                if ( error && (-1 != $signed(result)) ) begin
                    $display("OVERFLOW AT %d / %d | FOUND = %d",$signed(dataX),$signed(dataY),$signed(result));
                end else if ( !error) begin
                    $display("MISMATCH AT %d / %d = %d | FOUND = %d",$signed(dataX),$signed(dataY),$signed(dataX/dataY),$signed(result));
                end
            end

            if (($signed(dataY)%$signed(dataX) != $signed(remainder)) ) begin
                if ( error && (-1 != $signed(remainder)) ) begin
                    $display("ERROR AT %d / %d | FOUND = %d",$signed(dataX),$signed(dataY),$signed(result));
                end else if ( !error) begin
                    $display("MISMATCH AT %d / %d REMAINDER = %d | FOUND = %d",$signed(dataX),$signed(dataY),$signed(dataX)%$signed(dataY),$signed(remainder));
                end
            end
        end
    end 
#1000  


// =============================================================================
// SQUARE
// =============================================================================

    op = SQUARE;
    dataX = 0;

    $display("------ Raiz ------");
    repeat (2**IN_DW) begin

        start = 0;  #50
        start = 1;  #10
    
        data = dataX; #50 

        load = 0;  #20
        load = 1;

         #1000


        $display("sqrt(%d) = %d | REMAINDER = %d | ERROR = %d",$signed(dataX),$signed(result),$signed(remainder),error);
	    if (($floor($sqrt(dataX)) != $signed(result)) ) begin
            if ( error && (-1 != $signed(result)) ) begin
                $display("sqrt(%d) = imaginary | FOUND = %d",$signed(dataX),$signed(result));
            end else if ( !error) begin
                $display("MISMATCH AT sqrt(%d) = %d | FOUND = %d",$signed(dataX),$floor($sqrt(dataX)),$signed(result));
            end
        end

        if (($signed(dataX) - ($floor($sqrt(dataX)) * $floor($sqrt(dataX))) != $signed(remainder)) ) begin
            if ( error && (-1 != $signed(remainder)) ) begin
                $display("sqrt(%d) = imaginary | FOUND = %d",$signed(dataX),$signed(result));
            end else if ( !error) begin
                $display("MISMATCH AT sqrt(%d) = %d REMAINDER = %d | FOUND = %d",$signed(dataX),$floor($sqrt(dataX)),$signed(dataX) - ($floor($sqrt(dataX)) * $floor($sqrt(dataX))),$signed(remainder));
            end
        end

        dataX = dataX+1;
    end 
#1000  



    #500
    $stop;
end

endmodule






