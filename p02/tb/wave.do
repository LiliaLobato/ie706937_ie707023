onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mult/uut/clk_gen
add wave -noupdate /tb_mult/uut/start
add wave -noupdate /tb_mult/uut/dbcr_start
add wave -noupdate /tb_mult/uut/rst
add wave -noupdate -divider TOP
add wave -noupdate /tb_mult/uut/load
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/data
add wave -noupdate /tb_mult/uut/pipo_data/inp
add wave -noupdate -color Plum /tb_mult/uut/MDR_inst/op
add wave -noupdate /tb_mult/uut/load_x
add wave -noupdate /tb_mult/uut/load_y
add wave -noupdate /tb_mult/uut/ready
add wave -noupdate -color Cyan /tb_mult/uut/error
add wave -noupdate -color Cyan -radix decimal /tb_mult/uut/result
add wave -noupdate -color Cyan -radix decimal /tb_mult/uut/remainder
add wave -noupdate -color Cyan /tb_mult/uut/signo
add wave -noupdate -expand -subitemconfig {{/tb_mult/uut/n_seg[1]} {-color Cyan -height 15} {/tb_mult/uut/n_seg[0]} {-color Cyan -height 15}} /tb_mult/uut/n_seg
add wave -noupdate -divider multiplication
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/dataX/out
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/dataY/out
add wave -noupdate /tb_mult/uut/sign_detection/sign
add wave -noupdate /tb_mult/uut/MDR_inst/sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/out_sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/sum
add wave -noupdate /tb_mult/uut/MDR_inst/n_product
add wave -noupdate /tb_mult/uut/MDR_inst/out
add wave -noupdate -color Plum -radix unsigned /tb_mult/uut/result_detection/result_bin
add wave -noupdate -color Plum -radix unsigned /tb_mult/uut/result_detection/remainder_bin
add wave -noupdate -divider divition
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/dataX/out
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/dataY/out
add wave -noupdate /tb_mult/uut/sign_detection/sign
add wave -noupdate /tb_mult/uut/MDR_inst/sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/out_sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/sum
add wave -noupdate /tb_mult/uut/MDR_inst/n_product
add wave -noupdate /tb_mult/uut/MDR_inst/out
add wave -noupdate -color Plum -radix unsigned /tb_mult/uut/result_detection/result_bin
add wave -noupdate -color Plum -radix unsigned /tb_mult/uut/result_detection/remainder_bin
add wave -noupdate -divider square
add wave -noupdate -radix decimal /tb_mult/uut/MDR_inst/i_count
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/dataX/out
add wave -noupdate /tb_mult/uut/sign_detection/sign
add wave -noupdate /tb_mult/uut/MDR_inst/r_sum
add wave -noupdate /tb_mult/uut/MDR_inst/sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/out_sum_wr
add wave -noupdate /tb_mult/uut/MDR_inst/sum
add wave -noupdate /tb_mult/uut/MDR_inst/n_product
add wave -noupdate /tb_mult/uut/MDR_inst/out
add wave -noupdate /tb_mult/uut/MDR_inst/r_sqrt
add wave -noupdate -color Plum -radix binary /tb_mult/uut/result_detection/result_bin
add wave -noupdate -color Plum -radix binary -childformat {{{/tb_mult/uut/result_detection/remainder_bin[7]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[6]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[5]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[4]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[3]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[2]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[1]} -radix unsigned} {{/tb_mult/uut/result_detection/remainder_bin[0]} -radix unsigned}} -subitemconfig {{/tb_mult/uut/result_detection/remainder_bin[7]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[6]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[5]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[4]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[3]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[2]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[1]} {-color Plum -height 15 -radix unsigned} {/tb_mult/uut/result_detection/remainder_bin[0]} {-color Plum -height 15 -radix unsigned}} /tb_mult/uut/result_detection/remainder_bin
add wave -noupdate -color Plum -radix binary /tb_mult/uut/result
add wave -noupdate /tb_mult/uut/result_detection/product_wr_next
add wave -noupdate /tb_mult/uut/MDR_inst/r_sqrt
add wave -noupdate /tb_mult/uut/result_detection/r_sqrt
add wave -noupdate -radix decimal /tb_mult/uut/conv_remainder/binario
add wave -noupdate -color Plum -radix decimal /tb_mult/uut/remainder
add wave -noupdate -divider state_machine
add wave -noupdate /tb_mult/uut/state_Mult/op_en
add wave -noupdate /tb_mult/uut/state_Mult/pipo_en
add wave -noupdate /tb_mult/uut/state_Mult/done_en
add wave -noupdate /tb_mult/uut/state_Mult/load_en
add wave -noupdate /tb_mult/uut/state_Mult/ready_en
add wave -noupdate -radix unsigned /tb_mult/uut/cntr/count_2
add wave -noupdate -radix decimal /tb_mult/uut/cntr/count
add wave -noupdate /tb_mult/uut/state_Mult/Edo_Actual
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {612237355 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 233
configure wave -valuecolwidth 64
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {612175089 ps} {612517101 ps}
