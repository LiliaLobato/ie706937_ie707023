// =============================================================================
// Title        :   Implementacion de un divisor de frecuencia
// Inputs       :   System clk and reset 
// Outputs      :   clock generated
// Project     	: 	t03
// File        	: 	tb_clk_div.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================


`timescale 1ns / 1ps
module tb_clk_div();
import clk_div_pkg::*;

logic    clk;
logic    rst;
logic    clk_gen;

clk_div #(
    .FREQUENCY      (FREQUENCY_TB),
    .REFERENCE_CLK  (REFERENCE_CLKTB)
) uut (
    .clk        (clk),
    .rst        (rst),
    .clk_gen    (clk_gen) 
);


initial begin
    clk = 0;
end

always begin
        #5  clk = ~clk;
end

initial begin 
    rst = 0;
    #20 rst = 1;

    #500
    $stop;
end

endmodule
