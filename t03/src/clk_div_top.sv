// =============================================================================
// Title       	:	Implementacion de un divisor de frecuencia
// Inputs		:	System clk and reset 
// Outputs		:	clock generated
// Project     	: 	t03
// File        	: 	clk_div_top.sv
// Date 		: 	01/02/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module clk_div_top
import clk_div_pkg::*;
(
	input  logic	clk,
	input  logic	rst,
	output logic	clk_gen   
);

clk_div #(
	.FREQUENCY      (FREQUENCY_TB),
	.REFERENCE_CLK  (REFERENCE_CLKFPGA)
) clk_divisor (
	.clk    (clk),
	.rst    (rst),
	.clk_gen(clk_gen)
);

endmodule