// =============================================================================
// Title        : Siso right
//               	The serial input will be shifter DW time to 
//					the right to generate a serial output
// Inputs       : System clk, reset, enb, serial input
// Outputs      : serial output
// Project      :   t05
// File         :   siso_right.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          		Gabriel Paz    ie707023
// =============================================================================

module siso_right #(
parameter DW = 4
) (
input               clk,
input               rst,
input               enb,
input               inp,
output              out
);

logic [DW-1:0]      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (enb)
        rgstr_r     <= {inp, rgstr_r[DW-1:1]};
end:rgstr_label

assign out  = rgstr_r[DW-1];

endmodule