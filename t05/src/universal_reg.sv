// =============================================================================
// Title        : Universal register
//               	Depending on the value of sel, the data input will be 
//					passed to the corresponding register following values 
// 	   				for sel:
// 	   					PIPO = 2'b000, 
// 	   					PISO_LSB = 2'b001, 
// 	   					PISO_MSB = 2'b010, 
// 	 	  				SIPO_LSB = 2'b011, 
// 	   					SIPO_MSB = 2'b100, 
// 	   					SISO_LEFT = 2'b101, 
// 	   					SISO_RIGHT = 2'b110
// Inputs       : System clk, reset, enb, load/shift, selector, data input
// Outputs      : corresponding output from register
// Project      :   t05
// File         :   universal_reg.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          		Gabriel Paz    ie707023
// =============================================================================

import universal_reg_pkg::*;

module universal_reg 
import universal_reg_pkg::*;
#(
parameter DW = 4
) (
input 			clk,
input			rst,
input 			enb,
input 			l_s,
input universal_reg_type sel,
input [DW-1:0] inp,
output [DW-1:0] out
);

logic [DW-1:0] PIPO_out;
logic [DW-1:0] SIPO_out_lsb;
logic [DW-1:0] SIPO_out_msb;
logic SISO_out_left;
logic SISO_out_right;
logic PISO_out_lsb;
logic PISO_out_msb;
logic [DW-1:0] Output_w;

// ==========================
// PIPO
// ==========================

pipo #(
.DW(DW)
) pipo(
.clk(clk),
.rst(rst),
.enb(enb),
.inp(inp),
.out(PIPO_out) );

// ==========================
// SISO
// ==========================
siso_left #(
.DW(DW)
) siso_left(
.clk(clk),
.rst(rst),
.enb(enb),
.inp(inp[0]),
.out(SISO_out_left) );

siso_right #(
.DW(DW)
) siso_right(
.clk(clk),
.rst(rst),
.enb(enb),
.inp(inp[0]),
.out(SISO_out_right) );

// ==========================
// SIPO
// ==========================
sipo_msb #(
.DW(DW)
) sipo_msb (
.clk(clk),
.rst(rst),
.enb(enb),
.inp(inp[0]),
.out(SIPO_out_msb) );

sipo_lsb #(
.DW(DW)
) sipo_lsb (
.clk(clk),
.rst(rst),
.enb(enb),
.inp(inp[0]),
.out(SIPO_out_lsb) );

// ==========================
// PISO
// ==========================
piso_msb #(
.DW(DW)
) piso_msb (
.clk(clk),    
.rst(rst),    
.enb(enb),    	
.l_s(l_s),    	    
.inp(inp),    
.out(PISO_out_msb) );

piso_lsb #(
.DW(DW)
) piso_lsb (
.clk(clk),    
.rst(rst),    
.enb(enb),    	
.l_s(l_s),    	    
.inp(inp),    
.out(PISO_out_lsb) );

// ==========================
// SELECTOR
// ==========================

always_comb begin
	case(sel)			 
		PIPO:
			Output_w = PIPO_out;
    	PISO_LSB:
			Output_w = PISO_out_lsb;
    	PISO_MSB:
			Output_w = PISO_out_msb;
    	SIPO_LSB:
			Output_w = SIPO_out_lsb;
    	SIPO_MSB:
			Output_w = SIPO_out_msb;
    	SISO_LEFT:
			Output_w = SISO_out_left;
    	SISO_RIGHT:
			Output_w = SISO_out_right;
		default:
			Output_w = '0;
   endcase

end

assign out = Output_w;
endmodule