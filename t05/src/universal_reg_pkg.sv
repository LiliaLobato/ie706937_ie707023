// =============================================================================
// Title        : Universal register package
//               	Package for universal register type
// Project      :   t05
// File         :   universal_reg_pkg.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//          		Gabriel Paz    ie707023
// =============================================================================


`ifndef UNIVERSAL_PKG_SV
    `define UNIVERSAL_PKG_SV
package universal_reg_pkg;

// 
typedef enum logic [2:0]{
    PIPO = 			3'b000, 
    PISO_LSB = 		3'b001, 
    PISO_MSB = 		3'b010, 
    SIPO_LSB = 		3'b011, 
    SIPO_MSB = 		3'b100, 
    SISO_LEFT = 	3'b101, 
    SISO_RIGHT = 	3'b110
    } universal_reg_type;

endpackage
`endif 

