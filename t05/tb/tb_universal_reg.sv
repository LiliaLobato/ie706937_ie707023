`timescale 1ns / 1ps

import universal_reg_pkg::*;

localparam DW = 4;

module tb_universal_reg;

logic               clk;
logic               rst;
logic               enb;
logic               l_s;
universal_reg_type  sel;
logic   [DW-1:0]    inp;
logic   [DW-1:0]    out;

universal_reg #(
    .DW(DW)
)uut (
    .clk(clk),
    .rst(rst),
    .enb(enb),
    .l_s(l_s),
    .sel(sel),
    .inp(inp),
    .out(out)
);
//valores iniciales
initial begin
    rst     = 1;
    clk     = 0;
    enb     = 0;
    inp     = 0;
    l_s     = 0;
end


initial begin
//reset
    rst     = 0;        #2
    rst     = 1;

// ==========================
// PIPO
// ==========================
//comienzan transacciones PIPO
    sel     = PIPO;
    enb     = 1;
    inp     = $urandom%16;      #4
    inp     = $urandom%16;      #4
//reset
    rst     = 0;        #2
    rst     = 1;
// ==========================
    
// ==========================
// PISO
// ==========================
//comienzan transacciones LSB
    sel     = PISO_LSB;
    inp     = $urandom%16+1;
    l_s     = 1;        #2
    l_s     = 0;
//midle toogle en enable para sacar 4 datos
    enb     = 0;        #1
    enb     = 1;        #2
    enb     = 0;        #1        
    enb     = 1;        #2
    enb     = 0;        #1
    enb     = 1;        #2
    enb     = 0;        #1
    enb     = 1;        #2
//comienzan transacciones MSB
    sel     = PISO_MSB;
    inp     = $urandom%16;
    l_s     = 1;        #2
    l_s     = 0;
//midle toogle en enable para sacar 4 datos
    enb     = 0;        #1
    enb     = 1;        #2
    enb     = 0;        #1        
    enb     = 1;        #2
    enb     = 0;        #1
    enb     = 1;        #2
    enb     = 0;        #1
    enb     = 1;        #2
//reset
    rst     = 0;        #2
    rst     = 1;
// ==========================
// ==========================
// SIPO
// ==========================
//comienzan transacciones LSB
    sel     = SIPO_LSB;
    enb     = 1;
    inp     = 1;        #2
    inp     = 0;        #2
    inp     = 1;        #4
    inp     = 0;        #4
//reset
    rst     = 0;        #2
    rst     = 1;
//comienzan transacciones MSB
    sel     = SIPO_MSB;
    inp     = 1;        #2
    inp     = 0;        #2
    inp     = 1;        #4
    inp     = 0;        #4
//reset
    rst     = 0;        #2
    rst     = 1;
// ==========================
    
// ==========================
// SISO
// ==========================
//comienzan transacciones RIGHT
    sel     = SISO_RIGHT;
    enb     = 1;
    inp     = 1;        #2
    inp     = 0;        #2
    inp     = 1;        #4
    inp     = 0;        #4
//reset
    rst     = 0;        #2
    rst     = 1;
//comienzan transacciones LEFT
    sel     = SISO_LEFT;
    inp     = 1;        #2
    inp     = 0;        #2
    inp     = 1;        #4
    inp     = 0;        #4
//reset
    rst     = 0;        #2
    rst     = 1;
// ==========================
    #100
    $stop;

end

always begin
    #1 clk <= ~clk;
end

endmodule
