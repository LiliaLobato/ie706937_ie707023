onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_siso_left/uut/clk
add wave -noupdate /tb_siso_left/uut/rst
add wave -noupdate /tb_siso_left/uut/enb
add wave -noupdate /tb_siso_left/uut/inp
add wave -noupdate /tb_siso_left/uut/out
add wave -noupdate -color Violet -expand -subitemconfig {{/tb_siso_left/uut/rgstr_r[3]} {-color Violet -height 15} {/tb_siso_left/uut/rgstr_r[2]} {-color Violet -height 15} {/tb_siso_left/uut/rgstr_r[1]} {-color Violet -height 15} {/tb_siso_left/uut/rgstr_r[0]} {-color Violet -height 15}} /tb_siso_left/uut/rgstr_r
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7220 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 165
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {42050 ps}
