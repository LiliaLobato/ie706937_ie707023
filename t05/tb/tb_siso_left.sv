`timescale 1ns / 1ps
module tb_siso_left;

logic           clk;
logic           rst;
logic           enb;
logic           inp;
logic           out;


siso_left uut(
.clk    (clk    ),
.rst    (rst    ),
.enb    (enb    ),
.inp    (inp    ),
.out    (out    )
);

//valores iniciales
initial begin
    rst     = 1;
    clk     = 0;
    enb     = 0;
    inp     = 0;
end

initial begin
//reset
#2      rst = 0;
#2      rst = 1;

//comienzan transacciones
    #5      enb     = 1;
    #5      inp     = 1;
    #5      inp     = 0;
    #5      inp     = 1;
    #3      inp     = 0;
    #6      inp     = 1;
    #6      inp     = 0;
    #6      inp     = 1;
    #6      inp     = 0;
//midle toogle en enable
    #2      enb     = 0;
    #2      enb     = 1;
    #4      inp     = 1;

    #100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
