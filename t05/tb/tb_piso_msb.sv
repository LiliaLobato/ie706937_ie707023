`timescale 1ns / 1ps
module tb_piso_msb;

logic           clk;
logic           rst;
logic           enb;
logic           l_s;
logic [3:0]     inp;
logic           out;


piso_msb uut(
.clk    (clk    ),
.rst    (rst    ),
.enb    (enb    ),
.l_s    (l_s    ),
.inp    (inp    ),
.out    (out    )
);

//valores iniciales
initial begin
    rst     = 1;
    clk     = 0;
    enb     = 0;
    inp     = 0;
    l_s     = 0;
end

initial begin

//reset
#2      rst = 0;
#2      rst = 1;

//comienzan transacciones
    #5      enb     = 1;
    #5      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;

//midle toogle en enable
    #5      enb     = 0;
    #5      enb     = 1;

    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #8      l_s     = 1;
            inp     = $urandom%16;
    #2      l_s     = 0;
    #5      inp     = $urandom%16;
    #3      inp     = $urandom%16;
    #6      inp     = $urandom%16;
    #100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule

