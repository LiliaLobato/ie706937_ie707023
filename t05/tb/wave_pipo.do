onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_pipo/clk
add wave -noupdate /tb_pipo/rst
add wave -noupdate /tb_pipo/enb
add wave -noupdate /tb_pipo/inp
add wave -noupdate -color Violet /tb_pipo/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {67740 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 172
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {82007 ps}
