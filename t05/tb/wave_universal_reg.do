onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_universal_reg/clk
add wave -noupdate /tb_universal_reg/rst
add wave -noupdate /tb_universal_reg/enb
add wave -noupdate /tb_universal_reg/l_s
add wave -noupdate -color {Slate Blue} /tb_universal_reg/sel
add wave -noupdate -color Violet -expand -subitemconfig {{/tb_universal_reg/inp[3]} {-color Violet -height 15} {/tb_universal_reg/inp[2]} {-color Violet -height 15} {/tb_universal_reg/inp[1]} {-color Violet -height 15} {/tb_universal_reg/inp[0]} {-color Violet -height 15}} /tb_universal_reg/inp
add wave -noupdate -color {Steel Blue} /tb_universal_reg/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {36449 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 166
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {47176 ps} {86698 ps}
