`timescale 1ns / 1ps
module tb_pipo;

logic           clk;
logic           rst;
logic           enb;
logic [3:0]     inp;
logic [3:0]     out;


pipo uut(
.clk    (clk    ),
.rst    (rst    ), //pullup
.enb    (enb    ),
.inp    (inp    ),
.out    (out    )
);

//valores iniciales
initial begin
    rst     = 1;
    clk     = 0;
    enb     = 0;
    inp     = 0;
end

initial begin

//reset
#2      rst = 0;
#2      rst = 1;

//comienzan transacciones
#2      enb     = 1;

    #5      inp     = $urandom%16;
    #5      inp     = $urandom%16;
    #5      inp     = $urandom%16;
    #3      inp     = $urandom%16;
//midle toogle en enable
    #5      enb     = 0;
    #5      enb     = 1;
    #6      inp     = $urandom%16;
    #6      inp     = $urandom%16;
    #6      inp     = $urandom%16;
    #6      inp     = $urandom%16;

#100
$stop;

end

always begin
    #1 clk <= ~clk;
end

endmodule

