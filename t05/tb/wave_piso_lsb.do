onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_piso_lsb/clk
add wave -noupdate /tb_piso_lsb/rst
add wave -noupdate /tb_piso_lsb/enb
add wave -noupdate /tb_piso_lsb/l_s
add wave -noupdate -color {Medium Violet Red} -expand -subitemconfig {{/tb_piso_lsb/inp[3]} {-color {Medium Violet Red} -height 15} {/tb_piso_lsb/inp[2]} {-color {Medium Violet Red} -height 15} {/tb_piso_lsb/inp[1]} {-color {Medium Violet Red} -height 15} {/tb_piso_lsb/inp[0]} {-color {Medium Violet Red} -height 15}} /tb_piso_lsb/inp
add wave -noupdate -color Violet /tb_piso_lsb/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {34282 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 181
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {121478 ps}
