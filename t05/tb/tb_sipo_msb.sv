`timescale 1ns / 1ps
module tb_sipo_msb;

logic           clk;
logic           rst;
logic           enb;
logic           inp;
logic  [3:0]    out;


sipo_msb    uut(
.clk    (clk    ),
.rst    (rst    ),
.enb    (enb    ),
.inp    (inp    ),
.out    (out    )
);

//valores iniciales
initial begin
    rst     = 1;
    clk     = 0;
    enb     = 0;
    inp     = 0;
end

initial begin
//reset
#2      rst = 0;
#2      rst = 1;

//comienzan transacciones
    #1      enb     = 1;
    #2      inp     = 1;
    #2      inp     = 0;
    #2      inp     = 1;
    #4      inp     = 0;
    #4      inp     = 1;
    #2      inp     = 0;
    #2      inp     = 1;
    #4      inp     = 0;
    #4      inp     = 1;
    //midle toogle en enable
    #2      enb     = 0;
    #2      enb     = 1;
    #4      inp     = 1;
    
    #100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
