onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_piso_msb/clk
add wave -noupdate /tb_piso_msb/rst
add wave -noupdate /tb_piso_msb/enb
add wave -noupdate /tb_piso_msb/l_s
add wave -noupdate -color {Medium Violet Red} -expand -subitemconfig {{/tb_piso_msb/inp[3]} {-color {Medium Violet Red}} {/tb_piso_msb/inp[2]} {-color {Medium Violet Red}} {/tb_piso_msb/inp[1]} {-color {Medium Violet Red}} {/tb_piso_msb/inp[0]} {-color {Medium Violet Red}}} /tb_piso_msb/inp
add wave -noupdate -color Violet /tb_piso_msb/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {33206 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {105 ns}
