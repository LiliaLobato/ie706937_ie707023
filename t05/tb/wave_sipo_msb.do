onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_sipo_msb/clk
add wave -noupdate /tb_sipo_msb/rst
add wave -noupdate /tb_sipo_msb/enb
add wave -noupdate /tb_sipo_msb/inp
add wave -noupdate -color Violet -expand -subitemconfig {{/tb_sipo_msb/out[3]} {-color Violet} {/tb_sipo_msb/out[2]} {-color Violet} {/tb_sipo_msb/out[1]} {-color Violet} {/tb_sipo_msb/out[0]} {-color Violet}} /tb_sipo_msb/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {42522 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {25996 ps} {52246 ps}
